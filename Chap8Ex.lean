import HTPILib.Chap8Part2
import HTPILib.Chap6
namespace HTPI.Exercises

/- Section 8.1 -/
-- 1.
--Hint:  Use Exercise_6_1_16a2 from the exercises of Section 6.1
lemma fnz_odd (k : Nat) : fnz (2 * k + 1) = -↑(k + 1) := by
  have h1 : ¬2 ∣ 2 * k + 1 := by
    have h1 : nat_odd (2 * k + 1) := by
      define
      apply Exists.intro k
      rfl
    have h2 : ¬((nat_even (2 * k + 1)) ∧ nat_odd (2 * k + 1)) :=
      Exercise_6_1_16a2 (2 * k + 1)
    demorgan at h2
    disj_syll h2 h1
    exact h2
  have h2 : fnz (2 * k + 1) = if 2 ∣ 2 * k + 1 then ↑((2 * k + 1) / 2)
    else -↑((2 * k + 1 + 1) / 2) := by rfl
  rw [if_neg h1] at h2
  have h3 : 2 * k + 1 + 1 = 2 * k + 2 * 1 := by rfl
  have h4 : 0 < 2 := by linarith
  rw [h3, ← mul_add, Nat.mul_div_cancel_left (k + 1) h4] at h2
  exact h2
  done

-- 2.
lemma fnz_fzn : fnz ∘ fzn = id  := by
  apply funext
  fix a : Int
  rw [comp_def]
  have h1 : fzn a = if a ≥ 0 then 2 * Int.toNat a else 2 * Int.toNat (-a) - 1
    := by rfl
  by_cases h : a ≥ 0
  · rw [if_pos h] at h1
    rw [h1, fnz_even, Int.toNat_of_nonneg h]
    rfl
  · rw [if_neg h] at h1
    have h2 : a ≤ -1 := by linarith
    set a' : Int := a + 1
    have h3 : a = a' - 1 := by ring
    have h4 : a' ≤ 0 := by
      rw [h3] at h2
      linarith
    have h5 : -(a' - 1) = -a' + 1 := by ring
    have h6 : 0 ≤ -a' := by linarith
    have h7 : (0 : Int) ≤ (1 : Int) := by linarith
    have h8 : ∀ (n : Int), 0 ≤ -n → Int.toNat (-n + 1) = Int.toNat (-n) + 1 := by
      fix n; assume h8 : 0 ≤ -n
      have h9 : Int.toNat (-n + 1) = Int.toNat (-n) + Int.toNat 1
        := Int.toNat_add h8 h7
      rw [Int.toNat_one] at h9
      exact h9
    have h9 : 2 * (Int.toNat (-a') + 1) - 1 = 2 * Int.toNat (-a') + 1 := by
      calc 2 * (Int.toNat (-a') + 1) - 1
        _ = 2 * Int.toNat (-a') + 2 * 1 - 1 := by rw [mul_add]
        _ = 2 * Int.toNat (-a') + 2 - 1 := by rw [mul_one]
        _ = 2 * Int.toNat (-a') + 1 := by norm_num
    rw [h1, h3, h5, h8 a' h6, h9, fnz_odd]
    have h10 : a' = a + 1 := by rfl
    rw [h10]
    have h11 : 0 ≤ -(a + 1) := by linarith
    have h12 : 0 ≤ -(a + 1) + 1 := by linarith
    rw [← h8 (a + 1) h11, Int.toNat_of_nonneg h12]
    have h13 : -(-(a + 1) + 1) = a := by ring
    have h14 : a + 1 - 1 = a := by ring
    rw [h13, h14]
    rfl
  done

-- 3.
lemma tri_step (k : Nat) : tri (k + 1) = tri k + k + 1 := by
  have h1 : tri (k + 1) = (k + 1) * (k + 2) / 2 := by rfl
  have h2 : tri k + k + 1 = k * (k + 1) / 2 + k + 1 := by rfl
  rw [h1, h2]
  rw [add_mul, mul_add, one_mul, mul_add, mul_one]
  have h3 : k * k + k * 2 + (k + 2) = (k * k + k) + 2 * (k + 1) := by
    calc k * k + k * 2 + (k + 2)
      _ = k * k + k * 2 + k + 2 := by ring
      _ = k * k + k + 2 * k + 2 := by ring
      _ = (k * k + k) + 2 * (k + 1) := by ring
  rw [h3]
  have h4 : 2 ∣ 2 * (k + 1) := by
    apply Exists.intro (k + 1)
    rfl
  have h5 : 0 < 2 := by linarith
  rw [Nat.add_div_of_dvd_left h4, Nat.mul_div_cancel_left (k + 1) h5]
  rfl

-- 4.
lemma tri_incr {j k : Nat} (h1 : j ≤ k) : tri j ≤ tri k := by
  have h2 : tri j = j * (j + 1) / 2 := by rfl
  have h3 : tri k = k * (k + 1) / 2 := by rfl
  rw [h2, h3]
  have h4 : j + 1 ≤ k + 1 := by linarith
  have h5 : j * (j + 1) ≤ k * (k + 1) := by
    calc j * (j + 1)
      _ ≤ k * (j + 1) := by rel [h1]
      _ ≤ k * (k + 1) := by rel [h4]
  rel [h5]
  done

-- 5.
lemma ctble_of_equinum_ctble {U V : Type} {A : Set U} {B : Set V}
    (h1 : A ∼ B) (h2 : ctble A) : ctble B := by
  define
  define at h2
  by_cases on h2
  · -- A finite
    define at h2
    obtain (n : Nat) (h3 : I n ∼ A) from h2
    have h4 : I n ∼ B := Theorem_8_1_3_3 h3 h1
    left; define; apply Exists.intro n; exact h4
  · -- A denumerable
    rw [denum_def] at h2
    have h3 : Univ ℕ ∼ B := Theorem_8_1_3_3 h2 h1
    rw [← denum_def] at h3
    right; exact h3
  done

-- 6.
def to_even (n : Int) : Int := 2 * n

theorem Exercise_8_1_1_b : denum { n : Int | even n } := by
  have h1 : image to_even (Univ Int) = { n : Int | even n } := by
    apply Set.ext
    fix n : Int
    apply Iff.intro
    · assume h1 : n ∈ image to_even (Univ ℤ)
      define at h1; define
      obtain (x : Int) (h2 : x ∈ Univ ℤ ∧ to_even x = n) from h1
      apply Exists.intro x
      have h3 : to_even x = n := h2.right
      exact h3.symm
    · assume h1 : n ∈ {n : ℤ | even n}
      define at h1
      obtain (k : Int) (h2 : n = 2 * k) from h1
      define
      apply Exists.intro k
      apply And.intro (elt_Univ k)
      exact h2.symm
  have h2 : one_one_on to_even (Univ Int) := by
    define
    fix x1 : Int; fix x2 : Int
    assume h2 : x1 ∈ Univ ℤ
    assume h3 : x2 ∈ Univ ℤ
    assume h4 : to_even x1 = to_even x2
    have h5 : to_even x1 = 2 * x1 := by rfl
    have h6 : to_even x2 = 2 * x2 := by rfl
    rw [h5, h6] at h4
    have h7 : (2 : Int) ≠ 0 := by norm_num
    exact Int.eq_of_mul_eq_mul_left h7 h4
  have h3 : Univ Int ∼ { n : Int | even n } := equinum_image h2 h1
  rw [denum_def]
  exact Theorem_8_1_3_3 (Theorem_8_1_3_2 Z_equinum_N) h3
  done

-- Definition for next four exercises
def Rel_image {U V : Type} (R : Rel U V) (A : Set U) : Set V :=
  { y : V | ∃ (x : U), x ∈ A ∧ R x y }

-- 7.
lemma Rel_image_on_power_set {U V : Type} {R : Rel U V}
    {A C : Set U} {B : Set V} (h1 : matching R A B) (h2 : C ∈ 𝒫 A) :
    Rel_image R C ∈ 𝒫 B := by
  define at h1
  define; fix v : V; assume h3 : v ∈ Rel_image R C
  define at h3
  obtain (u : U) (h4 : u ∈ C ∧ R u v) from h3
  define at h2
  have h5 : u ∈ A := h2 h4.left
  exact (h1.left h4.right).right
  done

-- 8.
lemma Rel_image_inv {U V : Type} {R : Rel U V}
    {A C : Set U} {B : Set V} (h1 : matching R A B) (h2 : C ∈ 𝒫 A) :
    Rel_image (invRel R) (Rel_image R C) = C := by
  apply Set.ext
  fix u : U
  apply Iff.intro
  · assume h3 : u ∈ Rel_image (invRel R) (Rel_image R C)
    define at h3
    obtain (v : V) (h4 : v ∈ Rel_image R C ∧ invRel R v u) from h3
    define at h1
    have h5 : R u v := h4.right
    have h6 : u ∈ A ∧ v ∈ B := h1.left h5
    have h7 : v ∈ Rel_image R C := h4.left
    define at h7
    obtain (u' : U) (h8 : u' ∈ C ∧ R u' v) from h7
    have h9 : u' ∈ A := h2 h8.left
    have h10 : fcnl_on (invRel R) B := h1.right.right
    have h11 : invRel R v u' := h8.right
    have h12 : u = u' := (h10 h6.right).unique h4.right h11
    rw [h12]; exact h8.left
  · assume h3 : u ∈ C
    define
    have h4 : u ∈ A := h2 h3
    define at h1
    obtain (v : V) (h5 : R u v) from (h1.right.left h4).exists
    apply Exists.intro v
    apply And.intro _ h5
    define
    apply Exists.intro u; exact And.intro h3 h5
  done

-- 9.
lemma Rel_image_one_one_on {U V : Type} {R : Rel U V}
    {A : Set U} {B : Set V} (h1 : matching R A B) :
    one_one_on (Rel_image R) (𝒫 A) := by
  define
  fix X1 : Set U; fix X2 : Set U
  assume h2 : X1 ∈ 𝒫 A
  assume h3 : X2 ∈ 𝒫 A
  assume h4 : Rel_image R X1 = Rel_image R X2
  have h5 : Rel_image (invRel R) (Rel_image R X1) = X1 := Rel_image_inv h1 h2
  have h6 : Rel_image (invRel R) (Rel_image R X2) = X2 := Rel_image_inv h1 h3
  rw [h4] at h5
  rw [← h5]; exact h6
  done

-- 10.
lemma Rel_image_image {U V : Type} {R : Rel U V}
    {A : Set U} {B : Set V} (h1 : matching R A B) :
    image (Rel_image R) (𝒫 A) = 𝒫 B := by
  apply Set.ext
  fix Y : Set V
  apply Iff.intro
  · assume h2 : Y ∈ image (Rel_image R) (𝒫 A)
    define at h2
    obtain (X : Set U) (h3 : X ∈ 𝒫 A ∧ Rel_image R X = Y) from h2
    have h4 : Rel_image R X ∈ 𝒫 B := Rel_image_on_power_set h1 h3.left
    rw [← h3.right]
    exact h4
  · assume h2 : Y ∈ 𝒫 B
    define
    set X : Set U := Rel_image (invRel R) Y
    have h3 : matching (invRel R) B A := inv_match h1
    apply Exists.intro X
    apply And.intro
    · exact Rel_image_on_power_set h3 h2
    · exact Rel_image_inv h3 h2
  done

-- 11.
--Hint:  Use the previous two exercises.
theorem Exercise_8_1_5 {U V : Type} {A : Set U} {B : Set V}
    (h1 : A ∼ B) : 𝒫 A ∼ 𝒫 B := by
  define at h1
  obtain (R : Rel U V) (h2 : matching R A B) from h1
  have h3 : image (Rel_image R) (𝒫 A) = 𝒫 B := Rel_image_image h2
  have h4 : one_one_on (Rel_image R) (𝒫 A) := Rel_image_one_one_on h2
  exact equinum_image h4 h3
  done

-- 12.
theorem Exercise_8_1_17 {U : Type} {A B : Set U}
    (h1 : B ⊆ A) (h2 : ctble A) : ctble B := by
  obtain (R : Rel Nat U) (h3 : fcnl_onto_from_nat R A)
    from (Theorem_8_1_5_2 A).ltr h2
  define at h3
  rw [Theorem_8_1_5_2]
  apply Exists.intro R
  apply And.intro
  · exact h3.left
  · define
    fix u : U; assume h4 : u ∈ B
    have h5 : u ∈ A := h1 h4
    exact h3.right h5
  done

-- 13.
theorem ctble_of_onto_func_from_N {U : Type} {A : Set U} {f : Nat → U}
    (h1 : ∀ x ∈ A, ∃ (n : Nat), f n = x) : ctble A := by
  set R : Rel Nat U := RelFromExt {(n, u) : Nat × U | f n = u}
  rw [Theorem_8_1_5_2]
  apply Exists.intro R
  define
  apply And.intro
  · define
    fix n : Nat
    fix u1 : U; fix u2 : U
    assume h2 : R n u1; assume h3 : R n u2
    define at h2
    define at h3
    rw [← h2]; exact h3
  · define
    exact h1
  done

-- 14.
theorem ctble_of_one_one_func_to_N {U : Type} {A : Set U} {f : U → Nat}
    (h1 : one_one_on f A) : ctble A := by
  set R : Rel U Nat := RelFromExt {(u, n) : U × Nat | f u = n}
  rw [Theorem_8_1_5_3]
  apply Exists.intro R
  apply And.intro
  · define
    fix u : U; assume h2 : u ∈ A
    exists_unique
    · apply Exists.intro (f u)
      rfl
    · define at h1
      fix n1 : Nat; fix n2 : Nat
      assume h3 : R u n1; assume h4 : R u n2
      define at h3; define at h4
      rw [← h3]; exact h4
  · fix u1 : U; fix u2 : U; fix n : Nat
    assume h2 : u1 ∈ A ∧ R u1 n
    assume h3 : u2 ∈ A ∧ R u2 n
    have h4 : f u1 = f u2 := by
      have h4 : f u1 = n := h2.right
      have h5 : f u2 = n := h3.right
      rw [h4, h5]
    exact h1 h2.left h3.left h4
  done

/- Section 8.1½ -/
-- 1.
lemma remove_one_iff {U V : Type}
    {A : Set U} {B : Set V} {R : Rel U V} (h1 : matching R A B)
    {u : U} (h2 : u ∈ A) (v : V) {x : U} (h3 : x ∈ A \ {u}) :
    ∃ w ∈ A, ∀ (y : V), remove_one R u v x y ↔ R w y := by
  by_cases h4 : R x v
  · apply Exists.intro u
    apply And.intro h2
    fix y : V
    apply Iff.intro
    · assume h5 : remove_one R u v x y
      define at h5
      have h6 : R x y ∨ R x v ∧ R u y := h5.right.right
      have h7 : ¬R x y := by
        by_contra h7
        have h8 : y = v := (h1.right.left h3.left).unique h7 h4
        show False from h5.right.left h8
      disj_syll h6 h7
      exact h6.right
    · assume h5 : R u y
      define
      apply And.intro h3.right
      apply And.intro
      · by_contra h6
        rw [h6] at h5
        have h7 : v ∈ B := (h1.left h5).right
        have h8 : x = u := (h1.right.right h7).unique h4 h5
        show False from h3.right h8
      right
      exact And.intro h4 h5
  · apply Exists.intro x
    apply And.intro h3.left
    fix y : V
    have h5 : ¬(R x v ∧ R u y) := by
      by_contra h5
      show False from h4 h5.left
    apply Iff.intro
    · assume h6 : remove_one R u v x y
      define at h6
      have h7 : R x y ∨ R x v ∧ R u y := h6.right.right
      disj_syll h7 h5
      exact h7
    · assume h6 : R x y
      define
      apply And.intro h3.right
      apply And.intro
      · by_contra h7
        rw [h7] at h6
        show False from h4 h6
      · left; exact h6
  done

-- 2.
lemma inv_one_match {U V : Type} (a : U) (b : V) :
    invRel (one_match a b) = one_match b a := by
  apply relext
  fix v : V; fix u : U
  apply Iff.intro
  · assume h : invRel (one_match a b) v u
    define at h
    define
    exact And.intro h.right h.left
  · assume h : one_match b a v u
    define at h
    define
    exact And.intro h.right h.left
  done

-- 3.
theorem one_match_fcnl {U V : Type} (a : U) (b : V) :
    fcnl_on (one_match a b) {a} := by
  define
  fix x : U; assume h : x ∈ {a}
  define at h
  exists_unique
  · -- existence
    apply Exists.intro b
    rw [h]
    define
    trivial
  · -- uniqueness
    fix y1 : V; fix y2 : V
    assume h1 : one_match a b x y1
    assume h2 : one_match a b x y2
    define at h1
    define at h2
    rw [h1.right, h2.right]
  done

-- 4.
--Hint:  Use the previous two exercises.
lemma one_match_match {U V : Type} (a : U) (b : V) :
    matching (one_match a b) {a} {b} := by
  define
  apply And.intro
  · -- rel_within
    define
    fix x : U; fix y : V
    assume h : one_match a b x y
    define at h
    exact h
  · -- fcnl_ons
    apply And.intro
    · exact one_match_fcnl a b
    · rw [inv_one_match]
      exact one_match_fcnl b a
  done

-- 5.
lemma neb_nrpb (m : Nat) : ∀ ⦃k : Nat⦄, k ≤ m →
    num_elts_below (Set_rp_below m) k (num_rp_below m k) := by
  by_induc
  · assume h1 : 0 ≤ m
    rw [num_rp_below_base, num_elts_below]
  · fix k : Nat
    assume ih : k ≤ m → num_elts_below (Set_rp_below m) k (num_rp_below m k)
    assume h1 : k + 1 ≤ m
    have h2 : k ≤ m := by linarith
    have h3 : num_elts_below (Set_rp_below m) k (num_rp_below m k) := ih h2
    by_cases h4 : rel_prime m k
    · rw [num_rp_below_step_rp h4, num_elts_below]
      left
      apply And.intro
      · define
        apply And.intro h4
        linarith
      · apply And.intro
        · linarith
        · exact h3
    · rw [num_rp_below_step_not_rp h4, num_elts_below]
      right
      apply And.intro _ h3
      define
      demorgan
      left
      exact h4
  done

-- 6.
lemma prod_fcnl {U V W X : Type} {R : Rel U V} {S : Rel W X}
    {A : Set U} {C : Set W} (h1 : fcnl_on R A) (h2 : fcnl_on S C) :
    fcnl_on (R ×ᵣ S) (A ×ₛ C) := by
  define
  fix (u, w) : U × W
  assume h3 : (u, w) ∈ A ×ₛ C
  rw [Set_prod_def] at h3
  exists_unique
  · obtain (v : V) (h4 : R u v) from (h1 h3.left).exists
    obtain (x : X) (h5 : S w x) from (h2 h3.right).exists
    apply Exists.intro (v, x)
    rw [Rel_prod_def]
    exact And.intro h4 h5
  · fix (v1, x1) : V × X
    fix (v2, x2) : V × X
    assume h4 : (R ×ᵣ S) (u, w) (v1, x1); rw [Rel_prod_def] at h4
    assume h5 : (R ×ᵣ S) (u, w) (v2, x2); rw [Rel_prod_def] at h5
    have h6 : v1 = v2 := (h1 h3.left).unique h4.left h5.left
    have h7 : x1 = x2 := (h2 h3.right).unique h4.right h5.right
    rw [h6, h7]

-- 7.
--Hint:  Use the previous exercise.
lemma prod_match {U V W X : Type}
    {A : Set U} {B : Set V} {C : Set W} {D : Set X}
    {R : Rel U V} {S : Rel W X}
    (h1 : matching R A B) (h2 : matching S C D) :
    matching (R ×ᵣ S) (A ×ₛ C) (B ×ₛ D) := by
  define
  apply And.intro
  · -- rel_within
    define
    fix (u, w) : U × W
    fix (v, x) : V × X
    assume h3 : (R ×ᵣ S) (u, w) (v, x)
    rw [Set_prod_def, Set_prod_def]
    rw [Rel_prod_def] at h3
    have h4 : u ∈ A ∧ v ∈ B := h1.left h3.left
    have h5 : w ∈ C ∧ x ∈ D := h2.left h3.right
    exact And.intro (And.intro h4.left h5.left) (And.intro h4.right h5.right)
  · apply And.intro
    · exact prod_fcnl h1.right.left h2.right.left
    · exact prod_fcnl h1.right.right h2.right.right
  done

-- 8.
--Hint:  You might find it helpful to apply the theorem div_mod_char
--from the exercises of Section 6.4.

-- We make use of the following exercise
theorem quot_rem_unique (m q r q' r' : Nat)
    (h1 : m * q + r = m * q' + r') (h2 : r < m) (h3 : r' < m) :
    q = q' ∧ r = r' := sorry

lemma qr_image (m n : Nat) : image (qr n) (I (m * n)) = I m ×ₛ I n := by
  by_cases h : n = 0
  · rw [h, mul_zero]
    apply Set.ext
    fix (q, r) : Nat × Nat
    rw [Set_prod_def]
    apply Iff.intro
    · assume h1 : (q, r) ∈ image (qr 0) (I 0)
      define at h1
      obtain (x : Nat) (h2 : x ∈ I 0 ∧ qr 0 x = (q, r)) from h1
      have h3 : x ∈ I 0 := h2.left
      define at h3
      linarith
    · assume h1 : q ∈ I m ∧ r ∈ I 0
      have h2 : r ∈ I 0 := h1.right
      define at h2
      linarith
  · have h' : n > 0 := Nat.pos_of_ne_zero h
    apply Set.ext
    fix (q, r) : Nat × Nat
    apply Iff.intro
    · assume h1 : (q, r) ∈ image (qr n) (I (m * n))
      define at h1
      obtain (x : Nat) (h2 : x ∈ I (m * n) ∧ qr n x = (q, r)) from h1
      rw [qr_def n x] at h2
      rw [Set_prod_def]
      have h3 : (x / n, x % n) = (q, r) := h2.right
      have h4 : x / n = q ∧ x % n = r := Prod.ext_iff.ltr h3
      apply And.intro
      · define
        have h5 : x ∈ I (m * n) := h2.left
        define at h5
        have h6 : n ∣ m * n := by
          apply Exists.intro m; rw [mul_comm]
        have h7 : x / n < m * n / n := Nat.div_lt_div_of_lt_of_dvd h6 h5
        rw [← h4.left]
        rw [Nat.mul_div_cancel m h'] at h7
        exact h7
      · define
        rw [← h4.right]
        exact Nat.mod_lt x h'
    · assume h1 : (q, r) ∈ I m ×ₛ I n
      rw [Set_prod_def] at h1
      define
      apply Exists.intro (n * q + r)
      have h2 : n ∣ n * q := by
        apply Exists.intro q
        rfl
      apply And.intro
      · define
        have h3 : q < m := h1.left
        have h4 : r < n := h1.right
        have h5 : q + 1 ≤ m := by linarith
        have h6 : n * (q + 1) ≤ n * m := by rel [h5]
        rw [mul_add, mul_one] at h6
        have h7 : n * q + r < n * q + n :=
          calc n * q + r
            _ < n * q + n := by rel [h4]
        rw [mul_comm n m] at h6
        exact Nat.lt_of_lt_of_le h7 h6
      · have h3 : n * ((n * q + r) / n) + (n * q + r) % n = n * q + r
          := Nat.div_add_mod (n * q + r) n
        have h4 : (n * q + r) % n < n := Nat.mod_lt (n * q + r) h'
        have h5 : (n * q + r) / n = q ∧ (n * q + r) % n = r
          := quot_rem_unique n ((n * q + r) / n) ((n * q + r) % n) q r h3 h4 h1.right
        rw [qr_def n (n * q + r), h5.left, h5.right]
  done

-- 9.
def Rel_union {U V : Type} (R : Rel U V) (S : Rel U V) : Rel U V :=
  RelFromExt (extension R ∪ extension S)

lemma Rel_union_inv {U V : Type} (R : Rel U V) (S : Rel U V) :
    invRel (Rel_union R S) = Rel_union (invRel R) (invRel S) := by
  apply relext
  fix v : V; fix u : U
  apply Iff.intro
  · assume h : invRel (Rel_union R S) v u
    define at h
    by_cases on h
    · exact Or.inl h
    · exact Or.inr h
  · assume h : Rel_union (invRel R) (invRel S) v u
    define
    define at h
    by_cases on h
    · exact Or.inl h
    · exact Or.inr h
  done

lemma union_fcnl {U V : Type} {A C : Set U} {B D : Set V}
    {R : Rel U V} {S : Rel U V} (h1 : empty (A ∩ C)) (h2 : empty (B ∩ D))
    (h3 : matching R A B) (h4 : matching S C D) :
    fcnl_on (Rel_union R S) (A ∪ C) := by
  define
  fix u : U
  assume h5 : u ∈ A ∪ C
  exists_unique
  · by_cases on h5
    · obtain (v : V) (h6 : R u v) from (h3.right.left h5).exists
      apply Exists.intro v
      exact Or.inl h6
    · obtain (v : V) (h6 : S u v) from (h4.right.left h5).exists
      apply Exists.intro v
      exact Or.inr h6
  · fix v1 : V; fix v2 : V
    assume h6 : Rel_union R S u v1
    assume h7 : Rel_union R S u v2
    by_cases on h5
    · have h8 : ¬u ∈ C := by
        contradict h1 with h8
        apply Exists.intro u
        exact And.intro h5 h8
      have h9 : ¬(u, v1) ∈ extension S := by
        contradict h8 with h9
        exact (h4.left h9).left
      disj_syll h6 h9
      have h10 : ¬(u, v2) ∈ extension S := by
        contradict h8 with h10
        exact (h4.left h10).left
      disj_syll h7 h10
      exact (h3.right.left h5).unique h6 h7
    · have h8 : ¬u ∈ A := by
        contradict h1 with h8
        apply Exists.intro u
        exact And.intro h8 h5
      have h9 : ¬(u, v1) ∈ extension R := by
        contradict h8 with h9
        exact (h3.left h9).left
      disj_syll h6 h9
      have h10 : ¬(u, v2) ∈ extension R := by
        contradict h8 with h10
        exact (h3.left h10).left
      disj_syll h7 h10
      exact (h4.right.left h5).unique h6 h7
  done

theorem Theorem_8_1_2_2
    {U V : Type} {A C : Set U} {B D : Set V}
    (h1 : empty (A ∩ C)) (h2 : empty (B ∩ D))
    (h3 : A ∼ B) (h4 : C ∼ D) : A ∪ C ∼ B ∪ D := by
  define at h3
  define at h4
  obtain (R : Rel U V) (h5: matching R A B) from h3
  obtain (S : Rel U V) (h6: matching S C D) from h4
  apply Exists.intro (Rel_union R S)
  apply And.intro
  · define
    fix u : U
    fix v : V
    assume h7 : Rel_union R S u v
    define at h7
    by_cases on h7
    · have h8 : u ∈ A ∧ v ∈ B := h5.left h7
      exact And.intro (Or.inl h8.left) (Or.inl h8.right)
    · have h8 : u ∈ C ∧ v ∈ D := h6.left h7
      exact And.intro (Or.inr h8.left) (Or.inr h8.right)
  · apply And.intro
    · exact union_fcnl h1 h2 h5 h6
    . rw [Rel_union_inv]
      exact union_fcnl h2 h1 (inv_match h5) (inv_match h6)
  done

-- 10.
def plus_n (n : Nat) (i : Nat) : Nat := n + i

lemma plus_n_def {i : Nat} : plus_n n i = n + i := by rfl

lemma shift_I_equinum (n m : Nat) : I m ∼ I (n + m) \ I n := by
  have h1 : one_one_on (plus_n n) (I m) := by
    define
    fix x1 : Nat; fix x2 : Nat
    assume h1 : x1 ∈ I m
    assume h2 : x2 ∈ I m
    assume h3 : plus_n n x1 = plus_n n x2
    rw [plus_n_def, plus_n_def] at h3
    exact Nat.add_left_cancel h3
  have h2 : image (plus_n n) (I m) = I (n + m) \ I n := by
    apply Set.ext
    fix i : Nat
    apply Iff.intro
    · assume h2 : i ∈ image (plus_n n) (I m)
      obtain (j : Nat) (h3 : j ∈ I m ∧ plus_n n j = i) from h2
      define
      rw [plus_n_def] at h3
      apply And.intro
      · define
        have h4 : j < m := h3.left
        have h5 : n + j < n + m := by rel [h4]
        rw [h3.right] at h5
        exact h5
      · define
        linarith
    · assume h2 : i ∈ I (n + m) \ I n
      define
      apply Exists.intro (i - n)
      have h3 : i < n + m := h2.left
      have h4 : ¬i < n := h2.right
      have h5 : n ≤ i := by linarith
      apply And.intro
      · define
        by_contra h6
        have h7 : i - n ≥ m := by linarith
        have h8 : i - n + n ≥ m + n := by rel [h7]
        have h9 : i - n + n = i := Nat.sub_add_cancel h5
        rw [h9] at h8
        linarith
      · rw [plus_n_def]
        exact Nat.add_sub_of_le h5
  exact equinum_image h1 h2
  done

-- 11.
theorem Theorem_8_1_7 {U : Type} {A B : Set U} {n m : Nat}
    (h1 : empty (A ∩ B)) (h2 : numElts A n) (h3 : numElts B m) :
    numElts (A ∪ B) (n + m) := by
  rw [numElts_def] at h2
  rw [numElts_def] at h3
  have h4 : I (n + m) \ I n ∼ B :=
    Theorem_8_1_3_3 (Theorem_8_1_3_2 (shift_I_equinum n m)) h3
  have h5 : empty (I n ∩ (I (n + m) \ I n)) := by
    define
    by_contra h5
    obtain (x : Nat) (h6 : x ∈ I n ∩ (I (n + m) \ I n)) from h5
    have h7 : x < n := h6.left
    have h8 : ¬x < n := h6.right.right
    linarith
  have h6 : I n ∪ (I (n + m) \ I n) = I (n + m) := by
    apply Set.ext
    fix x : Nat
    apply Iff.intro
    · assume h6 : x ∈ I n ∪ I (n + m) \ I n
      by_cases on h6
      · have h7 : n ≤ n + m := by linarith
        exact Nat.lt_of_lt_of_le h6 h7
      · exact h6.left
    · assume h6 : x ∈ I (n + m)
      by_cases h7 : x < n
      · left; exact h7
      · right; exact And.intro h6 h7
  have h7 : I n ∪ (I (n + m) \ I n) ∼ A ∪ B
    := Theorem_8_1_2_2 h5 h1 h2 h4
  rw [h6] at h7
  rw [numElts_def]
  exact h7
  done

-- 12.
theorem equinum_sub {U V : Type} {A C : Set U} {B : Set V}
    (h1 : A ∼ B) (h2 : C ⊆ A) : ∃ (D : Set V), D ⊆ B ∧ C ∼ D := by
  define at h1
  obtain (R : Rel U V) (h3 : matching R A B) from h1
  apply Exists.intro (Rel_image R C)
  apply And.intro
  · define
    fix v : V
    assume h4 : v ∈ Rel_image R C
    define at h4
    obtain (u : U) (h5 : u ∈ C ∧ R u v) from h4
    exact (h3.left h5.right).right
  · apply Exists.intro (restrict_to R C)
    apply And.intro
    · define
      fix u : U; fix v : V
      assume h4 : restrict_to R C u v
      define at h4
      apply And.intro h4.left
      define
      apply Exists.intro u
      exact h4
    · apply And.intro
      · define
        fix u : U
        assume h4 : u ∈ C
        have h5 : u ∈ A := h2 h4
        have h6 : ∃! (v : V), R u v := h3.right.left h5
        exists_unique
        · obtain (v : V) (h7 : R u v) from h6.exists
          apply Exists.intro v
          define
          exact And.intro h4 h7
        · fix v1 : V
          fix v2 : V
          assume h7 : restrict_to R C u v1
          assume h8 : restrict_to R C u v2
          define at h7; define at h8
          exact h6.unique h7.right h8.right
      · define
        fix v : V
        assume h4 : v ∈ Rel_image R C
        define at h4
        obtain (u : U) (h5 : u ∈ C ∧ R u v) from h4
        have h6 : v ∈ B := (h3.left h5.right).right
        have h7 : ∃! (u : U), R u v := h3.right.right h6
        exists_unique
        · apply Exists.intro u
          define
          exact h5
        · fix u1 : U; fix u2 : U
          assume h8 : invRel (restrict_to R C) v u1
          assume h9 : invRel (restrict_to R C) v u2
          define at h8; define at h9
          exact h7.unique h8.right h9.right
  done

-- 13.
lemma finite_by_induction : ∀ (n : Nat), ∀ (B : Set U), (∃ (A : Set U), (I n ∼ A ∧ B ⊆ A))
      → ∃ (m : Nat), (m ≤ n ∧ I m ∼ B) := by
  by_induc
  · fix B : Set U
    assume h : ∃ (A : Set U), I 0 ∼ A ∧ B ⊆ A
    obtain (A : Set U) (h1 : I 0 ∼ A ∧ B ⊆ A) from h
    rw [← numElts_def, zero_elts_iff_empty] at h1
    have h2 : empty B := by
      define
      by_contra h2
      apply h1.left
      obtain (x : U) (h3 : x ∈ B) from h2
      have h3 : x ∈ A := h1.right h3
      apply Exists.intro x; exact h3
    apply Exists.intro 0
    apply And.intro
    · linarith
    · rw [← numElts_def, zero_elts_iff_empty]
      exact h2
  · fix n : Nat
    assume ih : (∀ (B : Set U), (∃ (A : Set U), I n ∼ A ∧ B ⊆ A)
      → ∃ (m : ℕ), m ≤ n ∧ I m ∼ B)
    fix B : Set U
    assume h1 : ∃ (A : Set U), I (n + 1) ∼ A ∧ B ⊆ A
    obtain (A : Set U) (h2 : I (n + 1) ∼ A ∧ B ⊆ A) from h1
    have h3 : I (n + 1) ∼ A := h2.left
    have h4 : n ∈ I (n + 1) := by
      define
      linarith
    by_cases h5 : empty B
    · apply Exists.intro 0
      apply And.intro
      · linarith
      · rw [← numElts_def, zero_elts_iff_empty]
        exact h5
    · define at h5
      double_neg at h5
      obtain (b : U) (h6 : b ∈ B) from h5
      have h7 : B \ {b} ⊆ A \ {b} := by
        define
        fix a : U
        assume h7 : a ∈ B \ {b}
        exact And.intro (h2.right h7.left) h7.right
      have h8 : I (n + 1) \ {n} ∼ A \ {b} :=
        remove_one_equinum h2.left h4 (h2.right h6)
      rw [I_diff] at h8
      have h9 : ∃ (A : Set U), I n ∼ A ∧ B \ {b} ⊆ A := by
        apply Exists.intro (A \ {b})
        exact And.intro h8 h7
      obtain (m : Nat) (h10 : m ≤ n ∧ I m ∼ B \ {b}) from ih (B \ {b}) h9
      apply Exists.intro (m + 1)
      apply And.intro
      · linarith
      · rw [← numElts_def]
        have h11 : empty ((B \ {b}) ∩ {b}) := by
          define
          by_contra h11
          obtain (x : U) (h12 : x ∈ B \ {b} ∩ {b}) from h11
          have h13 : x = b := by
            have h13 : x ∈ {b} := h12.right
            define at h13
            exact h13
          rw [h13] at h12
          apply h12.left.right
          rfl
        rw [← numElts_def] at h10
        have h12 : numElts {b} 1 := singleton_one_elt b
        have h13 : (B \ {b} ∪ {b}) = B := by
          apply Set.ext
          fix x : U
          apply Iff.intro
          · assume h13 : x ∈ B \ {b} ∪ {b}
            by_cases on h13
            · exact h13.left
            · define at h13
              rw [h13]; exact h6
          · assume h13 : x ∈ B
            by_cases h14 : x = b
            · right; define; exact h14
            · left; exact And.intro h13 h14
        have h14 : numElts (B \ {b} ∪ {b}) (m + 1) := Theorem_8_1_7 h11 h10.right h12
        rw [h13] at h14
        exact h14
  done

theorem Exercise_8_1_8b {U : Type} {A B : Set U}
    (h1 : finite A) (h2 : B ⊆ A) : finite B := by
  define
  define at h1
  obtain (n : Nat) (h3 : I n ∼ A) from h1
  have h4 : ∃ (A : Set U), (I n ∼ A ∧ B ⊆ A) := by
    apply Exists.intro A; exact And.intro h3 h2
  obtain (m : Nat) (h5 : m ≤ n ∧ I m ∼ B) from finite_by_induction n B h4
  apply Exists.intro m
  exact h5.right

-- 14.
--Hint:  Use Like_Exercise_6_2_16 from the exercises of Section 6.2
theorem Like_Exercise_6_2_16 {A : Type} (f : A → A)
    (h : one_to_one f) : ∀ (n : Nat) (B : Set A), numElts B n →
    closed f B → ∀ y ∈ B, ∃ x ∈ B, f x = y := sorry

lemma N_not_finite : ¬finite (Univ Nat) := by
  let f (n : ℕ) : ℕ := n + 1
  have h1 : one_to_one f := by
    define
    fix x1; fix x2; assume h : f x1 = f x2
    have hx1 : f x1 = x1 + 1 := by rfl
    have hx2 : f x2 = x2 + 1 := by rfl
    rw [hx1, hx2] at h
    exact Nat.add_right_cancel h
  have h2 : closed f (Univ Nat) := by
    define
    fix x : Nat
    assume h2 : x ∈ Univ Nat
    trivial
  define
  by_contra h
  obtain (n : Nat) (h3 : I n ∼ Univ Nat) from h
  rw [← numElts_def] at h3
  have h4 : ∀ y ∈ (Univ Nat), ∃ x ∈ (Univ Nat), f x = y :=
      Like_Exercise_6_2_16 f h1 n (Univ Nat) h3 h2
  have h5 : 0 ∈ (Univ Nat) := by trivial
  obtain (x : Nat) (h6 : x ∈ Univ ℕ ∧ f x = 0) from h4 0 h5
  have h7 : f x = x + 1 := by rfl
  rw [h7] at h6
  apply Nat.succ_ne_zero
  exact h6.right
  done

-- 15.
theorem denum_not_finite {U : Type} {A : Set U}
    (h : denum A) : ¬finite A := by
  by_contra h1
  apply N_not_finite
  define at h1
  obtain (n : Nat) (h2 : I n ∼ A) from h1
  rw [denum_def] at h
  have h3 : A ∼ Univ Nat := Theorem_8_1_3_2 h
  have h4 : I n ∼ Univ Nat := Theorem_8_1_3_3 h2 h3
  apply Exists.intro n
  exact h4
  done

/- Section 8.2 -/
-- 1.
lemma pair_ctble {U : Type} (a b : U) : ctble {a, b} := sorry

-- 2.
--Hint:  Use the previous exercise and Theorem_8_2_2
theorem Theorem_8_2_1_2 {U : Type} {A B : Set U}
    (h1 : ctble A) (h2 : ctble B) : ctble (A ∪ B) := sorry

-- 3.
lemma seq_cons_image {U : Type} (A : Set U) (n : Nat) :
    image (seq_cons U) (A ×ₛ (seq_by_length A n)) =
      seq_by_length A (n + 1) := sorry

-- 4.
--Hint:  Use induction on the size of A
lemma set_to_list {U : Type} {A : Set U} (h : finite A) :
    ∃ (l : List U), ∀ (x : U), x ∈ l ↔ x ∈ A := sorry

-- 5.
--Hint:  Use the previous exercise and Theorem_8_2_4
theorem Like_Exercise_8_2_4 {U : Type} {A : Set U} (h : ctble A) :
    ctble { X : Set U | X ⊆ A ∧ finite X } := sorry

-- 6.
theorem Exercise_8_2_6b (A B C : Type) :
    equinum (Univ (A × B → C)) (Univ (A → B → C)) := sorry

-- 7.
theorem Like_Exercise_8_2_7 : ∃ (P : Set (Set Nat)),
    partition P ∧ ctble P ∧ ∀ X ∈ P, ctble X := sorry

-- 8.
theorem unctbly_many_inf_set_nat :
    ¬ctble { X : Set Nat | ¬finite X } := sorry

-- 9.
theorem Exercise_8_2_8 {U : Type} {A B : Set U}
    (h : empty (A ∩ B)) : 𝒫 (A ∪ B) ∼ 𝒫 A ×ₛ 𝒫 B := sorry

/- Section 8.3 -/
-- 1.
theorem CSB_func {U V : Type} {f : U → V} {g : V → U}
    (h1 : one_to_one f) (h2 : one_to_one g) : Univ U ∼ Univ V := sorry

-- 2.
theorem intervals_equinum :
    { x : Real | 0 < x ∧ x < 1 } ∼ { x : Real | 0 < x ∧ x ≤ 1 } := sorry

-- Definitions for next six exercises
def EqRel (A : Type) : Set (BinRel A) :=
  { R : BinRel A | equiv_rel R }

def Part (A : Type) : Set (Set (Set A)) :=
  { P : Set (Set A) | partition P }

def EqRelExt (A : Type) : Set (Set (A × A)) :=
  { E : Set (A × A) | ∃ (R : BinRel A), equiv_rel R ∧ extension R = E}

def shift_and_zero (X : Set Nat) : Set Nat :=
  { x + 2 | x ∈ X } ∪ {0}

def saz_pair (X : Set Nat) : Set (Set Nat) :=
  { shift_and_zero X, (Univ Nat) \ (shift_and_zero X) }

-- 3.
theorem EqRel_equinum_Part (A : Type) : EqRel A ∼ Part A := sorry

-- 4.
theorem EqRel_equinum_EqRelExt (A : Type) :
    EqRel A ∼ EqRelExt A := sorry

-- 5.
theorem EqRel_Nat_equinum_sub_PN :
    ∃ (D : Set (Set Nat)), D ⊆ 𝒫 (Univ Nat) ∧ EqRel Nat ∼ D := sorry

-- 6.
theorem saz_pair_part (X : Set Nat) : partition (saz_pair X) := sorry

-- 7.
theorem sub_EqRel_Nat_equinum_PN :
    ∃ (C : Set (BinRel Nat)), C ⊆ EqRel Nat ∧ C ∼ 𝒫 (Univ Nat) := sorry

-- 8.
theorem EqRel_Nat_equinum_PN : EqRel Nat ∼ 𝒫 (Univ Nat) := sorry
