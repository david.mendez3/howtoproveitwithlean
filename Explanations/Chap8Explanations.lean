import HTPILib.HTPIDefs
import HTPILib.Chap6
import HTPILib.Chap7

namespace HTPI


-- Chapter 8. Infinite sets

-- Section 8.1. Equinumerous sets

/-
We say that two sets are equinumerous if there is a bijective function
between them. We will see how in Lean we need to phrase this somewhat
differently. But first, let us consider some examples of functions that
are one-to-one and onto.
-/

def fnz (n : Nat) : Int := if 2 ∣ n then ↑(n / 2) else -↑((n + 1) / 2)

/-
The behavior of this function is easy to deduce.
-/

#eval [fnz 0, fnz 1, fnz 2, fnz 3, fnz 4, fnz 5, fnz 6]
  --Answer: [0, -1, 1, -2, 2, -3, 3]

/-
One possible way of showing that a function is one-to-one and onto is
defining it s inverse. We use the function `Int.toNat` to convert
non-negative integers back into naturals.
-/

def fzn (a : Int) : Nat :=
  if a ≥ 0 then 2 * Int.toNat a else 2 * Int.toNat (-a) - 1

#eval [fzn 0, fzn (-1), fzn 1, fzn (-2), fzn 2, fzn (-3), fzn 3]
  --Answer: [0, 1, 2, 3, 4, 5, 6]

/-
To prove that `fnz` and `fzn` are inverses of each other we first
will show some lemmas making it easier to compute these functions.
-/

lemma fnz_even (k : Nat) : fnz (2 * k) = ↑k := by
  have h1 : 2 ∣ 2 * k := by
    apply Exists.intro k
    rfl
  have h2 : fnz (2 * k) = if 2 ∣ 2 * k then ↑(2 * k / 2)
    else -↑((2 * k + 1) / 2) := by rfl
  rw [if_pos h1] at h2
  have h3 : 0 < 2 := by linarith
  rw [Nat.mul_div_cancel_left k h3] at h2
  exact h2
  done

lemma fnz_odd (k : Nat) : fnz (2 * k + 1) = -↑(k + 1) := by
  have h1 : ¬2 ∣ 2 * k + 1 := by
    have h1 : nat_odd (2 * k + 1) := by
      define
      apply Exists.intro k
      rfl
    have h2 : ¬((nat_even (2 * k + 1)) ∧ nat_odd (2 * k + 1)) :=
      Exercise_6_1_16a2 (2 * k + 1)
    demorgan at h2
    disj_syll h2 h1
    exact h2
  have h2 : fnz (2 * k + 1) = if 2 ∣ 2 * k + 1 then ↑((2 * k + 1) / 2)
    else -↑((2 * k + 1 + 1) / 2) := by rfl
  rw [if_neg h1] at h2
  have h3 : 2 * k + 1 + 1 = 2 * k + 2 * 1 := by rfl
  have h4 : 0 < 2 := by linarith
  rw [h3, ← mul_add, Nat.mul_div_cancel_left (k + 1) h4] at h2
  exact h2
  done

lemma fzn_nat (k : Nat) : fzn ↑k = 2 * k := by rfl

lemma fzn_neg_succ_nat (k : Nat) : fzn (-↑(k + 1)) = 2 * k + 1 := by rfl

lemma fzn_fnz : fzn ∘ fnz = id := by
  apply funext
  fix n : Nat
  rw [comp_def]
  have h1 : nat_even n ∨ nat_odd n := Exercise_6_1_16a1 n
  by_cases on h1
  · -- h1 : nat_even n
    obtain (k : Nat) (h2 : n = 2 * k) from h1
    rw [h2, fnz_even, fzn_nat]
    rfl
  · -- Case 2. h1 : nat_odd n
    obtain (k : Nat) (h2 : n = 2 * k + 1) from h1
    rw [h2, fnz_odd, fzn_neg_succ_nat]
    rfl

lemma fnz_fzn : fnz ∘ fzn = id  := by
  apply funext
  fix a : Int
  rw [comp_def]
  have h1 : fzn a = if a ≥ 0 then 2 * Int.toNat a else 2 * Int.toNat (-a) - 1
    := by rfl
  by_cases h : a ≥ 0
  · rw [if_pos h] at h1
    rw [h1, fnz_even, Int.toNat_of_nonneg h]
    rfl
  · rw [if_neg h] at h1
    have h2 : a ≤ -1 := by linarith
    set a' : Int := a + 1
    have h3 : a = a' - 1 := by ring
    have h4 : a' ≤ 0 := by
      rw [h3] at h2
      linarith
    have h5 : -(a' - 1) = -a' + 1 := by ring
    have h6 : 0 ≤ -a' := by linarith
    have h7 : (0 : Int) ≤ (1 : Int) := by linarith
    have h8 : ∀ (n : Int), 0 ≤ -n → Int.toNat (-n + 1) = Int.toNat (-n) + 1 := by
      fix n; assume h8 : 0 ≤ -n
      have h9 : Int.toNat (-n + 1) = Int.toNat (-n) + Int.toNat 1
        := Int.toNat_add h8 h7
      rw [Int.toNat_one] at h9
      exact h9
    have h9 : 2 * (Int.toNat (-a') + 1) - 1 = 2 * Int.toNat (-a') + 1 := by
      calc 2 * (Int.toNat (-a') + 1) - 1
        _ = 2 * Int.toNat (-a') + 2 * 1 - 1 := by rw [mul_add]
        _ = 2 * Int.toNat (-a') + 2 - 1 := by rw [mul_one]
        _ = 2 * Int.toNat (-a') + 1 := by norm_num
    rw [h1, h3, h5, h8 a' h6, h9, fnz_odd]
    have h10 : a' = a + 1 := by rfl
    rw [h10]
    have h11 : 0 ≤ -(a + 1) := by linarith
    have h12 : 0 ≤ -(a + 1) + 1 := by linarith
    rw [← h8 (a + 1) h11, Int.toNat_of_nonneg h12]
    have h13 : -(-(a + 1) + 1) = a := by ring
    have h14 : a + 1 - 1 = a := by ring
    rw [h13, h14]
    rfl
  done

/-
And since we have inverses, we can now use results from chapter 5.
-/

lemma fzn_one_one : one_to_one fzn := Theorem_5_3_3_1 fzn fnz fnz_fzn

lemma fzn_onto : onto fzn := Theorem_5_3_3_2 fzn fnz fzn_fnz

lemma fnz_one_one : one_to_one fnz := Theorem_5_3_3_1 fnz fzn fzn_fnz

lemma fnz_onto : onto fnz := Theorem_5_3_3_2 fnz fzn fnz_fzn

/-
We now show the same for a function from `Nat × Nat` to `Nat`.
-/

def tri (k : Nat) : Nat := k * (k + 1) / 2

def fnnn (p : Nat × Nat) : Nat := tri (p.1 + p.2) + p.1

lemma fnnn_def (a b : Nat) : fnnn (a, b) = tri (a + b) + a := by rfl

#eval [fnnn (0, 0), fnnn (0, 1), fnnn (1, 0), fnnn (0, 2), fnnn (1, 1)]
  --Answer: [0, 1, 2, 3, 4]

/-
Again, some lemmas to facilitate computations.
-/

lemma tri_step (k : Nat) : tri (k + 1) = tri k + k + 1 := by
  have h1 : tri (k + 1) = (k + 1) * (k + 2) / 2 := by rfl
  have h2 : tri k + k + 1 = k * (k + 1) / 2 + k + 1 := by rfl
  rw [h1, h2]
  rw [add_mul, mul_add, one_mul, mul_add, mul_one]
  have h3 : k * k + k * 2 + (k + 2) = (k * k + k) + 2 * (k + 1) := by
    calc k * k + k * 2 + (k + 2)
      _ = k * k + k * 2 + k + 2 := by ring
      _ = k * k + k + 2 * k + 2 := by ring
      _ = (k * k + k) + 2 * (k + 1) := by ring
  rw [h3]
  have h4 : 2 ∣ 2 * (k + 1) := by
    apply Exists.intro (k + 1)
    rfl
  have h5 : 0 < 2 := by linarith
  rw [Nat.add_div_of_dvd_left h4, Nat.mul_div_cancel_left (k + 1) h5]
  rfl

lemma tri_incr {j k : Nat} (h1 : j ≤ k) : tri j ≤ tri k := by
  have h2 : tri j = j * (j + 1) / 2 := by rfl
  have h3 : tri k = k * (k + 1) / 2 := by rfl
  rw [h2, h3]
  have h4 : j + 1 ≤ k + 1 := by linarith
  have h5 : j * (j + 1) ≤ k * (k + 1) := by
    calc j * (j + 1)
      _ ≤ k * (j + 1) := by rel [h1]
      _ ≤ k * (k + 1) := by rel [h4]
  rel [h5]
  done

lemma le_of_fnnn_eq {a1 b1 a2 b2 : Nat}
    (h1 : fnnn (a1, b1) = fnnn (a2, b2)) : a1 + b1 ≤ a2 + b2 := by
  by_contra h2
  have h3 : a2 + b2 + 1 ≤ a1 + b1 := by linarith
  have h4 : fnnn (a2, b2) < fnnn (a1, b1) :=
    calc fnnn (a2, b2)
      _ = tri (a2 + b2) + a2 := by rfl
      _ < tri (a2 + b2) + (a2 + b2) + 1 := by linarith
      _ = tri (a2 + b2 + 1) := (tri_step _).symm
      _ ≤ tri (a1 + b1) := tri_incr h3
      _ ≤ tri (a1 + b1) + a1 := by linarith
      _ = fnnn (a1, b1) := by rfl
  linarith
  done

lemma fnnn_one_one : one_to_one fnnn := by
  fix (a1, b1) : Nat × Nat
  fix (a2, b2) : Nat × Nat
  assume h1 : fnnn (a1, b1) = fnnn (a2, b2)
  have h2 : a1 + b1 ≤ a2 + b2 := le_of_fnnn_eq h1
  have h3 : a2 + b2 ≤ a1 + b1 := le_of_fnnn_eq h1.symm
  have h4 : a1 + b1 = a2 + b2 := by linarith
  rewrite [fnnn_def, fnnn_def, h4] at h1
  have h6 : a1 = a2 := Nat.add_left_cancel h1
  rw [h6] at h4
  have h7 : b1 = b2 := Nat.add_left_cancel h4
  rw [h6, h7]
  done

lemma fnnn_onto : onto fnnn := by
  define
  by_induc
  · -- Base Case
    apply Exists.intro (0, 0)
    rfl
  · -- Induction Step
    fix n : Nat
    assume ih : ∃ (x : Nat × Nat), fnnn x = n
    obtain ((a, b) : Nat × Nat) (h1 : fnnn (a, b) = n) from ih
    by_cases h2 : b = 0
    · -- Case 1. h2 : b = 0
      apply Exists.intro (0, a + 1)
      show fnnn (0, a + 1) = n + 1 from
        calc fnnn (0, a + 1)
          _ = tri (0 + (a + 1)) + 0 := by rfl
          _ = tri (a + 1) := by ring
          _ = tri a + a + 1 := tri_step a
          _ = tri (a + 0) + a + 1 := by ring
          _ = fnnn (a, b) + 1 := by rw [h2, fnnn_def]
          _ = n + 1 := by rw [h1]
    · -- Case 2. h2 : b ≠ 0
      obtain (k : Nat) (h3 : b = k + 1) from
        exists_eq_add_one_of_ne_zero h2
      apply Exists.intro (a + 1, k)
      show fnnn (a + 1, k) = n + 1 from
        calc fnnn (a + 1, k)
          _ = tri (a + 1 + k) + (a + 1) := by rfl
          _ = tri (a + (k + 1)) + a + 1 := by ring
          _ = tri (a + b) + a + 1 := by rw [h3]
          _ = fnnn (a, b) + 1 := by rfl
          _ = n + 1 := by rw [h1]
  done

/-
Despite of this, we use a different definition for equinumerousness.
1. The domain of a function in Lean must be a type, but we want to talk about
   equinumerousness of sets.
2. Lean expects functions to be computable.

Suppose we have `U` and `V` types and we have `A : Set U` and `B : Set V`. We
will define `A` to be equinumerous to `B` if there is a relation `R` from `U`
to `V` inducing a bijective correspondence between elements of `A` and `B`.

We impose three requirements on `R : Rel U V`.
-/

/-
1. `R` should hold only between elements of `A` and `B`.
-/
def rel_within' {U V : Type} (R : Rel U V) (A : Set U) (B : Set V) : Prop :=
  ∀ ⦃x : U⦄ ⦃y : V⦄, R x y → x ∈ A ∧ y ∈ B

/-
2. Every element of A is related by R to exactly one element.
   We say that R is functional on A.
-/
def fcnl_on' {U V : Type} (R : Rel U V) (A : Set U) : Prop :=
  ∀ ⦃x : U⦄, x ∈ A → ∃! (y : V), R x y

/-
3. For every element of B, there is exactly one element related to it by R.
-/
def invRel' {U V : Type} (R : Rel U V) : Rel V U :=
  RelFromExt (inv (extension R))

lemma invRel_def' {U V : Type} (R : Rel U V) (u : U) (v : V) :
    invRel R v u ↔ R u v := by rfl

/-
We say that a relation is a matching if it satisfies the three points above.
-/

def matching' {U V : Type} (R : Rel U V) (A : Set U) (B : Set V) : Prop :=
  rel_within' R A B ∧ fcnl_on' R A ∧ fcnl_on' (invRel R) B

/-
Finally, two sets are equinumerous if there is a matching between them.
-/
def equinum' {U V : Type} (A : Set U) (B : Set V) : Prop :=
  ∃ (R : Rel U V), matching R A B

/-
The following notation line is included.
`notation:50  A:50 " ∼ " B:50 => equinum A B`
Therefore, to say that `A` and `B` are equinumerous we may denote `A ∼ B`.
-/

/-
The results that we showed earlier cannot be used to show that `Int ∼ Nat` or
`Nat ∼ Nat × Nat` because these are types. We must talk about the sets of all
objects of those types. Thus we introduce the following.
-/

def Univ' (U : Type) : Set U := { x : U | True }

lemma elt_Univ {U : Type} (u : U) :
    u ∈ Univ' U := by trivial

/-
We may call `Univ U` the universal set of the type U.
Now we proceed to proving that `Univ Int ∼ Univ Nat` and
`Univ (Nat × Nat) ∼ Univ Nat`. To do so, we need to convert the functions
from earlier into relations. We use the following.
-/

def RelWithinFromFunc' {U V : Type} (f : U → V) (A : Set U)
  (x : U) (y : V) : Prop := x ∈ A ∧ f x = y

/-
Thus, `RelWithinFromFunc f A` is a relation from `U` to `V` that relates
any `x : A` to `f x`.
-/

def one_one_on' {U V : Type} (f : U → V) (A : Set U) : Prop :=
  ∀ ⦃x1 x2 : U⦄, x1 ∈ A → x2 ∈ A → f x1 = f x2 → x1 = x2

theorem equinum_image' {U V : Type} {A : Set U} {B : Set V} {f : U → V}
    (h1 : one_one_on' f A) (h2 : image f A = B) : A ∼ B := by
  rw [← h2]
  define
  set R : Rel U V := RelWithinFromFunc' f A
  apply Exists.intro R
  define
  apply And.intro
  · -- Proof of rel_within
    define
    fix x : U; fix y : V
    assume h3 : R x y
    define at h3
    apply And.intro h3.left
    define
    apply Exists.intro x
    exact h3
  · apply And.intro
    · -- proof of fcnl_on R A
      define
      fix x : U
      assume h3 : x ∈ A
      exists_unique
      · -- Existence
        apply Exists.intro (f x)
        define
        apply And.intro h3
        rfl
      · -- Uniqueness
        fix y1 : V; fix y2 : V
        assume h4 : R x y1
        assume h5 : R x y2
        define at h4; define at h5
        rw [h4.right] at h5
        exact h5.right
    · -- proof of fcnl_on (invRel R) (image f A)
      define
      fix y : V
      assume h3 : y ∈ image f A
      obtain (x : U) (h4 : x ∈ A ∧ f x = y) from h3
      exists_unique
      · -- Existence
        apply Exists.intro x
        define
        exact h4
      · -- Uniqueness
        fix x1 : U; fix x2 : U
        assume h5 : invRel R y x1
        assume h6 : invRel R y x2
        define at h5; define at h6
        rw [← h6.right] at h5
        exact h1 h5.left h6.left h5.right
  done

/-
And now we want to use `Univ U` for the set `A` in the theorem above.
-/

lemma one_one_on_of_one_one {U V : Type} {f : U → V}
    (h : one_to_one f) (A : Set U) : one_one_on f A := by
  define
  fix x1 : U; fix x2 : U
  assume h1 : x1 ∈ A
  assume h2 : x2 ∈ A
  assume h3 : f x1 = f x2
  exact h x1 x2 h3
  done

theorem equinum_Univ {U V : Type} {f : U → V}
    (h1 : one_to_one f) (h2 : onto f) : Univ U ∼ Univ V := by
  have h3 : image f (Univ U) = Univ V := by
    apply Set.ext
    fix v : V
    apply Iff.intro
    · -- (→)
      assume h3 : v ∈ image f (Univ U)
      exact elt_Univ v
    · -- (←)
      assume h3 : v ∈ Univ V
      obtain (u : U) (h4 : f u = v) from h2 v
      apply Exists.intro u
      apply And.intro _ h4
      exact elt_Univ u
  exact equinum_image (one_one_on_of_one_one h1 (Univ U)) h3

theorem Z_equinum_N : Univ Int ∼ Univ Nat :=
  equinum_Univ fzn_one_one fzn_onto

theorem NxN_equinum_N : Univ (Nat × Nat) ∼ Univ Nat :=
  equinum_Univ fnnn_one_one fnnn_onto

/-
We now prove that equinumerousness is an equivelence relation. We begin with
reflexivity.
-/

lemma id_one_one_on' {U : Type} (A : Set U) : one_one_on id A := by
  define
  fix x1 : U; fix x2 : U
  assume h1 : x1 ∈ A
  assume h2 : x2 ∈ A
  assume h3 : id x1 = id x2
  exact h3
  done

lemma image_id' {U : Type} (A : Set U) : image id A = A := by
  apply Set.ext
  fix x : U
  apply Iff.intro
  · -- (→)
    assume h : x ∈ image id A
    define at h
    obtain (x' : U) (h1 : x' ∈ A ∧ id x' = x) from h
    rw [← h1.right]
    exact h1.left
  · -- (←)
    assume h1 : x ∈ A
    define
    apply Exists.intro x
    apply And.intro h1
    rfl
  done

theorem Theorem_8_1_3_1' {U : Type} (A : Set U) : A ∼ A :=
  equinum_image (id_one_one_on A) (image_id A)

/-
For symmetry, we show that the inverse of a matching is a matching.
-/

lemma inv_inv' {U V : Type} (R : Rel U V) : invRel (invRel R) = R := by rfl

lemma inv_match' {U V : Type} {R : Rel U V} {A : Set U} {B : Set V}
    (h : matching R A B) : matching (invRel R) B A := by
  define
  define at h
  apply And.intro
  · define
    fix y : V; fix x : U
    assume h1 : invRel R y x
    define at h1
    have h2 : x ∈ A ∧ y ∈ B := h.left h1
    exact And.intro h2.right h2.left
  · rw [inv_inv]
    exact And.intro h.right.right h.right.left
  done

theorem Theorem_8_1_3_2' {U V : Type} {A : Set U} {B : Set V}
    (h : A ∼ B) : B ∼ A := by
  obtain (R : Rel U V) (h1 : matching R A B) from h
  apply Exists.intro (invRel R)
  exact inv_match h1
  done

/-
The proof of transitivity is a bit more involved. We separate the existence and
uniqueness part of `fcnl_on`.
-/

lemma fcnl_exists' {U V : Type} {R : Rel U V} {A : Set U} {x : U}
    (h1 : fcnl_on R A) (h2 : x ∈ A) : ∃ (y : V), R x y := by
  define at h1
  obtain (y : V) (h3 : R x y)
    (h4 : ∀ (y_1 y_2 : V), R x y_1 → R x y_2 → y_1 = y_2) from h1 h2
  exact Exists.intro y h3
  done

lemma fcnl_unique' {U V : Type}
    {R : Rel U V} {A : Set U} {x : U} {y1 y2 : V} (h1 : fcnl_on R A)
    (h2 : x ∈ A) (h3 : R x y1) (h4 : R x y2) : y1 = y2 := by
  define at h1
  obtain (z : V) (h5 : R x z)
    (h6 : ∀ (y_1 y_2 : V), R x y_1 → R x y_2 → y_1 = y_2) from h1 h2
  exact h6 y1 y2 h3 h4
  done

/-
We also need to show that the composition of matchings is a matching.
-/

def compRel' {U V W : Type} (S : Rel V W) (R : Rel U V) : Rel U W :=
  RelFromExt (comp (extension S) (extension R))

lemma compRel_def' {U V W : Type}
    (S : Rel V W) (R : Rel U V) (u : U) (w : W) :
    compRel S R u w ↔ ∃ (x : V), R u x ∧ S x w := by rfl

lemma inv_comp' {U V W : Type} (R : Rel U V) (S : Rel V W) :
    invRel (compRel S R) = compRel (invRel R) (invRel S) :=
  calc invRel (compRel S R)
    _ = RelFromExt (inv (comp (extension S) (extension R))) := by rfl
    _ = RelFromExt (comp (inv (extension R)) (inv (extension S))) := by
          rw [Theorem_4_2_5_5]
    _ = compRel (invRel R) (invRel S) := by rfl

lemma comp_fcnl' {U V W : Type} {R : Rel U V} {S : Rel V W}
    {A : Set U} {B : Set V} {C : Set W} (h1 : matching R A B)
    (h2 : matching S B C) : fcnl_on (compRel S R) A := by
  define; define at h1; define at h2
  fix a : U
  assume h3 : a ∈ A
  obtain (b : V) (h4 : R a b) from fcnl_exists h1.right.left h3
  have h5 : a ∈ A ∧ b ∈ B := h1.left h4
  obtain (c : W) (h6 : S b c) from fcnl_exists h2.right.left h5.right
  exists_unique
  · -- Existence
    apply Exists.intro c
    rw [compRel_def]
    exact Exists.intro b (And.intro h4 h6)
  · -- Uniqueness
    fix c1 : W; fix c2 : W
    assume h7 : compRel S R a c1
    assume h8 : compRel S R a c2
    rw [compRel_def] at h7
    rw [compRel_def] at h8
    obtain (b1 : V) (h9 : R a b1 ∧ S b1 c1) from h7
    obtain (b2 : V) (h10 : R a b2 ∧ S b2 c2) from h8
    have h11 : b1 = b := fcnl_unique h1.right.left h3 h9.left h4
    have h12 : b2 = b := fcnl_unique h1.right.left h3 h10.left h4
    rw [h11] at h9
    rw [h12] at h10
    exact fcnl_unique h2.right.left h5.right h9.right h10.right
  done

lemma comp_match' {U V W : Type} {R : Rel U V} {S : Rel V W}
    {A : Set U} {B : Set V} {C : Set W} (h1 : matching R A B)
    (h2 : matching S B C) : matching (compRel S R) A C := by
  define
  apply And.intro
  · -- Proof of rel_within (compRel S R) A C
    define
    fix a : U; fix c : W
    assume h3 : compRel S R a c
    rw [compRel_def] at h3
    obtain (b : V) (h4 : R a b ∧ S b c) from h3
    have h5 : a ∈ A ∧ b ∈ B := h1.left h4.left
    have h6 : b ∈ B ∧ c ∈ C := h2.left h4.right
    exact And.intro h5.left h6.right
  · -- Proof of fcnl_on statements
    apply And.intro
    · -- Proof of fcnl_on (compRel S R) A
      show fcnl_on (compRel S R) A from comp_fcnl h1 h2
    · -- Proof of fcnl_on (invRel (compRel S R)) Z
      rw [inv_comp]
      have h3 : matching (invRel R) B A := inv_match h1
      have h4 : matching (invRel S) C B := inv_match h2
      exact comp_fcnl h4 h3
  done

theorem Theorem_8_1_3_3' {U V W : Type} {A : Set U} {B : Set V} {C : Set W}
    (h1 : A ∼ B) (h2 : B ∼ C) : A ∼ C := by
  obtain (R : Rel U V) (h3 : matching R A B) from h1
  obtain (S : Rel V W) (h4 : matching S B C) from h2
  apply Exists.intro (compRel S R)
  exact comp_match h3 h4
  done

/-
Now we can define finiteness, denumerable and countable.
-/

def I' (n : Nat) : Set Nat := { k : Nat | k < n }

lemma I_def' (k n : Nat) : k ∈ I n ↔ k < n := by rfl

def finite' {U : Type} (A : Set U) : Prop :=
  ∃ (n : Nat), I n ∼ A

def denum' {U : Type} (A : Set U) : Prop :=
  Univ Nat ∼ A

lemma denum_def' {U : Type} (A : Set U) : denum A ↔ Univ Nat ∼ A := by rfl

def ctble' {U : Type} (A : Set U) : Prop :=
  finite A ∨ denum A

/-
We now deal with some characterizations of these concepts. `enum A` should
relate a natural number `s` to `n` if and only if `s` is the number of
elements of `A` which are smaller than `n`. Thus, we first introduce a
proposition `num_elts_below A n`, and then we use this to define the
relation `enum A`. We then use this to show that `A` is countable.
-/

/-
The function is defined recursively.
-/

def num_elts_below (A : Set Nat) (m s : Nat) : Prop :=
  match m with
    | 0 => s = 0
    | n + 1 => (n ∈ A ∧ 1 ≤ s ∧ num_elts_below A n (s - 1)) ∨
                (n ∉ A ∧ num_elts_below A n s)

def enum (A : Set Nat) (s n : Nat) : Prop := n ∈ A ∧ num_elts_below A n s

lemma neb_exists (A : Set Nat) :
    ∀ (n : Nat), ∃ (s : Nat), num_elts_below A n s := sorry

lemma bdd_subset_nat_match {A : Set Nat} {m s : Nat}
    (h1 : ∀ n ∈ A, n < m) (h2 : num_elts_below A m s) :
    matching (enum A) (I s) A := sorry

lemma bdd_subset_nat {A : Set Nat} {m s : Nat}
    (h1 : ∀ n ∈ A, n < m) (h2 : num_elts_below A m s) :
    I s ∼ A := Exists.intro (enum A) (bdd_subset_nat_match h1 h2)

lemma unbdd_subset_nat_match {A : Set Nat}
    (h1 : ∀ (m : Nat), ∃ n ∈ A, n ≥ m) :
    matching (enum A) (Univ Nat) A := sorry

lemma unbdd_subset_nat {A : Set Nat}
    (h1 : ∀ (m : Nat), ∃ n ∈ A, n ≥ m) :
    denum A := Exists.intro (enum A) (unbdd_subset_nat_match h1)

lemma subset_nat_ctble (A : Set Nat) : ctble A := by
  define
  by_cases h1 : ∃ (m : Nat), ∀ n ∈ A, n < m
  · -- Case 1. h1 : ∃ (m : Nat), ∀ n ∈ A, n < m
    apply Or.inl
    obtain (m : Nat) (h2 : ∀ n ∈ A, n < m) from h1
    obtain (s : Nat) (h3 : num_elts_below A m s) from neb_exists A m
    apply Exists.intro s
    exact bdd_subset_nat h2 h3
  · -- Case 2. h1 : ¬∃ (m : Nat), ∀ n ∈ A, n < m
    apply Or.inr
    push_neg at h1
    exact unbdd_subset_nat h1
  done

/-
As a consequence, we get another characterization: a set is countable
if and only if it is equinumerous with some subset of natural numbers.
-/

lemma ctble_of_equinum_ctble {U V : Type} {A : Set U} {B : Set V}
    (h1 : A ∼ B) (h2 : ctble A) : ctble B := by
  define
  define at h2
  by_cases on h2
  · -- A finite
    define at h2
    obtain (n : Nat) (h3 : I n ∼ A) from h2
    have h4 : I n ∼ B := Theorem_8_1_3_3' h3 h1
    left; define; apply Exists.intro n; exact h4
  · -- A denumerable
    rw [denum_def] at h2
    have h3 : Univ ℕ ∼ B := Theorem_8_1_3_3' h2 h1
    rw [← denum_def] at h3
    right; exact h3
  done

lemma ctble_iff_equinum_set_nat {U : Type} (A : Set U) :
    ctble A ↔ ∃ (I : Set Nat), I ∼ A := by
  apply Iff.intro
  · -- (→)
    assume h1 : ctble A
    define at h1
    by_cases on h1
    · -- Case 1. h1 : finite A
      define at h1
      obtain (n : Nat) (h2 : I n ∼ A) from h1
      exact Exists.intro (I n) h2
    · -- Case 2. h1 : denum A
      rw [denum_def] at h1
      exact Exists.intro (Univ Nat) h1
  · -- (←)
    assume h1 : ∃ (I : Set Nat), I ∼ A
    obtain (I : Set Nat) (h2 : I ∼ A) from h1
    have h3 : ctble I := subset_nat_ctble I
    exact ctble_of_equinum_ctble h2 h3
  done

/-
Moving on, we now have results equivalent to countability of a set.
-/

def unique_val_on_N {U : Type} (R : Rel Nat U) : Prop :=
  ∀ ⦃n : Nat⦄ ⦃x1 x2 : U⦄, R n x1 → R n x2 → x1 = x2

def nat_rel_onto {U : Type} (R : Rel Nat U) (A : Set U) : Prop :=
  ∀ ⦃x : U⦄, x ∈ A → ∃ (n : Nat), R n x

def fcnl_onto_from_nat {U : Type} (R : Rel Nat U) (A : Set U) : Prop :=
  unique_val_on_N R ∧ nat_rel_onto R A

def fcnl_one_one_to_nat {U : Type} (R : Rel U Nat) (A : Set U) : Prop :=
  fcnl_on R A ∧ ∀ ⦃x1 x2 : U⦄ ⦃n : Nat⦄,
    (x1 ∈ A ∧ R x1 n) → (x2 ∈ A ∧ R x2 n) → x1 = x2

/-
The plan is to show that if A has type `Set U`, the following are
equivalent.


1.  ctble A
2.  ∃ (R : Rel Nat U), fcnl_onto_from_nat R A
3.  ∃ (R : Rel U Nat), fcnl_one_one_to_nat R A

We prove 1 → 2 → 3 → 1. This is 1 → 2.
-/

theorem Theorem_8_1_5_1_to_2 {U : Type} {A : Set U} (h1 : ctble A) :
    ∃ (R : Rel Nat U), fcnl_onto_from_nat R A := by
  rw [ctble_iff_equinum_set_nat] at h1
  obtain (I : Set Nat) (h2 : I ∼ A) from h1
  obtain (R : Rel Nat U) (h3 : matching R I A) from h2
  define at h3
  apply Exists.intro R
  define
  apply And.intro
  · -- Proof of unique_val_on_N R
    define
    fix n : Nat; fix x1 : U; fix x2 : U
    assume h4 : R n x1
    assume h5 : R n x2
    have h6 : n ∈ I ∧ x1 ∈ A := h3.left h4
    exact fcnl_unique h3.right.left h6.left h4 h5
    done
  · -- Proof of nat_rel_onto R A
    define
    fix x : U
    assume h4 : x ∈ A  --Goal : ∃ (n : Nat), R n x
    exact fcnl_exists h3.right.right h4
  done

/-
Now for 2 → 3.
-/

def least_rel_to {U : Type} (S : Rel Nat U) (x : U) (n : Nat) : Prop :=
  S n x ∧ ∀ (m : Nat), S m x → n ≤ m

lemma exists_least_rel_to {U : Type} {S : Rel Nat U} {x : U}
    (h1 : ∃ (n : Nat), S n x) : ∃ (n : Nat), least_rel_to S x n := by
  set W : Set Nat := { n : Nat | S n x }
  have h2 : ∃ (n : Nat), n ∈ W := h1
  exact well_ord_princ W h2
  done

theorem Theorem_8_1_5_2_to_3 {U : Type} {A : Set U}
    (h1 : ∃ (R : Rel Nat U), fcnl_onto_from_nat R A) :
    ∃ (R : Rel U Nat), fcnl_one_one_to_nat R A := by
  obtain (S : Rel Nat U) (h2 : fcnl_onto_from_nat S A) from h1
  define at h2  --h2 : unique_val_on_N S ∧ nat_rel_onto S A
  set R : Rel U Nat := least_rel_to S
  apply Exists.intro R
  define
  apply And.intro
  · -- Proof of fcnl_on R A
    define
    fix x : U
    assume h4 : x ∈ A
    exists_unique
    · -- Existence
      have h5 : ∃ (n : Nat), S n x := h2.right h4
      show ∃ (n : Nat), R x n from exists_least_rel_to h5
    · -- Uniqueness
      fix n1 : Nat; fix n2 : Nat
      assume h5 : R x n1
      assume h6 : R x n2
      define at h5
      define at h6
      have h7 : n1 ≤ n2 := h5.right n2 h6.left
      have h8 : n2 ≤ n1 := h6.right n1 h5.left
      linarith
  · -- Proof of one-to-one
    fix x1 : U; fix x2 : U; fix n : Nat
    assume h4 : x1 ∈ A ∧ R x1 n
    assume h5 : x2 ∈ A ∧ R x2 n
    have h6 : R x1 n := h4.right
    have h7 : R x2 n := h5.right
    define at h6
    define at h7
    exact h2.left h6.left h7.left
  done

/-
And 3 → 1
-/

def restrict_to {U V : Type} (S : Rel U V) (A : Set U)
  (x : U) (y : V) : Prop := x ∈ A ∧ S x y

theorem Theorem_8_1_5_3_to_1 {U : Type} {A : Set U}
    (h1 : ∃ (R : Rel U Nat), fcnl_one_one_to_nat R A) :
    ctble A := by
  obtain (S : Rel U Nat) (h2 : fcnl_one_one_to_nat S A) from h1
  define at h2
  rw [ctble_iff_equinum_set_nat]
  set R : Rel Nat U := invRel (restrict_to S A)
  set I : Set Nat := { n : Nat | ∃ (x : U), R n x }
  apply Exists.intro I
  define
  apply Exists.intro R
  define
  apply And.intro
  · -- Proof of rel_within R I A
    define
    fix n : Nat; fix x : U
    assume h3 : R n x
    apply And.intro
    · -- Proof that n ∈ I
      define
      exact Exists.intro x h3
    · -- Proof that x ∈ A
      define at h3
      exact h3.left
  · -- Proofs of fcnl_ons
    apply And.intro
    · -- Proof of fcnl_on R I
      define
      fix n : Nat
      assume h3 : n ∈ I
      exists_unique
      · -- Existence
        define at h3
        exact h3
      · -- Uniqueness
        fix x1 : U; fix x2 : U
        assume h4 : R n x1
        assume h5 : R n x2
        define at h4
        define at h5
        exact h2.right h4 h5
    · -- Proof of fcnl_on (invRel R) A
      define
      fix x : U
      assume h3 : x ∈ A
      exists_unique
      · -- Existence
        obtain (y : Nat) (h4 : S x y) from fcnl_exists h2.left h3
        apply Exists.intro y
        define
        exact And.intro h3 h4
      · -- Uniqueness
        fix n1 : Nat; fix n2 : Nat
        assume h4 : invRel R x n1
        assume h5 : invRel R x n2
        define at h4
        define at h5
        exact fcnl_unique h2.left h3 h4.right h5.right
  done

/-
And we can finally establish the equivalence.
-/

theorem Theorem_8_1_5_2 {U : Type} (A : Set U) :
    ctble A ↔ ∃ (R : Rel Nat U), fcnl_onto_from_nat R A := by
  apply Iff.intro
  · -- (→)
    assume h1 : ctble A
    exact Theorem_8_1_5_1_to_2 h1
  · -- (←)
    assume h1 : ∃ (R : Rel Nat U), fcnl_onto_from_nat R A
    have h2 : ∃ (R : Rel U Nat), fcnl_one_one_to_nat R A :=
      Theorem_8_1_5_2_to_3 h1
    exact Theorem_8_1_5_3_to_1 h2
  done

theorem Theorem_8_1_5_3 {U : Type} (A : Set U) :
    ctble A ↔ ∃ (R : Rel U Nat), fcnl_one_one_to_nat R A := by
  apply Iff.intro
  · -- (→)
    assume h1 : ctble A
    have h2 : ∃ (R : Rel ℕ U), fcnl_onto_from_nat R A := Theorem_8_1_5_1_to_2 h1
    exact Theorem_8_1_5_2_to_3 h2
  · -- (←)
    assume h1 : ∃ (R : Rel U ℕ), fcnl_one_one_to_nat R A
    exact Theorem_8_1_5_3_to_1 h1
  done

/-
We finish this section by proving that the set of rational numbers is
denumerable. The strategy is to define a one-to-one function from `Rat` to `Nat`.
That is the `fnnn` function applied to numerator and denominator.
-/

def fqn (q : Rat) : Nat := fnnn (fzn q.num, q.den)

lemma fqn_def (q : Rat) : fqn q = fnnn (fzn q.num, q.den) := by rfl

lemma fqn_one_one : one_to_one fqn := by
  define
  fix q1 : Rat; fix q2 : Rat
  assume h1 : fqn q1 = fqn q2
  rw [fqn_def, fqn_def] at h1
  have h2 : (fzn q1.num, q1.den) = (fzn q2.num, q2.den) :=
    fnnn_one_one _ _ h1
  have h3 : fzn q1.num = fzn q2.num ∧ q1.den = q2.den :=
    Prod.mk.inj h2
  have h4 : q1.num = q2.num := fzn_one_one _ _ h3.left
  exact Rat.ext q1 q2 h4 h3.right
  done

lemma image_fqn_unbdd :
    ∀ (m : Nat), ∃ n ∈ image fqn (Univ Rat), n ≥ m := by
  fix m : Nat
  set n : Nat := fqn ↑m
  apply Exists.intro n
  apply And.intro
  · -- Proof that n ∈ image fqn (Univ Rat)
    define
    apply Exists.intro ↑m
    apply And.intro (elt_Univ (↑m : Rat))
    rfl
  · -- Proof that n ≥ m
    show n ≥ m from
      calc n
        _ = tri (2 * m + 1) + 2 * m := by rfl
        _ ≥ m := by linarith
  done

theorem Theorem_8_1_6 : denum (Univ Rat) := by
  set I : Set Nat := image fqn (Univ Rat)
  have h1 : Univ Nat ∼ I := unbdd_subset_nat image_fqn_unbdd
  have h2 : image fqn (Univ Rat) = I := by rfl
  have h3 : Univ Rat ∼ I :=
    equinum_image (one_one_on_of_one_one fqn_one_one (Univ Rat)) h2
  have h4 : I ∼ Univ Rat := Theorem_8_1_3_2 h3
  exact Theorem_8_1_3_3 h1 h4
  done


-- Section 8.1½. Debts paid

/-
We now settle questions that were earlier said to be solved in a future time.
-/

/-
First, in Section 6.2 we defined a proposition `numElts A n` to express the idea
that a set `A` has `n` elements. We now see how we can define that.
-/

def numElts' {U : Type} (A : Set U) (n : Nat) : Prop := I n ∼ A

lemma numElts_def' {U : Type} (A : Set U) (n : Nat) :
    numElts' A n ↔ I n ∼ A := by rfl

/-
We can use this to provide a different characterization of finiteness.
-/

lemma finite_def' {U : Type} (A : Set U) :
    finite A ↔ ∃ (n : Nat), numElts A n := by rfl

/-
We owe some proofs of results regarding `numElts`.

We show that a set has zero elements if and only if it is empty. We
will need to produce a relation that is a matching between two sets if
the sets are empty. This is done via the `empty relation`.
-/

def emptyRel' (U V : Type) (x : U) (y : V) : Prop := False

lemma fcnl_on_empty' {U V : Type}
    (R : Rel U V) {A : Set U} (h1 : empty A) : fcnl_on R A := by
  define
  fix a : U
  assume h2 : a ∈ A
  contradict h1 with h3
  exact Exists.intro a h2
  done

lemma empty_match' {U V : Type} {A : Set U} {B : Set V}
    (h1 : empty A) (h2 : empty B) : matching (emptyRel U V) A B := by
  define
  apply And.intro
  · -- Proof of rel_within
    define
    fix a : U; fix b : V
    assume h3 : emptyRel U V a b
    by_contra h4
    define at h3
    exact h3
  · -- Proof of fcnl_ons
    apply And.intro
    · -- Proof of fcnl_on (emptyRel)
      exact fcnl_on_empty' (emptyRel U V) h1
    · -- Proof of fcnl_on (invRel emptyRel)
      exact fcnl_on_empty' (invRel (emptyRel U V)) h2
  done

lemma I_0_empty' : empty (I 0) := by
  define
  by_contra h1
  obtain (x : Nat) (h2 : x ∈ I 0) from h1
  define at h2
  linarith

theorem zero_elts_iff_empty' {U : Type} (A : Set U) :
    numElts A 0 ↔ empty A := by
  apply Iff.intro
  · -- (→)
    assume h1 : numElts A 0
    define
    by_contra h2
    obtain (x : U) (h3 : x ∈ A) from h2
    define at h1
    obtain (R : Rel Nat U) (h4 : matching R (I 0) A) from h1
    define at h4
    obtain (j : Nat) (h5 : invRel R x j) from
      fcnl_exists h4.right.right h3
    define at h5
    have h6 : j ∈ I 0 ∧ x ∈ A := h4.left h5
    contradict I_0_empty
    apply Exists.intro j
    exact h6.left
  · -- (←)
    assume h1 : empty A
    define
    apply Exists.intro (emptyRel Nat U)
    exact empty_match' I_0_empty h1
  done

/-
On the contrary, a set with a positive number of elements is not empty.
-/

theorem nonempty_of_pos_numElts' {U : Type} {A : Set U} {n : Nat}
    (h1 : numElts A n) (h2 : n > 0) : ∃ (x : U), x ∈ A := by
  define at h1
  obtain (R : Rel Nat U) (h3 : matching R (I n) A) from h1
  define at h3
  have h4 : 0 ∈ I n := h2
  obtain (x : U) (h5 : R 0 x) from fcnl_exists' h3.right.left h4
  have h6 : 0 ∈ I n ∧ x ∈ A := h3.left h5
  apply Exists.intro x
  exact h6.right
  done

/-
For the next result we will need to show that two relations are equal.
Just like we had set extensionality and function extensionality, we will
define a relation extensionality.
-/

theorem relext' {U V : Type} {R S : Rel U V}
    (h : ∀ (u : U) (v : V), R u v ↔ S u v) : R = S := by
  have h2 : extension R = extension S := by
    apply Set.ext
    fix (u, v) : U × V
    rw [ext_def, ext_def]
    exact h u v
  show R = S from
    calc R
      _ = RelFromExt (extension R) := by rfl
      _ = RelFromExt (extension S) := by rw [h2]
      _ = S := by rfl
  done

/-
We now prove that if a set A has n + 1 elements and we remove one, the resulting
set has n elements. First, we prove that if we remove an element from
equinumerous sets we still have an equinumerous set. We do so by
modifying the matching to match the elements left unmatched by the removal of
the element in the other set.
-/

def remove_one' {U V : Type} (R : Rel U V) (u : U) (v : V)
  (x : U) (y : V) : Prop := x ≠ u ∧ y ≠ v ∧ (R x y ∨ (R x v ∧ R u y))

lemma remove_one_def' {U V : Type} (R : Rel U V) (u x : U) (v y : V) :
    remove_one R u v x y ↔
      x ≠ u ∧ y ≠ v ∧ (R x y ∨ (R x v ∧ R u y)) := by rfl

/-
`remove_one` is a matching from `R \ {u}` to `R \ {v}`.
-/

lemma remove_one_rel_within' {U V : Type}
    {R : Rel U V} {A : Set U} {B : Set V} {x u : U} {y v : V}
    (h1 : matching R A B) (h2 : remove_one R u v x y) :
    x ∈ A \ {u} ∧ y ∈ B \ {v} := by
  define at h1
  define at h2
  have h3 : x ∈ A ∧ y ∈ B := by
    by_cases on h2.right.right with h3
    · -- Case 1. h3 : R x y
      exact h1.left h3
    · -- Case 2. h3 : R x v ∧ R u y
      have h4 : x ∈ A ∧ v ∈ B := h1.left h3.left
      have h5 : u ∈ A ∧ y ∈ B := h1.left h3.right
      exact And.intro h4.left h5.right
  exact And.intro (And.intro h3.left h2.left) (And.intro h3.right h2.right.left)
  done

lemma remove_one_inv' {U V : Type} (R : Rel U V) (u : U) (v : V) :
    invRel (remove_one R u v) = remove_one (invRel R) v u := by
  apply relext'
  fix y : V; fix x : U
  rw [invRel_def, remove_one_def, remove_one_def]
  rw [invRel_def, invRel_def, invRel_def]
  rw [←and_assoc, ←and_assoc]
  have h1 : x ≠ u ∧ y ≠ v ↔ y ≠ v ∧ x ≠ u := and_comm
  have h2 : R x v ∧ R u y ↔ R u y ∧ R x v := and_comm
  rw [h1, h2]
  done

lemma remove_one_iff' {U V : Type}
    {A : Set U} {B : Set V} {R : Rel U V} (h1 : matching R A B)
    {u : U} (h2 : u ∈ A) (v : V) {x : U} (h3 : x ∈ A \ {u}) :
    ∃ w ∈ A, ∀ (y : V), remove_one R u v x y ↔ R w y := by
  by_cases h4 : R x v
  · apply Exists.intro u
    apply And.intro h2
    fix y : V
    apply Iff.intro
    · assume h5 : remove_one R u v x y
      define at h5
      have h6 : R x y ∨ R x v ∧ R u y := h5.right.right
      have h7 : ¬R x y := by
        by_contra h7
        have h8 : y = v := (h1.right.left h3.left).unique h7 h4
        show False from h5.right.left h8
      disj_syll h6 h7
      exact h6.right
    · assume h5 : R u y
      define
      apply And.intro h3.right
      apply And.intro
      · by_contra h6
        rw [h6] at h5
        have h7 : v ∈ B := (h1.left h5).right
        have h8 : x = u := (h1.right.right h7).unique h4 h5
        show False from h3.right h8
      right
      exact And.intro h4 h5
  · apply Exists.intro x
    apply And.intro h3.left
    fix y : V
    have h5 : ¬(R x v ∧ R u y) := by
      by_contra h5
      show False from h4 h5.left
    apply Iff.intro
    · assume h6 : remove_one R u v x y
      define at h6
      have h7 : R x y ∨ R x v ∧ R u y := h6.right.right
      disj_syll h7 h5
      exact h7
    · assume h6 : R x y
      define
      apply And.intro h3.right
      apply And.intro
      · by_contra h7
        rw [h7] at h6
        show False from h4 h6
      · left; exact h6
  done

theorem remove_one_fcnl' {U V : Type}
    {R : Rel U V} {A : Set U} {B : Set V} {u : U}
    (h1 : matching R A B) (h2 : u ∈ A) (v : V) :
    fcnl_on (remove_one R u v) (A \ {u}) := by
  define
  fix x : U
  assume h3 : x ∈ A \ {u}
  obtain (w : U) (h4 : w ∈ A ∧ ∀ (y : V),
    remove_one R u v x y ↔ R w y) from remove_one_iff h1 h2 v h3
  define at h1
  exists_unique
  · -- Existence
    obtain (y : V) (h5 : R w y) from fcnl_exists h1.right.left h4.left
    apply Exists.intro y
    rw [h4.right]
    exact h5
  · fix y1 : V; fix y2 : V
    rw [h4.right, h4.right]
    assume h5 : R w y1
    assume h6 : R w y2
    exact fcnl_unique h1.right.left h4.left h5 h6
  done

theorem remove_one_match' {U V : Type}
    {R : Rel U V} {A : Set U} {B : Set V} {u : U} {v : V}
    (h1 : matching R A B) (h2 : u ∈ A) (h3 : v ∈ B) :
    matching (remove_one R u v) (A \ {u}) (B \ {v}) := by
  define
  apply And.intro
  · -- Proof of rel_within
    define
    fix x : U; fix y : V
    assume h4 : remove_one R u v x y
    exact remove_one_rel_within h1 h4
  · -- Proof of fcnl_ons
    apply And.intro (remove_one_fcnl h1 h2 v)
    rw [remove_one_inv]
    exact remove_one_fcnl (inv_match h1) h3 u
  done

theorem remove_one_equinum' {U V : Type}
    {A : Set U} {B : Set V} {u : U} {v : V}
    (h1 : A ∼ B) (h2 : u ∈ A) (h3 : v ∈ B) : A \ {u} ∼ B \ {v} := by
  define
  obtain (R : Rel U V) (h4 : matching R A B) from h1
  apply Exists.intro (remove_one R u v)
  exact remove_one_match h4 h2 h3

lemma I_max' (n : Nat) : n ∈ I (n + 1) := by
  define
  linarith
  done

lemma I_diff' (n : Nat) : I (n + 1) \ {n} = I n := by
  apply Set.ext
  fix x : Nat
  apply Iff.intro
  · -- (→)
    assume h1 : x ∈ I (n + 1) \ {n}
    define
    define at h1
    have h2 : x ∈ I (n + 1) := h1.left
    have h3 : ¬x ∈ {n} := h1.right
    define at h2
    define at h3
    have h4 : x ≤ n := Nat.le_of_lt_succ h2
    exact Nat.lt_of_le_of_ne h4 h3
  · -- (←)
    assume h1 : x ∈ I n
    define at h1
    define
    apply And.intro
    · -- Proof that x ∈ I (n + 1)
      define
      linarith
    · -- Proof that x ∉ {n}
      by_contra h2
      define at h2
      linarith
  done

theorem remove_one_numElts' {U : Type} {A : Set U} {n : Nat} {a : U}
    (h1 : numElts A (n + 1)) (h2 : a ∈ A) : numElts (A \ {a}) n := by
  have h3 : n ∈ I (n + 1) := I_max n
  rw [numElts_def] at h1
  have h4 : I (n + 1) \ {n} ∼ A \ {a} := remove_one_equinum h1 h3 h2
  rw [I_diff] at h4
  exact h4
  done

/-
Finally, a set has one element if and only if it is a singleton set.
The matching we need in this case should be obvious.
-/

def one_match' {U V : Type} (a : U) (b : V)
  (x : U) (y : V) : Prop := x = a ∧ y = b

lemma one_match_def' {U V : Type} (a x : U) (b y : V) :
    one_match a b x y ↔ x = a ∧ y = b := by rfl

/-
We first prove some lemmas.
-/

lemma inv_one_match {U V : Type} (a : U) (b : V) :
    invRel (one_match a b) = one_match b a := by
  apply relext
  fix v : V; fix u : U
  apply Iff.intro
  · assume h : invRel (one_match a b) v u
    define at h
    define
    exact And.intro h.right h.left
  · assume h : one_match b a v u
    define at h
    define
    exact And.intro h.right h.left
  done

theorem one_match_fcnl {U V : Type} (a : U) (b : V) :
    fcnl_on (one_match a b) {a} := by
  define
  fix x : U; assume h : x ∈ {a}
  define at h
  exists_unique
  · -- existence
    apply Exists.intro b
    rw [h]
    define
    trivial
  · -- uniqueness
    fix y1 : V; fix y2 : V
    assume h1 : one_match a b x y1
    assume h2 : one_match a b x y2
    define at h1
    define at h2
    rw [h1.right, h2.right]
  done

lemma one_match_match' {U V : Type} (a : U) (b : V) :
    matching (one_match a b) {a} {b} := by
  define
  apply And.intro
  · -- rel_within
    define
    fix x : U; fix y : V
    assume h : one_match a b x y
    define at h
    exact h
  · -- fcnl_ons
    apply And.intro
    · exact one_match_fcnl a b
    · rw [inv_one_match]
      exact one_match_fcnl b a
  done


lemma I_1_singleton' : I 1 = {0} := by
  apply Set.ext
  fix x : Nat
  apply Iff.intro
  · assume h : x ∈ I 1
    define at h
    define
    linarith
  · assume h : x ∈ {0}
    define at h
    define
    linarith
  done

lemma singleton_of_diff_empty' {U : Type} {A : Set U} {a : U}
    (h1 : a ∈ A) (h2 : empty (A \ {a})) : A = {a} := by
  apply Set.ext
  fix u : U
  apply Iff.intro
  · assume h3 : u ∈ A
    define at h2
    define
    contradict h2 with h4
    apply Exists.intro u
    exact And.intro h3 h4
  · assume h3 : u ∈ {a}
    define at h3
    rw [h3]
    exact h1
  done

lemma singleton_one_elt' {U : Type} (u : U) : numElts {u} 1 := by
  define
  rw [I_1_singleton]
  apply Exists.intro (one_match 0 u)
  exact one_match_match 0 u
  done

theorem one_elt_iff_singleton' {U : Type} (A : Set U) :
    numElts A 1 ↔ ∃ (x : U), A = {x} := by
  apply Iff.intro
  · -- (→)
    assume h1 : numElts A 1
    have h2 : 1 > 0 := by norm_num
    obtain (x : U) (h3 : x ∈ A) from nonempty_of_pos_numElts h1 h2
    have h4 : numElts (A \ {x}) 0 := remove_one_numElts h1 h3
    rw [zero_elts_iff_empty] at h4
    apply Exists.intro x
    exact singleton_of_diff_empty h3 h4
  · -- (←)
    assume h1 : ∃ (x : U), A = {x}
    obtain (x : U) (h2 : A = {x}) from h1
    rw [h2]
    exact singleton_one_elt x
  done

/-
This covers the debt from section 6.2. However, how do we know that there is
no set `A` that verifies `numElts A n` and `numElts A m` for `n ≠ m`?
Lets prove that!
-/

lemma eq_zero_of_I_zero_equinum {n : Nat} (h1 : I 0 ∼ I n) : n = 0 := by
  rw [←numElts_def, zero_elts_iff_empty] at h1
  contradict h1 with h2
  apply Exists.intro 0
  define
  exact Nat.pos_of_ne_zero h2

theorem eq_of_I_equinum : ∀ ⦃m n : Nat⦄, I m ∼ I n → m = n := by
  by_induc
  · -- Base case
    fix n : Nat
    assume h1 : I 0 ∼ I n
    exact (eq_zero_of_I_zero_equinum h1).symm
  · -- Inductive step
    fix m : Nat
    assume ih : ∀ ⦃n : ℕ⦄, I m ∼ I n → m = n
    fix n : Nat
    assume h1 : I (m + 1) ∼ I n
    have h2 : n ≠ 0 := by
      by_contra h2
      have h3 : I n ∼ I (m + 1) := Theorem_8_1_3_2 h1
      rw [h2] at h3
      have h4 : m + 1 = 0 := eq_zero_of_I_zero_equinum h3
      linarith
    obtain (k : Nat) (h3 : n = k + 1) from exists_eq_add_one_of_ne_zero h2
    rw [h3] at h1
    rw [h3]
    have h4 : m ∈ I (m + 1) := I_max m
    have h5 : k ∈ I (k + 1) := I_max k
    have h6 : I (m + 1) \ {m} ∼ I (k + 1) \ {k} :=
      remove_one_equinum h1 h4 h5
    rewrite [I_diff, I_diff] at h6
    have h7 : m = k := ih h6
    rw [h7]
  done

theorem numElts_unique {U : Type} {A : Set U} {m n : Nat}
    (h1 : numElts A m) (h2 : numElts A n) : m = n := by
  rewrite [numElts_def] at h1
  rewrite [numElts_def] at h2
  have h3 : A ∼ I n := Theorem_8_1_3_2 h2
  have h4 : I m ∼ I n := Theorem_8_1_3_3 h1 h3
  exact eq_of_I_equinum h4
  done

/-
Another promise made, in this  case in Section 7.4, is to prove that
Euler's totient function is multiplicative. In Chapter 7, we defined
the totient function as `num_rp_below m m`, where `num_rp_below m k`
is the number of natural numbers less than `k` and prime relative to `m`.
But now we have different techniques to count!

First, we show that both our counting methods agree.
-/

def Set_rp_below (m : Nat) : Set Nat := { n : Nat | rel_prime m n ∧ n < m }

lemma Set_rp_below_def (a m : Nat) :
    a ∈ Set_rp_below m ↔ rel_prime m a ∧ a < m := by rfl

lemma neb_nrpb (m : Nat) : ∀ ⦃k : Nat⦄, k ≤ m →
    num_elts_below (Set_rp_below m) k (num_rp_below m k) := by
  by_induc
  · assume h1 : 0 ≤ m
    rw [num_rp_below_base, num_elts_below]
  · fix k : Nat
    assume ih : k ≤ m → num_elts_below (Set_rp_below m) k (num_rp_below m k)
    assume h1 : k + 1 ≤ m
    have h2 : k ≤ m := by linarith
    have h3 : num_elts_below (Set_rp_below m) k (num_rp_below m k) := ih h2
    by_cases h4 : rel_prime m k
    · rw [num_rp_below_step_rp h4, num_elts_below]
      left
      apply And.intro
      · define
        apply And.intro h4
        linarith
      · apply And.intro
        · linarith
        · exact h3
    · rw [num_rp_below_step_not_rp h4, num_elts_below]
      right
      apply And.intro _ h3
      define
      demorgan
      left
      exact h4
  done

lemma neb_phi (m : Nat) :
    num_elts_below (Set_rp_below m) m (phi m) := by
  rw [phi_def]
  have h1 : m ≤ m := by linarith
  exact neb_nrpb m h1
  done

lemma phi_is_numElts (m : Nat) :
    numElts (Set_rp_below m) (phi m) := by
  rw [numElts_def]
  have h1 : ∀ n ∈ Set_rp_below m, n < m := by
    fix n : Nat
    assume h2 : n ∈ Set_rp_below m
    define at h2
    exact h2.right
  have h2 : num_elts_below (Set_rp_below m) m (phi m) := neb_phi m
  exact bdd_subset_nat h1 h2
  done

/-
And now we can fulfil our debt.
-/

lemma Lemma_7_4_7_aux {m n : Nat} {s t : Int}
    (h : s * m + t * n = 1) (a b : Nat) :
    t * n * a + s * m * b ≡ a (MOD m) := by
  define
  apply Exists.intro (s * (b - a))
  show t * n * a + s * m * b - a = m * (s * (b - a)) from
    calc t * n * a + s * m * b - a
      _ = (t * n - 1) * a + s * m * b := by ring
      _ = (t * n - (s * m + t * n)) * a + s * m * b := by rw [h]
      _ = m * (s * (b - a)) := by ring
  done

lemma Lemma_7_4_7 {m n : Nat} [NeZero m] [NeZero n]
    (h1 : rel_prime m n) (a b : Nat) :
    ∃ (r : Nat), r < m * n ∧ r ≡ a (MOD m) ∧ r ≡ b (MOD n) := by
  set s : Int := gcd_c1 m n
  set t : Int := gcd_c2 m n
  have h4 : s * m + t * n = gcd m n := gcd_lin_comb n m
  define at h1
  rw [h1, Nat.cast_one] at h4
  set x : Int := t * n * a + s * m * b
  have h5 : x ≡ a (MOD m) := Lemma_7_4_7_aux h4 a b
  rw [add_comm] at h4
  have h6 : s * m * b + t * n * a ≡ b (MOD n) :=
    Lemma_7_4_7_aux h4 b a
  have h7 : s * m * b + t * n * a = x := by ring
  rw [h7] at h6
  have h8 : m * n ≠ 0 := mul_ne_zero (NeZero.ne m) (NeZero.ne n)
  rw [←neZero_iff] at h8
  have h9 : 0 ≤ x % ↑(m * n) ∧ x % ↑(m * n) < ↑(m * n) ∧
    x ≡ x % ↑(m * n) (MOD m * n) := mod_cmpl_res (m * n) x
  have h10 : x % ↑(m * n) < ↑(m * n) ∧
    x ≡ x % ↑(m * n) (MOD m * n) := h9.right
  set r : Nat := Int.toNat (x % ↑(m * n))
  have h11 : x % ↑(m * n) = ↑r := (Int.toNat_of_nonneg h9.left).symm
  rw [h11, Nat.cast_lt] at h10
  apply Exists.intro r
  apply And.intro h10.left
  have h12 : r ≡ x (MOD (m * n)) := congr_symm h10.right
  rw [Lemma_7_4_5 _ _ h1] at h12 --h12 : r ≡ x (MOD m) ∧ r ≡ x (MOD n)
  apply And.intro
  · -- Proof that r ≡ a (MOD m)
    exact congr_trans h12.left h5
  · -- Proof that r ≡ b (MOD n)
    exact congr_trans h12.right h6
  done

/-
We now prove that if `A ∼ B` and `C ∼ D` then `A × C ∼ B × D`. How do we
state this theorem? We need to define a cartesian product operation on sets.
-/

def Set_prod {U V : Type} (A : Set U) (B : Set V) : Set (U × V) :=
  { (a, b) : U × V | a ∈ A ∧ b ∈ B }

notation:75 A:75 " ×ₛ " B:75 => Set_prod A B

lemma Set_prod_def {U V : Type} (A : Set U) (B : Set V) (a : U) (b : V) :
    (a, b) ∈ A ×ₛ B ↔ a ∈ A ∧ b ∈ B := by rfl

/-
The numbers used accompanying the notation tell Lean the priority given
to that notation. `×ₛ` has been given 75, whereas `∼` was given 50. Thus
`×ₛ` is given priority over `∼`, and `A ∼ B ×ₛ C` will be interpreted as
`A ∼ (B ×ₛ C)`.
-/

/-
To prove the desired result, we have to extend two matches to matches of
products. We can then introduce products of relations.
-/

def Rel_prod {U V W X : Type} (R : Rel U V) (S : Rel W X)
  (p : U × W) (q : V × X) : Prop := R p.1 q.1 ∧ S p.2 q.2

notation:75 R:75 " ×ᵣ " S:75 => Rel_prod R S

lemma Rel_prod_def {U V W X : Type} (R : Rel U V) (S : Rel W X)
    (u : U) (v : V) (w : W) (x : X) :
    (R ×ᵣ S) (u, w) (v, x) ↔ R u v ∧ S w x := by rfl

lemma prod_fcnl {U V W X : Type} {R : Rel U V} {S : Rel W X}
    {A : Set U} {C : Set W} (h1 : fcnl_on R A) (h2 : fcnl_on S C) :
    fcnl_on (R ×ᵣ S) (A ×ₛ C) := by
  define
  fix (u, w) : U × W
  assume h3 : (u, w) ∈ A ×ₛ C
  rw [Set_prod_def] at h3
  exists_unique
  · obtain (v : V) (h4 : R u v) from (h1 h3.left).exists
    obtain (x : X) (h5 : S w x) from (h2 h3.right).exists
    apply Exists.intro (v, x)
    rw [Rel_prod_def]
    exact And.intro h4 h5
  · fix (v1, x1) : V × X
    fix (v2, x2) : V × X
    assume h4 : (R ×ᵣ S) (u, w) (v1, x1); rw [Rel_prod_def] at h4
    assume h5 : (R ×ᵣ S) (u, w) (v2, x2); rw [Rel_prod_def] at h5
    have h6 : v1 = v2 := (h1 h3.left).unique h4.left h5.left
    have h7 : x1 = x2 := (h2 h3.right).unique h4.right h5.right
    rw [h6, h7]

lemma prod_match {U V W X : Type}
    {A : Set U} {B : Set V} {C : Set W} {D : Set X}
    {R : Rel U V} {S : Rel W X}
    (h1 : matching R A B) (h2 : matching S C D) :
    matching (R ×ᵣ S) (A ×ₛ C) (B ×ₛ D) := by
  define
  apply And.intro
  · -- rel_within
    define
    fix (u, w) : U × W
    fix (v, x) : V × X
    assume h3 : (R ×ᵣ S) (u, w) (v, x)
    rw [Set_prod_def, Set_prod_def]
    rw [Rel_prod_def] at h3
    have h4 : u ∈ A ∧ v ∈ B := h1.left h3.left
    have h5 : w ∈ C ∧ x ∈ D := h2.left h3.right
    exact And.intro (And.intro h4.left h5.left) (And.intro h4.right h5.right)
  · apply And.intro
    · exact prod_fcnl h1.right.left h2.right.left
    · exact prod_fcnl h1.right.right h2.right.right
  done

theorem Theorem_8_1_2_1
    {U V W X : Type} {A : Set U} {B : Set V} {C : Set W} {D : Set X}
    (h1 : A ∼ B) (h2 : C ∼ D) : A ×ₛ C ∼ B ×ₛ D := by
  obtain (R : Rel U V) (h3 : matching R A B) from h1
  obtain (S : Rel W X) (h4 : matching S C D) from h2
  apply Exists.intro (R ×ᵣ S)
  exact prod_match h3 h4
  done

/-
We now show that if `A` is a set of `m` elements and `B` is a set of `n`
elements then `A × B` is a set of `m * n` elements.
-/

def qr (n a : Nat) : Nat × Nat := (a / n, a % n)

lemma qr_def (n a : Nat) : qr n a = (a / n, a % n) := by rfl

lemma qr_one_one (n : Nat) : one_to_one (qr n) := by
  define
  fix a1 : Nat; fix a2 : Nat
  assume h1 : qr n a1 = qr n a2
  rw [qr_def, qr_def] at h1
  have h2 : a1 / n = a2 / n ∧ a1 % n = a2 % n := Prod.mk.inj h1
  show a1 = a2 from
    calc a1
      _ = n * (a1 / n) + a1 % n := (Nat.div_add_mod a1 n).symm
      _ = n * (a2 / n) + a2 % n := by rw [h2.left, h2.right]
      _ = a2 := Nat.div_add_mod a2 n
  done

theorem quot_rem_unique (m q r q' r' : Nat)
    (h1 : m * q + r = m * q' + r') (h2 : r < m) (h3 : r' < m) :
    q = q' ∧ r = r' := sorry

lemma qr_image (m n : Nat) : image (qr n) (I (m * n)) = I m ×ₛ I n := by
  by_cases h : n = 0
  · rw [h, mul_zero]
    apply Set.ext
    fix (q, r) : Nat × Nat
    rw [Set_prod_def]
    apply Iff.intro
    · assume h1 : (q, r) ∈ image (qr 0) (I 0)
      define at h1
      obtain (x : Nat) (h2 : x ∈ I 0 ∧ qr 0 x = (q, r)) from h1
      have h3 : x ∈ I 0 := h2.left
      define at h3
      linarith
    · assume h1 : q ∈ I m ∧ r ∈ I 0
      have h2 : r ∈ I 0 := h1.right
      define at h2
      linarith
  · have h' : n > 0 := Nat.pos_of_ne_zero h
    apply Set.ext
    fix (q, r) : Nat × Nat
    apply Iff.intro
    · assume h1 : (q, r) ∈ image (qr n) (I (m * n))
      define at h1
      obtain (x : Nat) (h2 : x ∈ I (m * n) ∧ qr n x = (q, r)) from h1
      rw [qr_def n x] at h2
      rw [Set_prod_def]
      have h3 : (x / n, x % n) = (q, r) := h2.right
      have h4 : x / n = q ∧ x % n = r := Prod.ext_iff.ltr h3
      apply And.intro
      · define
        have h5 : x ∈ I (m * n) := h2.left
        define at h5
        have h6 : n ∣ m * n := by
          apply Exists.intro m; rw [mul_comm]
        have h7 : x / n < m * n / n := Nat.div_lt_div_of_lt_of_dvd h6 h5
        rw [← h4.left]
        rw [Nat.mul_div_cancel m h'] at h7
        exact h7
      · define
        rw [← h4.right]
        exact Nat.mod_lt x h'
    · assume h1 : (q, r) ∈ I m ×ₛ I n
      rw [Set_prod_def] at h1
      define
      apply Exists.intro (n * q + r)
      have h2 : n ∣ n * q := by
        apply Exists.intro q
        rfl
      apply And.intro
      · define
        have h3 : q < m := h1.left
        have h4 : r < n := h1.right
        have h5 : q + 1 ≤ m := by linarith
        have h6 : n * (q + 1) ≤ n * m := by rel [h5]
        rw [mul_add, mul_one] at h6
        have h7 : n * q + r < n * q + n :=
          calc n * q + r
            _ < n * q + n := by rel [h4]
        rw [mul_comm n m] at h6
        exact Nat.lt_of_lt_of_le h7 h6
      · have h3 : n * ((n * q + r) / n) + (n * q + r) % n = n * q + r
          := Nat.div_add_mod (n * q + r) n
        have h4 : (n * q + r) % n < n := Nat.mod_lt (n * q + r) h'
        have h5 : (n * q + r) / n = q ∧ (n * q + r) % n = r
          := quot_rem_unique n ((n * q + r) / n) ((n * q + r) % n) q r h3 h4 h1.right
        rw [qr_def n (n * q + r), h5.left, h5.right]

lemma I_prod (m n : Nat) : I (m * n) ∼ I m ×ₛ I n := equinum_image
  (one_one_on_of_one_one (qr_one_one n) (I (m * n))) (qr_image m n)

theorem numElts_prod {U V : Type} {A : Set U} {B : Set V} {m n : Nat}
    (h1 : numElts A m) (h2 : numElts B n) : numElts (A ×ₛ B) (m * n) := by
  rewrite [numElts_def] at h1
  rewrite [numElts_def] at h2
  rewrite [numElts_def]
  have h3 : I m ×ₛ I n ∼ A ×ₛ B := Theorem_8_1_2_1 h1 h2
  have h4 : I (m * n) ∼ I m ×ₛ I n := I_prod m n
  exact Theorem_8_1_3_3 h4 h3
  done

/-
The strategy will now be to prove that if m and n are relatively prime, then
`Set_rp_below (m * n) ∼ Set_rp_below m ×ₛ Set_rp_below n`.
We do so with a function from `Nat` to `Nat × Nat`.
-/

def mod_mod (m n a : Nat) : Nat × Nat := (a % m, a % n)

lemma mod_mod_def (m n a : Nat) : mod_mod m n a = (a % m, a % n) := by rfl

/-
We also make use of the following results, from exercises in Sections 7.3 and
7.4.
-/

theorem congr_rel_prime {m a b : Nat} (h1 : a ≡ b (MOD m)) :
    rel_prime m a ↔ rel_prime m b := sorry

theorem rel_prime_mod (m a : Nat) :
    rel_prime m (a % m) ↔ rel_prime m a := sorry

theorem congr_iff_mod_eq_Nat' (m a b : Nat) [NeZero m] :
    ↑a ≡ ↑b (MOD m) ↔ a % m = b % m := sorry

lemma Lemma_7_4_6 {a b c : Nat} :
    rel_prime (a * b) c ↔ rel_prime a c ∧ rel_prime b c := sorry

/-
And using these...
-/

lemma left_NeZero_of_mul {m n : Nat} (h : m * n ≠ 0) : NeZero m :=
  neZero_iff.rtl (left_ne_zero_of_mul h)

lemma right_NeZero_of_mul {m n : Nat} (h : m * n ≠ 0) : NeZero n :=
  neZero_iff.rtl (right_ne_zero_of_mul h)

lemma mod_mod_one_one_on {m n : Nat} (h1 : rel_prime m n) :
    one_one_on (mod_mod m n) (Set_rp_below (m * n)) := by
  define
  fix a1 : Nat; fix a2 : Nat
  assume h2 : a1 ∈ Set_rp_below (m * n)
  assume h3 : a2 ∈ Set_rp_below (m * n)
  assume h4 : mod_mod m n a1 = mod_mod m n a2
  define at h2; define at h3
  rw [mod_mod_def, mod_mod_def] at h4
  have h5 : a1 % m = a2 % m ∧ a1 % n = a2 % n := Prod.mk.inj h4
  have h6 : m * n ≠ 0 := by linarith
  have h7 : NeZero m := left_NeZero_of_mul h6
  have h8 : NeZero n := right_NeZero_of_mul h6
  rw [←congr_iff_mod_eq_Nat, ←congr_iff_mod_eq_Nat] at h5
  rw [←Lemma_7_4_5 _ _ h1] at h5
  rw [congr_iff_mod_eq_Nat] at h5
  rw [Nat.mod_eq_of_lt h2.right, Nat.mod_eq_of_lt h3.right] at h5
  exact h5
  done

lemma mod_elt_Set_rp_below {a m : Nat} [NeZero m] (h1 : rel_prime m a) :
    a % m ∈ Set_rp_below m := by
  define
  rw [rel_prime_mod]
  exact And.intro h1 (mod_nonzero_lt a (NeZero.ne m))
  done

lemma mod_mod_image {m n : Nat} (h1 : rel_prime m n) :
    image (mod_mod m n) (Set_rp_below (m * n)) =
      (Set_rp_below m) ×ₛ (Set_rp_below n) := by
  apply Set.ext
  fix (b, c) : Nat × Nat
  apply Iff.intro
  · -- (→)
    assume h2 : (b, c) ∈ image (mod_mod m n) (Set_rp_below (m * n))
    define at h2
    obtain (a : Nat)
      (h3 : a ∈ Set_rp_below (m * n) ∧ mod_mod m n a = (b, c)) from h2
    rw [Set_rp_below_def, mod_mod_def] at h3
    have h4 : rel_prime (m * n) a := h3.left.left
    rw [Lemma_7_4_6] at h4
    have h5 : a % m = b ∧ a % n = c := Prod.mk.inj h3.right
    define
    rw [←h5.left, ←h5.right]
    have h6 : m * n ≠ 0 := by linarith
    have h7 : NeZero m := left_NeZero_of_mul h6
    have h8 : NeZero n := right_NeZero_of_mul h6
    apply And.intro
    · -- Proof that a % m ∈ Set_rp_below m
      exact mod_elt_Set_rp_below h4.left
    · -- Proof that a % n ∈ Set_rp_below n
      exact mod_elt_Set_rp_below h4.right
  · -- (←)
    assume h2 : (b, c) ∈ Set_rp_below m ×ₛ Set_rp_below n
    rewrite [Set_prod_def, Set_rp_below_def, Set_rp_below_def] at h2
      --h2 : (rel_prime m b ∧ b < m) ∧ (rel_prime n c ∧ c < n)
    define
    have h3 : m ≠ 0 := by linarith
    have h4 : n ≠ 0 := by linarith
    rewrite [←neZero_iff] at h3
    rewrite [←neZero_iff] at h4
    obtain (a : Nat) (h5 : a < m * n ∧ a ≡ b (MOD m) ∧ a ≡ c (MOD n))
      from Lemma_7_4_7 h1 b c
    apply Exists.intro a
    apply And.intro
    · -- Proof of a ∈ Set_rp_below (m * n)
      define
      apply And.intro _ h5.left
      rw [Lemma_7_4_6]
      rw [congr_rel_prime h5.right.left,
        congr_rel_prime h5.right.right]
      exact And.intro h2.left.left h2.right.left
      done
    · -- Proof of mod_mod m n a = (b, c)
      rw [congr_iff_mod_eq_Nat, congr_iff_mod_eq_Nat] at h5
      rw [mod_mod_def, h5.right.left, h5.right.right]
        --Goal : (b % m, c % n) = (b, c)
      rw [Nat.mod_eq_of_lt h2.left.right,
        Nat.mod_eq_of_lt h2.right.right]
  done

lemma Set_rp_below_prod {m n : Nat} (h1 : rel_prime m n) :
    Set_rp_below (m * n) ∼ (Set_rp_below m) ×ₛ (Set_rp_below n) :=
  equinum_image (mod_mod_one_one_on h1) (mod_mod_image h1)

/-
We can finally prove the theorem!
-/

lemma eq_numElts_of_equinum {U V : Type} {A : Set U} {B : Set V} {n : Nat}
    (h1 : A ∼ B) (h2 : numElts A n) : numElts B n := by
  rw [numElts_def] at h2
  rw [numElts_def]
  exact Theorem_8_1_3_3 h2 h1
  done

theorem Theorem_7_4_4 {m n : Nat} (h1 : rel_prime m n) :
    phi (m * n) = (phi m) * (phi n) := by
  have h2 : numElts (Set_rp_below m) (phi m) := phi_is_numElts m
  have h3 : numElts (Set_rp_below n) (phi n) := phi_is_numElts n
  have h4 : numElts (Set_rp_below (m * n)) (phi (m * n)) :=
    phi_is_numElts (m * n)
  have h5 : numElts (Set_rp_below m ×ₛ Set_rp_below n) (phi (m * n)) :=
    eq_numElts_of_equinum (Set_rp_below_prod h1) h4
  have h6 : numElts (Set_rp_below m ×ₛ Set_rp_below n) (phi m * phi n) :=
    numElts_prod h2 h3
  exact numElts_unique h5 h6
  done
