import HTPILib.HTPIDefs
import HTPILib.Chap6
import HTPILib.Chap7

namespace HTPI


-- Chapter 7. Number Theory

-- Section 7.1 Greatest Common Divisors

/-
The explanations here are going to be more succinct. In this section we
introduce the Euclidean Algorithm to compute the gcd of two numbers.
Recall that the remainder operation was denoted `a % b`.

The main ingredient of the algorithm lies in the next two results.
-/

theorem dvd_mod_of_dvd_a_b' {a b d : Nat}
    (h1 : d ∣ a) (h2 : d ∣ b) : d ∣ (a % b) := by
  set q : Nat := a / b
  have h3 : b * q + a % b = a := Nat.div_add_mod a b
  obtain (j : Nat) (h4 : a = d * j) from h1
  obtain (k : Nat) (h5 : b = d * k) from h2
  define
  apply Exists.intro (j - k * q)
  show a % b = d * (j - k * q) from
    calc a % b
      _ = b * q + a % b - b * q := (Nat.add_sub_cancel_left _ _).symm
      _ = a - b * q := by rw [h3]
      _ = d * j - d * (k * q) := by rw [h4, h5, mul_assoc]
      _ = d * (j - k * q) := (Nat.mul_sub_left_distrib _ _ _).symm
  done

theorem dvd_a_of_dvd_b_mod' {a b d : Nat}
    (h1 : d ∣ b) (h2 : d ∣ (a % b)) : d ∣ a := by
  set q : Nat := a / b
  have h3 : b * q + a % b = a := Nat.div_add_mod a b
  define at h1; obtain (j : ℕ) (h4 : b = d * j) from h1
  define at h2; obtain (k : ℕ) (h5 : a % b = d * k) from h2
  define; apply Exists.intro (j * q + k)
  show a = d * (j * q + k) from
    calc a
      _ = b * q + a % b := h3.symm
      _ = (d * j) * q + a % b := by rw [h4]
      _ = (d * j) * q + d * k := by rw [h5]
      _ = d * (j * q + k) := by ring_nf
  done

/-
These theorems tell us that the gcd of `a` and `b` is the same as that of
`b` and `a % b`; thus we can iterate:
-/

def gcd₁ (a b : Nat) : Nat :=
  match b with
    | 0 => a
    | n + 1 => gcd₁ (n + 1) (a % (n + 1))

/-
Unfortunately, Lean cannot determine that this terminates, so it shows
an error. Recursive functions don't necessarily need to end.

Consider the following more obvious example.
-/

def loop (n : Nat) : Nat := loop (n + 1)

/-
Again, the lack of guarantee of termination means this definition fails.
It does not provide us with a function from Nat to Nat.
-/

/-
While the `loop` function does not terminate, gcd should. We just need
to show it to Lean.

The function `gcd` has two arguments: `a` and `b`. When `b = n + 1`, we
have to compute `gcd (n + 1) (a % (n + 1))`. The first argument here
could be larger than the first argument in the value we are trying to
compute, but no matter, because the second will always be smaller.
This suffices to show that the computation must terminate. We can tell
Lean to focus on the second argument by adding a `termination_by`.
-/

def gcd₂ (a b : Nat) : Nat :=
  match b with
    | 0 => a
    | n + 1 => gcd₂ (n + 1) (a % (n + 1))
  termination_by gcd₂ a b => b

/-
Lean is still not satisfied, but the error is more helpful now. Namely,
it claims to have proven that `a % (n + 1) < Nat.succ n`. where
`Nat.succ n` is `n + 1`. We need to produce a proof of this fact.
-/

lemma mod_succ_lt' (a n : Nat) : a % (n + 1) < n + 1 := by
  have h : n + 1 > 0 := Nat.succ_pos n
  show a % (n + 1) < n + 1 from Nat.mod_lt a h
  done

/-
With this, and using a `have` expression as suggested by the error, we
can finally successfully define the function.
-/

def gcd' (a b : Nat) : Nat :=
  match b with
    | 0 => a
    | n + 1 =>
      have : a % (n + 1) < n + 1 := mod_succ_lt a n
      gcd' (n + 1) (a % (n + 1))
  termination_by gcd' a b => b

/-
And now we can compute stuff!
-/

#eval gcd 672 161    --Answer: 7.  Note 672 = 7 * 96 and 161 = 7 * 23.

/-
We now move to establishing the main properties of gcd.
-/

lemma gcd_base' (a : Nat) : gcd a 0 = a := by rfl

lemma gcd_nonzero' (a : Nat) {b : Nat} (h : b ≠ 0) :
    gcd a b = gcd b (a % b) := by
  obtain (n : Nat) (h2 : b = n + 1) from exists_eq_add_one_of_ne_zero h
  rewrite [h2]
  rfl
  done

lemma mod_nonzero_lt' (a : Nat) {b : Nat} (h : b ≠ 0) : a % b < b := by
  have h1 : b > 0 := Nat.pos_of_ne_zero h
  show a % b < b from Nat.mod_lt a h1
  done

lemma dvd_self' (n : Nat) : n ∣ n := by
  apply Exists.intro 1
  rw [mul_one]
  done

/-
One important result is that `gcd a b` divides both `a` and `b`. We prove it by strong
induction.
-/

theorem gcd_dvd' : ∀ (b a : Nat), (gcd a b) ∣ a ∧ (gcd a b) ∣ b := by
  by_strong_induc
  fix b : ℕ
  assume ih : ∀ (b' : ℕ), b' < b → ∀ (a : ℕ), gcd a b' ∣ a ∧ gcd a b' ∣ b'
  fix a : ℕ
  by_cases h1 : b = 0
  · -- Case b = 0
    rw [h1, gcd_base]
    apply And.intro (dvd_self a)
    define; apply Exists.intro 0
    rfl
  · -- Case b ≠ 0
    rw [gcd_nonzero a h1]
    have h2 : a % b < b := mod_nonzero_lt a h1
    have h3 : (gcd b (a % b)) ∣ b ∧ (gcd b (a % b)) ∣ (a % b) :=
      ih (a % b) h2 b
    apply And.intro _ h3.left
    exact dvd_a_of_dvd_b_mod h3.left h3.right
  done

/-
Note that we would not be able to prove the result fixing a and then doing strong
induction, because of how the `gcd` function is defined.

We can now get two different theorems stating this relation for both left and right.
-/

theorem gcd_dvd_left' (a b : Nat) : (gcd a b) ∣ a := (gcd_dvd b a).left

theorem gcd_dvd_right' (a b : Nat) : (gcd a b) ∣ b := (gcd_dvd b a).right

/-
Euclid's algorithm to compute the `gcd` can be extended to show constructively
that we can obtain integers `t` and `s` such that `gcd(a, b) = s * a + t * b`.
Note that we need to use integers, as it is possible that `t` and `s` cannot be
positive at the same time. Here, we will use a slightly different technique to
prove this result and obtain the integers.
-/

/-
If `b = 0`, we have that `gcd(a,b) = a = 1 * a + 0 * b`, so we can use `s = 1` and
`t = 0`. Otherwise, let `q` and `r` be the quotient and remainder of dividing `a` by
`b`, hence `a = b * q + r`. Furthermore, `gcd(a,b) = gcd(b,r)`. Now, if we have
already computed `s'` and `t'` such that `gcd(b,r) = s' * b + t' * r`, then
`gcd(a,b) = gcd(b,r) = s' * b + t' * r = s' * b + t' * (a - b * q)`
        ` = t' * a + (s' - t' * q) * b`
Therefore, to write `gcd(a,b) = a * s + t * b` we use `s = t'` and `t = s' - t' * q`.
If we need to continue computations we can keep doing it recursively.
-/

/-
This provides us with a basis to define two recursive functions: `gcd_c1` and `gcd_c2`,
with `s = gcd_c1 a b` and `t = gcd_c2 a b`. These two functions will be mutually
recursive: each is defined not only in terms of them but in terms of the other as well.
Lean allows for this, as we now show.

One further note: since `s` and `t` can be negative, we need to work with integers, so
type coercion will be used.
-/

mutual
  def gcd_c1' (a b : Nat) : Int :=
    match b with
      | 0 => 1
      | n + 1 =>
        have : a % (n + 1) < n + 1 := mod_succ_lt a n
        gcd_c2' (n + 1) (a % (n + 1))
          --Corresponds to s = t' in (*)

  def gcd_c2' (a b : Nat) : Int :=
    match b with
      | 0 => 0
      | n + 1 =>
        have : a % (n + 1) < n + 1 := mod_succ_lt a n
        gcd_c1' (n + 1) (a % (n + 1)) -
          (gcd_c2' (n + 1) (a % (n + 1))) * ↑(a / (n + 1))
          --Corresponds to t = s' - t'q in (*)
end
  termination_by
    gcd_c1' a b => b
    gcd_c2' a b => b

/-
We now prove the desired result: that this function provides the coefficients to write
the gcd. Let us begin with some lemmas.
-/

lemma gcd_c1_base' (a : Nat) : gcd_c1 a 0 = 1 := by rfl

lemma gcd_c1_nonzero' (a : Nat) {b : Nat} (h : b ≠ 0) :
    gcd_c1 a b = gcd_c2 b (a % b) := by
  obtain (n : ℕ) (h1 : b = n + 1) from exists_eq_add_one_of_ne_zero h
  rw [h1]
  rfl
  done

lemma gcd_c2_base' (a : Nat) : gcd_c2 a 0 = 0 := by rfl

lemma gcd_c2_nonzero' (a : Nat) {b : Nat} (h : b ≠ 0) :
    gcd_c2 a b = gcd_c1 b (a % b) - (gcd_c2 b (a % b)) * ↑(a / b) := by
  obtain (n : Nat) (h2 : b = n + 1) from exists_eq_add_one_of_ne_zero h
  rw [h2]
  rfl
  done

/-
And now we can prove the result.
-/

theorem gcd_lin_comb' : ∀ (b a : Nat),
    (gcd_c1 a b) * ↑a + (gcd_c2 a b) * ↑b = ↑(gcd a b) := by
  by_strong_induc
  fix b : ℕ
  assume ih : ∀ (b_1 : ℕ), b_1 < b → ∀ (a : ℕ),
      gcd_c1 a b_1 * ↑a + gcd_c2 a b_1 * ↑b_1 = ↑(gcd a b_1)
  fix a : ℕ
  by_cases h1 : b = 0
  · -- Case b = 0
    rw [h1, gcd_c1_base, gcd_c2_base, gcd_base]
    norm_num
  · -- Case b ≠ 0
    rw [gcd_c1_nonzero a h1, gcd_c2_nonzero a h1, gcd_nonzero a h1]
    -- We introduce some variables to simplify the notation
    set r : Nat := a % b
    set q : Nat := a / b
    set s : Int := gcd_c1 b r
    set t : Int := gcd_c2 b r
    have h2 : r < b := mod_nonzero_lt a h1
    have h3 : s * ↑b + t * ↑r = ↑(gcd b r) := ih r h2 b
    have h4 : b * q + r = a := Nat.div_add_mod a b
    rewrite [←h3, ←h4]
    rewrite [Nat.cast_add, Nat.cast_mul]
    ring
  done

/-
The use of the cast is necessary in order for `ring` to be able to complete the proof.
It makes the remaining naturals in the expression into integers.
-/

#eval gcd_c1 672 161  --Answer: 6
#eval gcd_c2 672 161  --Answer: -25
  --Note 6 * 672 - 25 * 161 = 4032 - 4025 = 7 = gcd 672 161

/-
We finish this section with another result.
-/

theorem Theorem_7_1_6' {d a b : Nat} (h1 : d ∣ a) (h2 : d ∣ b) :
    d ∣ gcd a b := by
  rewrite [←Int.coe_nat_dvd]
  set s : Int := gcd_c1 a b
  set t : Int := gcd_c2 a b
  have h3 : s * ↑a + t * ↑b = ↑(gcd a b) := gcd_lin_comb b a
  rewrite [←h3]
  obtain (j : Nat) (h4 : a = d * j) from h1
  obtain (k : Nat) (h5 : b = d * k) from h2
  rewrite [h4, h5, Nat.cast_mul, Nat.cast_mul]
  define
  apply Exists.intro (s * ↑j + t * ↑k)
  ring
  done

/-
`Int.coe_nat_dvd` establishes that divisibility is equivalent over the integers and
naturals, when both numbers are natural.
-/


-- Section 7.2. Prime factorization

def prime' (n : Nat) : Prop :=
  2 ≤ n ∧ ¬∃ (a b : Nat), a * b = n ∧ a < n ∧ b < n

/-
The main goal of this section is to prove that every integer has a unique factorization.
Let us first prove that every natural number greater or equal than 2 has a prime factor.
-/

def prime_factor' (p n : Nat) : Prop := prime p ∧ p ∣ n

lemma dvd_trans' {a b c : Nat} (h1 : a ∣ b) (h2 : b ∣ c) : a ∣ c := by
  define at h1
  obtain (c₁ : Nat) (h3 : b = a * c₁) from h1
  define at h2
  obtain (c₂ : Nat) (h4 : c = b * c₂) from h2
  define
  apply Exists.intro (c₁ * c₂)
  rw [← mul_assoc, ← h3, ← h4]
  done

lemma exists_prime_factor' : ∀ (n : Nat), 2 ≤ n →
    ∃ (p : Nat), prime_factor p n := by
  by_strong_induc
  fix n : Nat
  assume ih : ∀ (n₁ : ℕ), n₁ < n → 2 ≤ n₁ → ∃ (p : ℕ), prime_factor p n₁
  assume h1 : 2 ≤ n
  by_cases h2 : prime n
  · -- If `n` is already prime, it's pretty obvious
    apply Exists.intro n
    define
    exact And.intro h2 (dvd_self n)
  · -- If `n` is not prime
    define at h2
    demorgan at h2
    disj_syll h2 h1
    obtain (a : Nat) (h3 : ∃ (b : Nat), a * b = n ∧ a < n ∧ b < n) from h2
    obtain (b : Nat) (h4 : a * b = n ∧ a < n ∧ b < n) from h3
    have h5 : 2 ≤ a := by
      by_contra h6
      have h7 : a ≤ 1 := by linarith
      have h8 : n ≤ b :=
        calc n
          _ = a * b := h4.left.symm
          _ ≤ 1 * b := by rel [h7]
          _ = b := by ring
      linarith -- n ≤ b contradicts b < n
    have h6 : ∃ (p : Nat), prime_factor p a := ih a h4.right.left h5
    obtain (p : Nat) (h7 : prime_factor p a) from h6
    apply Exists.intro p
    define
    define at h7
    apply And.intro h7.left
    have h8 : a ∣ n := by
      apply Exists.intro b
      exact h4.left.symm
    exact dvd_trans h7.right h8
  done

/-
By the well ordering principle, every number greater to or equal than 2 must
have a smallest prime factor.
-/

lemma exists_least_prime_factor' {n : Nat} (h : 2 ≤ n) :
    ∃ (p : Nat), prime_factor p n ∧
    ∀ (q : Nat), prime_factor q n → p ≤ q := by
  set S : Set Nat := { p : Nat | prime_factor p n }
  have h2 : ∃ (p : Nat), p ∈ S := exists_prime_factor n h
  show ∃ (p : Nat), prime_factor p n ∧
    ∀ (q : Nat), prime_factor q n → p ≤ q from well_ord_princ S h2
  done

/-
We now introduce a new type: Lists. If `U` is a type, then `List U` is the type
of lists of objects of type `U`. They are denoted using brackets, so `[3, 7, 1]`
is of type `List Nat`.

`[]` denotes the empty list, also called `nil`.

If `a : U` and `l : List U`, `a :: l` denotes the list consisting of `a`
followed by the items of `l`. This is the `cons` operator, and every list can be
constructed by applying the `cons` operation repeatedly.

If `l : List U` and `a : U`, then `a ∈ l` denotes that `a` is one of the entries
in the list `l`. For example, `7 ∈ [3, 7, 1]`.

Here are some basic results about the `cons` operator.
-/

#check List.not_mem_nil
-- ∀ {α : Type u_1} (a : α), a ∉ []

#check List.mem_cons
-- ∀ {α : Type u_1} {a b : α} {l : List α}, a ∈ b :: l ↔ a = b ∨ a ∈ l

#check List.mem_cons_self
-- ∀ {α : Type u_1} (a : α) (l : List α), a ∈ a :: l

#check List.mem_cons_of_mem
-- ∀ {α : Type u_1} (y : α) {a : α} {l : List α}, a ∈ l → a ∈ y :: l

/-
Let us define some properties that we need towards our goals.
-/

def all_prime' (l : List Nat) : Prop := ∀ p ∈ l, prime p
-- Every member of the list is prime

def nondec' (l : List Nat) : Prop :=
  match l with
    | [] => True   -- True is a proposition that is always true
    | n :: L => (∀ m ∈ L, n ≤ m) ∧ nondec L
-- Every member of the list is less than or equal to all later members

def nondec_prime_list' (l : List Nat) : Prop := all_prime l ∧ nondec l

def prod' (l : List Nat) : Nat :=
  match l with
    | [] => 1
    | n :: L => n * (prod L)
-- The product of all members of l

def prime_factorization' (n : Nat) (l : List Nat) : Prop :=
  nondec_prime_list l ∧ prod l = n
-- l is a non-decreasing list of prime numbers whose product is n.

/-
Let us prove some results
-/

lemma all_prime_nil' : all_prime [] := by
  define
  fix p : Nat
  contrapos
  assume h1 : ¬prime p
  show p ∉ [] from List.not_mem_nil p
  done

lemma all_prime_cons' (n : Nat) (L : List Nat) :
    all_prime (n :: L) ↔ prime n ∧ all_prime L := by
  apply Iff.intro
  · -- (→)
    assume h1 : all_prime (n :: L)
    define at h1
    apply And.intro (h1 n (List.mem_cons_self n L))
    define
    fix p : Nat; assume h2 : p ∈ L
    exact h1 p (List.mem_cons_of_mem n h2)
  · -- (←)
    assume h1 : prime n ∧ all_prime L
    define : all_prime L at h1
    define
    fix p : Nat; assume h2 : p ∈ n :: L
    rw [List.mem_cons] at h2
    by_cases on h2
    · rw [h2]; exact h1.left
    · exact h1.right p h2
  done

lemma nondec_nil' : nondec [] := by
  define
  trivial -- Proves some obviously true statements
  done

lemma nondec_cons' (n : Nat) (L : List Nat) :
    nondec (n :: L) ↔ (∀ m ∈ L, n ≤ m) ∧ nondec L := by rfl

lemma prod_nil' : prod [] = 1 := by rfl

lemma prod_cons' : prod (n :: L) = n * (prod L) := by rfl

/-
We now quickly review some other statements about lists.
If `l` is a list, we can get its length as `List.length l` or `l.length`.
-/

#check List.length_eq_zero
-- ∀ {α : Type u_1} {l : List α}, List.length l = 0 ↔ l = []

#check List.length_cons
-- ∀ {α : Type u_1} (a : α) (as : List α), List.length (a :: as) = Nat.succ (List.length as)

#check List.exists_cons_of_ne_nil
-- ∀ {α : Type u_1} {l : List α}, l ≠ [] → ∃ (b : α), ∃ (L : List α), l = b :: L

/- And one last lemma -/

lemma exists_cons_of_length_eq_succ' {A : Type}
    {l : List A} {n : Nat} (h : l.length = n + 1) :
    ∃ (a : A) (L : List A), l = a :: L ∧ L.length = n := by
  have h1 : l ≠ [] := by
    by_contra h1
    have h2 : l.length = 0 := List.length_eq_zero.mpr h1
    linarith
  obtain (a : A) (h2 :  ∃ (L : List A), l = a :: L) from List.exists_cons_of_ne_nil h1
  obtain (L : List A) (h3 : l = a :: L) from h2
  apply Exists.intro a; apply Exists.intro L
  apply And.intro h3
  have h4 : List.length (a :: L) = List.length L + 1 := List.length_cons a L
  rw [← h3, h] at h4
  linarith
  done

/-
We can now prove that every member of a list of natural numbers divides
the product of the list.
-/

lemma list_elt_dvd_prod_by_length' (a : Nat) : ∀ (n : Nat),
    ∀ (l : List Nat), l.length = n → a ∈ l → a ∣ prod l := by
  by_induc
  · fix l : List Nat; assume h1 : l.length = 0
    rw [List.length_eq_zero] at h1
    rw [h1]
    contrapos
    assume h2 : ¬a ∣ prod []
    exact List.not_mem_nil a
  · fix n : Nat
    assume ih : ∀ (l : List Nat), List.length l = n → a ∈ l → a ∣ prod l
    fix l : List Nat
    assume h1 : l.length = n + 1
    obtain (b : Nat) (h2 : ∃ (L : List Nat),
      l = b :: L ∧ L.length = n) from exists_cons_of_length_eq_succ h1
    obtain (L : List Nat) (h3 : l = b :: L ∧ L.length = n) from h2
    have h4 : a ∈ L → a ∣ prod L := ih L h3.right
    assume h5 : a ∈ l
    rw [h3.left, prod_cons]
    rw [h3.left, List.mem_cons] at h5
    by_cases on h5
    · apply Exists.intro (prod L)
      rw [h5]
    · have h6 : a ∣ prod L := h4 h5
      have h7 : prod L ∣ b * prod L := by
        apply Exists.intro b; ring
      exact dvd_trans h6 h7
  done

lemma list_elt_dvd_prod' {a : Nat} {l : List Nat}
    (h : a ∈ l) : a ∣ prod l := by
  set n : Nat := l.length
  have h1 : l.length = n := by rfl
  show a ∣ prod l from list_elt_dvd_prod_by_length a n l h1 h
  done

/-
We can now prove that every natural has a prime factorization.
-/

lemma exists_prime_factorization' : ∀ (n : Nat), n ≥ 1 →
    ∃ (l : List Nat), prime_factorization n l := by
  by_strong_induc
  fix n : Nat
  assume ih : ∀ n_1 < n, n_1 ≥ 1 →
    ∃ (l : List Nat), prime_factorization n_1 l
  assume h1 : n ≥ 1
  by_cases h2 : n = 1
  · -- Case n = 1
    apply Exists.intro []
    define
    apply And.intro
    · define
      exact And.intro all_prime_nil nondec_nil
    · rw [prod_nil, h2]
  · -- Case n > 1
    have h3 : n ≥ 2 := lt_of_le_of_ne' h1 h2
    obtain (p : Nat) (h4 : prime_factor p n ∧ ∀ (q : Nat),
      prime_factor q n → p ≤ q) from exists_least_prime_factor h3
    have p_prime_factor : prime_factor p n := h4.left
    define at p_prime_factor
    have p_prime : prime p := p_prime_factor.left
    have p_dvd_n : p ∣ n := p_prime_factor.right
    have p_least : ∀ (q : Nat), prime_factor q n → p ≤ q := h4.right
    obtain (m : Nat) (n_eq_pm : n = p * m) from p_dvd_n
    have h5 : m ≠ 0 := by
      contradict h1 with h6
      have h7 : n = 0 :=
        calc n
          _ = p * m := n_eq_pm
          _ = p * 0 := by rw [h6]
          _ = 0 := by ring
      rw [h7]
      norm_num
    have m_pos : 0 < m := Nat.pos_of_ne_zero h5
    have m_lt_n : m < n := by
      define at p_prime
      show m < n from
        calc m
          _ < m + m := by linarith
          _ = 2 * m := by ring
          _ ≤ p * m := by rel [p_prime.left]
          _ = n := n_eq_pm.symm
    obtain (L : List Nat) (h6 : prime_factorization m L)
      from ih m m_lt_n m_pos
    define at h6
    have ndpl_L : nondec_prime_list L := h6.left
    define at ndpl_L
    apply Exists.intro (p :: L)
    define
    apply And.intro
    · -- Proof of nondec_prime_list (p :: L)
      define
      apply And.intro
      · -- Proof of all_prime (p :: L)
        rw [all_prime_cons]
        exact And.intro p_prime ndpl_L.left
      · -- Proof of nondec (p :: L)
        rw [nondec_cons]
        apply And.intro _ ndpl_L.right
        fix q : Nat
        assume q_in_L : q ∈ L
        have h7 : q ∣ prod L := list_elt_dvd_prod q_in_L
        rw [h6.right] at h7
        have h8 : m ∣ n := by
          apply Exists.intro p
          rewrite [n_eq_pm]
          ring
        have q_dvd_n : q ∣ n := dvd_trans h7 h8
        have ap_L : all_prime L := ndpl_L.left
        define at ap_L
        have q_prime_factor : prime_factor q n :=
          And.intro (ap_L q q_in_L) q_dvd_n
        show p ≤ q from p_least q q_prime_factor
    · -- Proof of prod (p :: L) = n
      rewrite [prod_cons, h6.right, n_eq_pm]
      rfl
  done

/-
We now turn to uniqueness. We first need the concept of relative primes.
-/

def rel_prime' (a b : Nat) : Prop := gcd a b = 1

theorem Theorem_7_2_2' {a b c : Nat}
    (h1 : c ∣ a * b) (h2 : rel_prime a c) : c ∣ b := by
  rw [←Int.coe_nat_dvd]
  define at h1; define at h2; define
  obtain (j : Nat) (h3 : a * b = c * j) from h1
  set s : Int := gcd_c1 a c
  set t : Int := gcd_c2 a c
  have h4 : s * ↑a + t * ↑c = ↑(gcd a c) := gcd_lin_comb c a
  rw [h2, Nat.cast_one] at h4
  apply Exists.intro (s * ↑j + t * ↑b)
  show ↑b = ↑c * (s * ↑j + t * ↑b) from
    calc ↑b
      _ = (1 : Int) * ↑b := (one_mul _).symm
      _ = (s * ↑a + t * ↑c) * ↑b := by rw [h4]
      _ = s * (↑a * ↑b) + t * ↑c * ↑b := by ring
      _ = s * (↑c * ↑j) + t * ↑c * ↑b := by
            rw [←Nat.cast_mul a b, h3, Nat.cast_mul c j]
      _ = ↑c * (s * ↑j + t * ↑b) := by ring
  done

lemma dvd_prime' {a p : Nat}
    (h1 : prime p) (h2 : a ∣ p) : a = 1 ∨ a = p := by
  by_cases h3 : a = 1
  · left; exact h3
  · right
    define at h1
    have two_le_p : 2 ≤ p := h1.left
    have p_prime_cond : ¬∃ (a : ℕ), ∃ (b : ℕ), a * b = p ∧ a < p ∧ b < p := h1.right
    obtain (b : Nat) (h4 : p = a * b) from h2
    have h5 : a ≠ 0 := by
      by_contra h5
      rw [h5, zero_mul] at h4
      linarith
    have h6 : a ≥ p := by
      by_contra h6
      apply p_prime_cond
      apply Exists.intro a; apply Exists.intro b
      apply And.intro h4.symm
      have h7 : a < p := by linarith
      apply And.intro h7
      by_contra h8
      have h9 : b ≥ p := by linarith
      have h10 : a > 1 := by
        have h10 : a ≥ 1 := Nat.pos_of_ne_zero h5
        exact lt_of_le_of_ne' h10 h3
      have h10 : p < p := by
        calc p
          _ = a * b := by rw [h4]
          _ ≥ a * p := by rel [h9]
          _ > 1 * p := by rel [h10]
          _ = p := by ring
      linarith
    have h7 : b ≥ 1 := by
      by_contra h7
      have h8 : b = 0 := by linarith
      rw [h8, mul_zero] at h4
      linarith
    have h8 : a ≤ p := by
      calc a
        _ = a * 1 := by rw [mul_one]
        _ ≤ a * b := by rel [h7]
        _ = p := by rw [h4]
    linarith
  done

lemma rel_prime_of_prime_not_dvd' {a p : Nat}
    (h1 : prime p) (h2 : ¬p ∣ a) : rel_prime a p := by
  have h3 : gcd a p ∣ a := gcd_dvd_left a p
  have h4 : gcd a p ∣ p := gcd_dvd_right a p
  have h5 : gcd a p = 1 ∨ gcd a p = p := dvd_prime h1 h4
  have h6 : gcd a p ≠ p := by
    contradict h2 with h6
    rewrite [h6] at h3
    show p ∣ a from h3
    done
  disj_syll h5 h6
  show rel_prime a p from h5
  done

theorem Theorem_7_2_3' {a b p : Nat}
    (h1 : prime p) (h2 : p ∣ a * b) : p ∣ a ∨ p ∣ b := by
  or_right with h3
  have h4 : rel_prime a p := rel_prime_of_prime_not_dvd h1 h3
  show p ∣ b from Theorem_7_2_2 h2 h4
  done

/-
Before we move on, we mention that Lean has a theorem called `List.rec`
to justify induction on lists, which is more convenient than doing
induction on the length of a list.
-/

lemma eq_one_of_dvd_one' {n : Nat} (h : n ∣ 1) : n = 1 := by
  define at h
  obtain (c : Nat) (h1 : 1 = n * c) from h
  by_contra h2
  by_cases h3 : n = 0
  · rw [h3, zero_mul] at h1
    trivial
  · have h4 : n > 1 := by
      have h5 : n ≥ 1 := Nat.pos_of_ne_zero h3
      exact lt_of_le_of_ne' h5 h2
    have h5 : c ≠ 0 := by
      by_contra h5
      rw [h5, mul_zero] at h1
      trivial
    have h5 : c ≥ 1 := Nat.pos_of_ne_zero h5
    have h6 : 1 > 1 := by
      calc 1
        _ = n * c := by rw [h1]
        _ > 1 * c := by rel [h4]
        _ = c := by rw [one_mul]
        _ ≥ 1 := h5
    trivial
  done

lemma prime_not_one' {p : Nat} (h : prime p) : p ≠ 1 := by
  define at h
  have h1 : 2 ≤ p := h.left
  linarith

theorem Theorem_7_2_4' {p : Nat} (h1 : prime p) :
    ∀ (l : List Nat), p ∣ prod l → ∃ a ∈ l, p ∣ a := by
  apply List.rec
  · -- Base case
    rw [prod_nil]
    assume h2 : p ∣ 1
    have h3 : p = 1 := eq_one_of_dvd_one h2
    by_contra
    exact (prime_not_one h1) h3
  · -- Induction step
    fix b : Nat
    fix L : List Nat
    assume ih : p ∣ prod L → ∃ a ∈ L, p ∣ a
    assume h2 : p ∣ prod (b :: L)
    rw [prod_cons] at h2
    have h3 : p ∣ b ∨ p ∣ prod L := Theorem_7_2_3 h1 h2
    by_cases on h3
    · -- Case p ∣ b
      apply Exists.intro b
      exact And.intro (List.mem_cons_self b L) h3
    · -- Case p ∣ prod L
      obtain (a : Nat) (h4 : a ∈ L ∧ p ∣ a) from ih h3
      apply Exists.intro a
      exact And.intro (List.mem_cons_of_mem b h4.left) h4.right
  done

lemma prime_in_list' {p : Nat} {l : List Nat}
    (h1 : prime p) (h2 : all_prime l) (h3 : p ∣ prod l) : p ∈ l := by
  obtain (a : Nat) (h4 : a ∈ l ∧ p ∣ a) from Theorem_7_2_4 h1 l h3
  define at h2
  have h5 : prime a := h2 a h4.left
  have h6 : p = 1 ∨ p = a := dvd_prime h5 h4.right
  disj_syll h6 (prime_not_one h1)
  rw [h6]
  show a ∈ l from h4.left
  done

lemma first_le_first' {p q : Nat} {l m : List Nat}
    (h1 : nondec_prime_list (p :: l)) (h2 : nondec_prime_list (q :: m))
    (h3 : prod (p :: l) = prod (q :: m)) : p ≤ q := by
  define at h1; define at h2
  have h4 : q ∣ prod (p :: l) := by
    define
    apply Exists.intro (prod m)
    rw [←prod_cons]
    show prod (p :: l) = prod (q :: m) from h3
    done
  have h5 : all_prime (q :: m) := h2.left
  rw [all_prime_cons] at h5
  have h6 : q ∈ p :: l := prime_in_list h5.left h1.left h4
  have h7 : nondec (p :: l) := h1.right
  rw [nondec_cons] at h7
  rw [List.mem_cons] at h6
  by_cases on h6
  · -- Case 1. h6 : q = p
    linarith
    done
  · -- Case 2. h6 : q ∈ l
    have h8 : ∀ m ∈ l, p ≤ m := h7.left
    show p ≤ q from h8 q h6
    done
  done

lemma nondec_prime_list_tail' {p : Nat} {l : List Nat}
    (h : nondec_prime_list (p :: l)) : nondec_prime_list l := by
  define at h
  define
  apply And.intro
  · have h1 : all_prime (p :: l) := h.left
    define at h1
    define; fix p' : Nat; assume h2 : p' ∈ l
    have h3 : p' ∈ p :: l := List.mem_cons_of_mem p h2
    exact h1 p' h3
  · have h2 : nondec (p :: l) := h.right
    define at h2
    exact h2.right
  done

lemma cons_prod_not_one' {p : Nat} {l : List Nat}
    (h : nondec_prime_list (p :: l)) : prod (p :: l) ≠ 1 := by
  define at h
  have h1 : all_prime (p :: l) := h.left
  define at h1
  have h2 : p ∈ p :: l :=  List.mem_cons_self p l
  have h3 : prime p := h1 p h2
  have h4 : p ∣ prod (p :: l) := list_elt_dvd_prod h2
  have h5 : p ≠ 1 := prime_not_one h3
  by_contra h6
  rw [h6] at h4
  have h7 : p = 1 := eq_one_of_dvd_one h4
  apply h5; exact h7
  done

lemma list_nil_iff_prod_one' {l : List Nat} (h : nondec_prime_list l) :
    l = [] ↔ prod l = 1 := by
  apply Iff.intro
  · assume h1 : l = []
    rw [h1]
    rfl
  · assume h1 : prod l = 1
    by_contra h2
    obtain (p : Nat) (h3 : exists L : List Nat, l = p :: L) from List.exists_cons_of_ne_nil h2
    obtain (L : List Nat) (h4 : l = p :: L) from h3
    rw [h4] at h
    have h5 : prod (p :: L) ≠ 1 := cons_prod_not_one h
    apply h5
    rw [h4] at h1
    exact h1
  done

lemma prime_pos' {p : Nat} (h : prime p) : p > 0 := by
  define at h
  have h1 : 2 ≤ p := h.left
  linarith
  done

theorem Theorem_7_2_5' : ∀ (l1 l2 : List Nat),
    nondec_prime_list l1 → nondec_prime_list l2 →
    prod l1 = prod l2 → l1 = l2 := by
  apply List.rec
  · -- Base case
    fix l2 : List Nat
    assume h1 : nondec_prime_list []
    assume h2 : nondec_prime_list l2
    assume h3 : prod [] = prod l2
    rw [prod_nil, eq_comm, ← list_nil_iff_prod_one h2] at h3
    exact h3.symm
  · -- Induction step
    fix p : Nat
    fix L1 : List Nat
    assume ih : ∀ (l2 : List Nat), nondec_prime_list L1 →
        nondec_prime_list l2 → prod L1 = prod l2 → L1 = l2
    fix l2 : List Nat
    assume h1 : nondec_prime_list (p :: L1)
    assume h2 : nondec_prime_list l2
    assume h3 : prod (p :: L1) = prod l2
    have h4 : ¬prod (p :: L1) = 1 := cons_prod_not_one h1
    rw [h3, ←list_nil_iff_prod_one h2] at h4
    obtain (q : Nat) (h5 : ∃ (L : List Nat), l2 = q :: L) from
      List.exists_cons_of_ne_nil h4
    obtain (L2 : List Nat) (h6 : l2 = q :: L2) from h5
    rw [h6] at h2
    rw [h6] at h3
    have h7 : p ≤ q := first_le_first h1 h2 h3
    have h8 : q ≤ p := first_le_first h2 h1 h3.symm
    have h9 : p = q := by linarith
    rewrite [h9, prod_cons, prod_cons] at h3
    have h10 : nondec_prime_list L1 := nondec_prime_list_tail h1
    have h11 : nondec_prime_list L2 := nondec_prime_list_tail h2
    define at h2
    have h12 : all_prime (q :: L2) := h2.left
    rewrite [all_prime_cons] at h12
    have h13 : q > 0 := prime_pos h12.left
    have h14 : prod L1 = prod L2 := Nat.eq_of_mul_eq_mul_left h13 h3
    have h15 : L1 = L2 := ih L2 h10 h11 h14
    rw [h6, h9, h15]
  done

/-
Finally, we can prove the fundamental theorem of arithmetic
-/

theorem fund_thm_arith' (n : Nat) (h : n ≥ 1) :
    ∃! (l : List Nat), prime_factorization n l := by
  exists_unique
  · -- Existence
    exact exists_prime_factorization n h
  · -- Uniqueness
    fix l1 : List Nat; fix l2 : List Nat
    assume h1 : prime_factorization n l1; define at h1
    assume h2 : prime_factorization n l2; define at h2
    have h3 : prod l1 = n := h1.right
    rw [← h2.right] at h3
    exact Theorem_7_2_5 l1 l2 h1.left h2.left h3
  done


-- Section 7.3. Modular arithmetic

/-
Congruence modulo a number, `a ≡ b (mod m)`, also denoted `a ≡ₘ b`,
means that `a` and `b` have the same remainder when dividing by `m`.
Equivalently, `m ∣ (a - b)`.

It is an equivalence relation over the integers, and we can thus
consider `ℤ/≡ₘ`. The equivalence classes `[a]ₘ` are referred to as the
congruence classes modulo m. We can define an operation on congruences
that satisfies nice properties:

1. For every `a : ℤ`, there is a congruence class `[a]ₘ ∈ ℤ/≡ₘ`
2. For every class `x ∈ ℤ/≡ₘ`, there is some integer `a` with `x = [a]ₘ`
3. For all integers `a` and `b`, `[a]ₘ = [b]ₘ ↔ a ≡ₘ b`
4. For all integers `a` and `b`, `[a]ₘ + [b]ₘ = [a + b]ₘ`
5. For all integers `a` and `b`, `[a]ₘ * [b]ₘ = [a * b]ₘ`

We will assume `m` to be a natural. Note that this includes the possibility
of `m = 0`, but we will focus on `m ≠ 0`. Our Lean definition is as
follows.
-/

def congr_mod' (m : Nat) (a b : Int) : Prop := (↑m : Int) ∣ (a - b)

/-
We can use meta-programming to introduce a nicer notation for this fact.
The following line is in the library for Chapter 7.

`notation:50 a " ≡ " b " (MOD " m ")" => congr_mod' m a b`
-/

/-
Lets prove that this is indeed an equivalence relation.
-/

theorem congr_refl' (m : Nat) : ∀ (a : Int), a ≡ a (MOD m) := by
  fix a : Int
  define
  apply Exists.intro 0
  ring
  done

theorem congr_symm' {m : Nat} : ∀ {a b : Int},
    a ≡ b (MOD m) → b ≡ a (MOD m) := by
  fix a : Int; fix b : Int
  assume h1 : a ≡ b (MOD m)
  define at h1
  define
  obtain (c : Int) (h2 : a - b = m * c) from h1
  apply Exists.intro (-c)
  show b - a = m * (-c) from
    calc b - a
      _ = -(a - b) := by ring
      _ = -(m * c) := by rw [h2]
      _ = m * (-c) := by ring
  done

theorem congr_trans' {m : Nat} : ∀ {a b c : Int},
    a ≡ b (MOD m) → b ≡ c (MOD m) → a ≡ c (MOD m) := by
  fix a : Int; fix b : Int; fix c : Int
  assume h1 : a ≡ b (MOD m)
  assume h2 : b ≡ c (MOD m)
  define at h1; define at h2
  obtain (d : Int) (h3 : a - b = m * d) from h1
  obtain (e : Int) (h4 : b - c = m * e) from h2
  define
  apply Exists.intro (d + e)
  show a - c = m * (d + e) from
    calc a - c
      _ = (a - b) + (b - c) := by ring
      _ = m * d + m * e := by rw [h3, h4]
      _ = m * (d + e) := by ring
  done

/-
We could now repeat the development of `ℤ/≡ₘ`, but it is already
included in Lean. For each `m`, Lean has the type `ZMod m`.
Note, however, that objects of type `ZMod m` are not sets of integers.
However, we do have the 5 properties listed above.
-/

/-
Property 1: If `a` has type `Int`, there should be a corresponding
congruence class in `ZMod m`.
-/

def cc' (m : Nat) (a : Int) : ZMod m := (↑a : ZMod m)

/-
`cc m a` is the congruence class of `a` modulo `m`. Again, we override
the notation. The following line is in the Library.

`notation:max "["a"]_"m:max => cc m a`
-/



/-
From now on, `[a]_m` is interpreted by Lean to mean cc m a.
-/

/-
Properties 2-5 are included in the library and are beyond the
scope of this book.
-/

#check cc_rep
-- ∀ {m : ℕ} (X : ZMod m), ∃ (a : ℤ), X = [a]_m

#check cc_eq_iff_congr
-- ∀ (m : ℕ) (a b : ℤ), [a]_m = [b]_m ↔ a ≡ b (MOD m)

#check add_class
-- ∀ (m : ℕ) (a b : ℤ), [a]_m + [b]_m = [a + b]_m

#check mul_class
-- ∀ (m : ℕ) (a b : ℤ), [a]_m * [b]_m = [a * b]_m


/-
In many results we will need to include the hypothesis `m ≠ 0`.
This is usually stated by the equivalent `NeZero m`.
-/

#check neZero_iff

/-
The distinguishing factor between the two statements is that
`NeZero m` is a type class. This means that once Lean has a proof
of `NeZero m`, it will remember that proof and recall it if necessary.
-/

/-
We will need results similar to those of divisibility of natural numbers,
but for the integers. Those are already included in Mathlib.
-/

#check Int.ediv_add_emod
-- ∀ (a b : ℤ), b * (a / b) + a % b = a

#check Int.emod_lt_of_pos
-- ∀ (a : ℤ) {b : ℤ}, 0 < b → a % b < b

#check Int.emod_nonneg
-- ∀ (a : ℤ) {b : ℤ}, b ≠ 0 → 0 ≤ a % b

/-
We now have enough background to prove that if `m ≠ 0`, every integer
`a` is congruent modulo `m` to exactly one integer `r` such that
`0 ≤ r < m`.
-/

/-
Note that when we write `a % m`, since `a` is an integer it must be
the integer operator, so `m` is implicitly coerced to an integer.
-/

lemma mod_nonneg' (m : Nat) [NeZero m] (a : Int) : 0 ≤ a % m := by
  have h1 : (↑m : Int) ≠ 0 := (Nat.cast_ne_zero).rtl (NeZero.ne m)
  exact Int.emod_nonneg a h1

lemma mod_lt' (m : Nat) [NeZero m] (a : Int) : a % m < m := by
  have h1 : 0 < m := by
    have h : m ≠ 0 := NeZero.ne m
    exact Nat.pos_of_ne_zero h
  have h2 : ↑0 < (↑m : Int) := by linarith
  exact Int.emod_lt_of_pos a h2
  done

lemma congr_mod_mod' (m : Nat) (a : Int) : a ≡ a % m (MOD m) := by
  define
  have h1 : m * (a / m) + a % m = a := Int.ediv_add_emod a m
  apply Exists.intro (a / m)
  show a - a % m = m * (a / m) from
    calc a - (a % m)
      _ = m * (a / m) + a % m - a % m := by rw [h1]
      _ = m * (a / m) := by ring
  done

lemma mod_cmpl_res' (m : Nat) [NeZero m] (a : Int) :
    0 ≤ a % m ∧ a % m < m ∧ a ≡ a % m (MOD m) :=
  And.intro (mod_nonneg m a) (And.intro (mod_lt m a) (congr_mod_mod m a))

theorem Theorem_7_3_1' (m : Nat) [NeZero m] (a : Int) :
    ∃! (r : Int), 0 ≤ r ∧ r < m ∧ a ≡ r (MOD m) := by
  exists_unique
  · -- Existence
    apply Exists.intro (a % m)
    exact mod_cmpl_res m a
  · -- Uniqueness
    fix r1 : Int; fix r2 : Int
    assume h1 : 0 ≤ r1 ∧ r1 < m ∧ a ≡ r1 (MOD m)
    assume h2 : 0 ≤ r2 ∧ r2 < m ∧ a ≡ r2 (MOD m)
    have h3 : r1 ≡ r2 (MOD m) :=
      congr_trans (congr_symm h1.right.right) h2.right.right
    obtain (d : Int) (h4 : r1 - r2 = m * d) from h3
    have h5 : r1 - r2 < m * 1 := by linarith
    have h6 : m * (-1) < r1 - r2 := by linarith
    rw [h4] at h5
    rw [h4] at h6
    have h7 : (↑m : Int) ≥ 0 := Nat.cast_nonneg m
    have h8 : d < 1 := lt_of_mul_lt_mul_of_nonneg_left h5 h7
    have h9 : -1 < d := lt_of_mul_lt_mul_of_nonneg_left h6 h7
    have h10 : d = 0 := by linarith
    show r1 = r2 from
      calc r1
        _ = r1 - r2 + r2 := by ring
        _ = m * 0 + r2 := by rw [h4, h10]
        _ = r2 := by ring
  done

/-
Note that we call lemmas that use the `NeZero m` hypothesis but we
don't need to pass it on as it is an implicit argument.

We now immediately have the following.
-/

lemma cc_eq_mod' (m : Nat) (a : Int) : [a]_m = [a % m]_m :=
  (cc_eq_iff_congr m a (a % m)).rtl (congr_mod_mod m a)

/-
We now prove a couple of properties of modular arithmetic.
-/

theorem Theorem_7_3_6_1' {m : Nat} (X Y : ZMod m) : X + Y = Y + X := by
  obtain (a : Int) (h1 : X = [a]_m) from cc_rep X
  obtain (b : Int) (h2 : Y = [b]_m) from cc_rep Y
  rewrite [h1, h2]
  have h3 : a + b = b + a := by ring
  show [a]_m + [b]_m = [b]_m + [a]_m from
    calc [a]_m + [b]_m
      _ = [a + b]_m := add_class m a b
      _ = [b + a]_m := by rw [h3]
      _ = [b]_m + [a]_m := (add_class m b a).symm
  done

theorem Theorem_7_3_6_7' {m : Nat} (X : ZMod m) : X * [1]_m = X := by
  obtain (a : Int) (h1 : X = [a]_m) from cc_rep X
  rewrite [h1]
  have h2 : a * 1 = a := by ring
  show [a]_m * [1]_m = [a]_m from
    calc [a]_m * [1]_m
      _ = [a * 1]_m := mul_class m a 1
      _ = [a]_m := by rw [h2]
  done

/-
Thus, `[1]_m` is the multiplicative identity. We can then define
multiplicative inverses as saying that `Y` is the multiplicative inverse
of `X` if `X * Y = [1]_m`.
-/

def invertible' {m : Nat} (X : ZMod m) : Prop :=
  ∃ (Y : ZMod m), X * Y = [1]_m

/-
What elements are invertible? We make use of the following exercise
from Section 2.
-/

#check Exercise_7_2_6
-- ∀ (a b : ℕ), rel_prime a b ↔ ∃ (s t : ℤ), s * ↑a + t * ↑b = 1

lemma gcd_c2_inv' {m a : Nat} (h1 : rel_prime m a) :
    [a]_m * [gcd_c2 m a]_m = [1]_m := by
  set s : Int := gcd_c1 m a
  have h2 : s * m + (gcd_c2 m a) * a = gcd m a := gcd_lin_comb a m
  define at h1
  rw [h1, Nat.cast_one] at h2
  rw [mul_class, cc_eq_iff_congr]
  define
  apply Exists.intro (-s)
  show a * gcd_c2 m a - 1 = m * -s from
    calc a * gcd_c2 m a - 1
      _ = s * m + (gcd_c2 m a) * a + m * (-s) - 1 := by ring
      _ = 1 + m * (-s) - 1 := by rw [h2]
      _ = m * -s := by ring
  done

theorem Theorem_7_3_7' (m a : Nat) :
    invertible [a]_m ↔ rel_prime m a := by
  apply Iff.intro
  · -- (→)
    assume h1 : invertible [a]_m
    define at h1
    obtain (Y : ZMod m) (h2 : [a]_m * Y = [1]_m) from h1
    obtain (b : Int) (h3 : Y = [b]_m) from cc_rep Y
    rw [h3, mul_class, cc_eq_iff_congr] at h2
    define at h2
    obtain (c : Int) (h4 : a * b - 1 = m * c) from h2
    rw [Exercise_7_2_6]
    apply Exists.intro (-c)
    apply Exists.intro b
    show (-c) * m + b * a = 1 from
      calc (-c) * m + b * a
        _ = (-c) * m + (a * b - 1) + 1 := by ring
        _ = (-c) * m + m * c + 1 := by rw [h4]
        _ = 1 := by ring
  · -- (←)
    assume h1 : rel_prime m a
    define
    apply Exists.intro ([gcd_c2 m a]_m)
    exact gcd_c2_inv h1
  done


-- Section 7.4. Euler's theorem

/-
Euler's theorem involves Euler's totient function `ϕ`. For any
positive integer `m`, `ϕ m` is the number of elements of `ℤ/≡ₘ`
that have multiplicative inverse. Let us first define such
function.

Since `{0, 1,..., m - 1}` is a complete residue system modulo `m`,
`ϕ m` can be described as the number of natural numbers `a < m`
such that `[a]_m` is invertible. As we have seen, this is only true
if `m` and `a` are relatively prime.

Let us thus define a function that counts how many numbers below `m`
are relatively prime.
-/

def num_rp_below' (m k : Nat) : Nat :=
  match k with
    | 0 => 0
    | j + 1 => if gcd m j = 1 then (num_rp_below m j) + 1
                else num_rp_below m j

/-
To prove theorems about `if` expressions, we will need the following.
-/

#check if_pos
-- ∀ {c : Prop} {h : Decidable c},
        -- c → ∀ {α : Sort u_1} {t e : α}, (if c then t else e) = t

#check if_neg
-- ∀ {c : Prop} {h : Decidable c},
  --      ¬c → ∀ {α : Sort u_1} {t e : α}, (if c then t else e) = e

/-
Thus, we have one or other outcome depending on whether we have
a proof for the property or for its opposite.
-/

lemma num_rp_below_base' {m : Nat} :
    num_rp_below m 0 = 0 := by rfl

lemma num_rp_below_step_rp' {m j : Nat} (h : rel_prime m j) :
    num_rp_below m (j + 1) = (num_rp_below m j) + 1 := by
  have h1 : num_rp_below m (j + 1) =
    if gcd m j = 1 then (num_rp_below m j) + 1
    else num_rp_below m j := by rfl
  define at h
  rw [if_pos h] at h1
  exact h1

lemma num_rp_below_step_not_rp' {m j : Nat} (h : ¬rel_prime m j) :
    num_rp_below m (j + 1) = num_rp_below m j := by
  have h1 : num_rp_below m (j +1) =
    if gcd m j = 1 then (num_rp_below m j) + 1
    else num_rp_below m j := by rfl
  define at h
  rw [if_neg h] at h1
  exact h1

/-
With this, we can define the totient function.
-/

def phi' (m : Nat) := num_rp_below m m

lemma phi_def' (m : Nat) : phi m = num_rp_below m m := by rfl

#eval phi 10   --Answer: 4

/-
The goal in this section is to prove Euler's theorem, which states
that if `a` and `m` are relative primes, `[a]_m ^ (ϕ m) = [1]_m`.

Let us begin with some prerequisites.
-/

lemma prod_inv_iff_inv' {m : Nat} {X : ZMod m}
    (h1 : invertible X) (Y : ZMod m) :
    invertible (X * Y) ↔ invertible Y := by
  apply Iff.intro
  · -- (→)
    assume h2 : invertible (X * Y)
    obtain (Z : ZMod m) (h3 : X * Y * Z = [1]_m) from h2
    apply Exists.intro (X * Z)
    rw [← h3]
    ring
  · -- (←)
    assume h2 : invertible Y
    obtain (Xi : ZMod m) (h3 : X * Xi = [1]_m) from h1
    obtain (Yi : ZMod m) (h4 : Y * Yi = [1]_m) from h2
    apply Exists.intro (Xi * Yi)
    show (X * Y) * (Xi * Yi) = [1]_m from
      calc X * Y * (Xi * Yi)
        _ = (X * Xi) * (Y * Yi) := by ring
        _ = [1]_m * [1]_m := by rw [h3, h4]
        _ = [1]_m := Theorem_7_3_6_7 [1]_m
  done

/-
The proof of Euler's theorem involves computing the product of
all invertible congruence classes. Thus, let us define:
-/

def F (m i : Nat) : ZMod m := if gcd m i = 1 then [i]_m else [1]_m

lemma F_rp_def' {m i : Nat} (h : rel_prime m i) :
    F m i = [i]_m := by
  have h1 : F m i = if gcd m i = 1 then [i]_m else [1]_m := by rfl
  define at h
  rw [if_pos h] at h1
  exact h1
  done

lemma F_not_rp_def' {m i : Nat} (h : ¬rel_prime m i) :
    F m i = [1]_m := by
  have h1 : F m i = if gcd m i = 1 then [i]_m else [1]_m := by rfl
  define at h
  rw [if_neg h] at h1
  exact h1
  done

/-
F is a function of two natural numbers, but `F m`, the partial
application, is a function from `Nat` to `ZMod m`.
-/

/-
We now introduce the mentioned product of all invertible congruence
classes, which we call F-product. This is the product of `F i` for
all `i` from `0` to `m - 1`. Indeed, if a class is not invertible,
`F i` returns `1`, and thus does not contribute to the product.

To express this product we follow the same strategy than what we used
for the summation.
-/

def prod_seq' {m : Nat}
    (j k : Nat) (f : Nat → ZMod m) : ZMod m :=
  match j with
    | 0 => [1]_m
    | n + 1 => prod_seq n k f * f (k + n)

/-
This is a product of j consecutive values of a function `f`, starting
with `f k`.
-/

lemma prod_seq_base' {m : Nat}
    (k : Nat) (f : Nat → ZMod m) : prod_seq 0 k f = [1]_m := by rfl

lemma prod_seq_step' {m : Nat}
    (n k : Nat) (f : Nat → ZMod m) :
    prod_seq (n + 1) k f = prod_seq n k f * f (k + n) := by rfl

lemma prod_seq_zero_step' {m : Nat}
    (n : Nat) (f : Nat → ZMod m) :
    prod_seq (n + 1) 0 f = prod_seq n 0 f * f n := by
  have h1 : prod_seq (n + 1) 0 f = prod_seq n 0 f * f (0 + n) := by rfl
  rw [zero_add] at h1
  exact h1
  done

/-
Thus, the F-product is `prod_seq m 0 (F m)`.
-/

/-
Now, if `a` is natural and relatively prime to `m`, we multiply each
factor in the product of the invertible congruence classes by `[a]_m`.
We do this by using a function
-/

def G (m a i : Nat) : Nat := (a * i) % m

lemma G_def' (m a i : Nat) : G m a i = (a * i) % m := by rfl

/-
Consider the following product, which we call FG-product.

`(F m (G m a 0)) * (F m (G m a 1)) * ... * (F m (G m a (m - 1)))`.

By partial application `G m a` we can express this as

`prod_seq m 0 ((F m) ∘ (G m a))`.

The following facts are needed to understand `G`.
-/

lemma cc_G' (m a i : Nat) : [G m a i]_m = [a]_m * [i]_m :=
  calc [G m a i]_m
    _ = [(a * i) % m]_m := by rfl
    _ = [a * i]_m := (cc_eq_mod m (a * i)).symm
    _ = [a]_m * [i]_m := (mul_class m a i).symm

lemma G_rp_iff' {m a : Nat} (h1 : rel_prime m a) (i : Nat) :
    rel_prime m (G m a i) ↔ rel_prime m i := by
  have h2 : invertible [a]_m := (Theorem_7_3_7 m a).rtl h1
  show rel_prime m (G m a i) ↔ rel_prime m i from
    calc rel_prime m (G m a i)
      _ ↔ invertible [G m a i]_m := (Theorem_7_3_7 m (G m a i)).symm
      _ ↔ invertible ([a]_m * [i]_m) := by rw [cc_G']
      _ ↔ invertible [i]_m := prod_inv_iff_inv h2 ([i]_m)
      _ ↔ rel_prime m i := Theorem_7_3_7 m i
  done

/-
Now we argue the following.

- If `i` is not relatively prime to `m`, then by `G_rp_iff`, `G m a i`
  is also not relatively prime to `m`, so `F m (G m a i) = [1]_m`.

- If `i` is relatively prime to `m`, so is `G m a i`, and by `cc_G`,
  `F m (G m a i) = [G m a i]_m = [a]_m * [i]_m = [a]_m * (F m i)`.

Thus, in the FG-product, each factor contributed by a value of `i`
relatively prime to `m` is `[a]_m` times the factor in the F-product.
Since the number of such factors is `φ m`, the FG-product is
`[a]_m ^ (phi m)` times the F-product. Let's prove this.
-/

lemma FG_rp' {m a i : Nat} (h1 : rel_prime m a) (h2 : rel_prime m i) :
    F m (G m a i) = [a]_m * F m i := by
  have h3 : rel_prime m (G m a i) := (G_rp_iff h1 i).rtl h2
  show F m (G m a i) = [a]_m * F m i from
    calc F m (G m a i)
      _ = [G m a i]_m := F_rp_def h3
      _ = [a]_m * [i]_m := cc_G m a i
      _ = [a]_m * F m i := by rw [F_rp_def' h2]
  done

lemma FG_not_rp' {m a i : Nat} (h1 : rel_prime m a) (h2 : ¬rel_prime m i) :
    F m (G m a i) = [1]_m := by
  rewrite [←G_rp_iff' h1 i] at h2
  exact F_not_rp_def' h2
  done

lemma FG_prod' {m a : Nat} (h1 : rel_prime m a) :
    ∀ (k : Nat), prod_seq k 0 ((F m) ∘ (G m a)) =
      [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m) := by
  by_induc
  · -- Base Case
    show prod_seq 0 0 ((F m) ∘ (G m a)) =
          [a]_m ^ (num_rp_below m 0) * prod_seq 0 0 (F m) from
      calc prod_seq 0 0 ((F m) ∘ (G m a))
        _ = [1]_m := prod_seq_base _ _
        _ = [a]_m ^ 0 * [1]_m := by ring
        _ = [a]_m ^ (num_rp_below m 0) * prod_seq 0 0 (F m) := by
              rw [num_rp_below_base, prod_seq_base]
  · -- Induction Step
    fix k : Nat
    assume ih : prod_seq k 0 ((F m) ∘ (G m a)) =
      [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m)
    by_cases h2 : rel_prime m k
    · -- Case 1. h2 : rel_prime m k
      show prod_seq (k + 1) 0 ((F m) ∘ (G m a)) =
          [a]_m ^ (num_rp_below m (k + 1)) *
          prod_seq (k + 1) 0 (F m) from
        calc prod_seq (k + 1) 0 ((F m) ∘ (G m a))
          _ = prod_seq k 0 ((F m) ∘ (G m a)) *
              F m (G m a k) := prod_seq_zero_step _ _
          _ = [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m) *
              F m (G m a k) := by rw [ih]
          _ = [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m) *
              ([a]_m * F m k) := by rw [FG_rp' h1 h2]
          _ = [a]_m ^ ((num_rp_below m k) + 1) *
              ((prod_seq k 0 (F m)) * F m k) := by ring
          _ = [a]_m ^ (num_rp_below m (k + 1)) *
              prod_seq (k + 1) 0 (F m) := by
                rw [num_rp_below_step_rp h2, prod_seq_zero_step]
    · -- Case 2. h2 : ¬rel_prime m k
      show prod_seq (k + 1) 0 ((F m) ∘ (G m a)) =
          [a]_m ^ (num_rp_below m (k + 1)) *
          prod_seq (k + 1) 0 (F m) from
        calc prod_seq (k + 1) 0 ((F m) ∘ (G m a))
          _ = prod_seq k 0 ((F m) ∘ (G m a)) *
              F m (G m a k) := prod_seq_zero_step _ _
          _ = [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m) *
              F m (G m a k) := by rw [ih]
          _ = [a]_m ^ (num_rp_below m k) * prod_seq k 0 (F m) *
              ([1]_m) := by rw [FG_not_rp' h1 h2]
          _ = [a]_m ^ (num_rp_below m k) *
              (prod_seq k 0 (F m) * ([1]_m)) := by ring
          _ = [a]_m ^ (num_rp_below m (k + 1)) *
              prod_seq (k + 1) 0 (F m) := by
                rw [num_rp_below_step_not_rp h2, prod_seq_zero_step,
                F_not_rp_def' h2]
  done

/-
This lemma, in the case `k = m`, tells us that

`prod_seq m 0 ((F m) ∘ (G m a)) = [a]_m ^ (phi m) * prod_seq m 0 (F m).`

In other words, the FG-product is `[a]_m ^ (phi m)` times the F-product.

And now we move to the following point: the congruence classes
multiplied in the FG-product are the same as the ones in the F-product,
but listed in a different order. The reason for this is that `G m a`
is just a permutation of the natural numbers less than `m`.

To prove these claims, let us first define what it means for a function
to permute the natural numbers less than `m`.
-/

def maps_below' (n : Nat) (g : Nat → Nat) : Prop := ∀ i < n, g i < n

def one_one_below' (n : Nat) (g : Nat → Nat) : Prop :=
  ∀ i1 < n, ∀ i2 < n, g i1 = g i2 → i1 = i2

def onto_below' (n : Nat) (g : Nat → Nat) : Prop :=
  ∀ k < n, ∃ i < n, g i = k

def perm_below' (n : Nat) (g : Nat → Nat) : Prop :=
  maps_below n g ∧ one_one_below n g ∧ onto_below n g

/-
And now we show that `G` verifies these properties. We need the following
lemmas.
-/

lemma G_maps_below' (m a : Nat) [NeZero m] : maps_below m (G m a) := by
  define
  fix i : Nat
  assume h1 : i < m
  rw [G_def']
  exact mod_nonzero_lt (a * i) (NeZero.ne m)

lemma right_inv_onto_below' {n : Nat} {g g' : Nat → Nat}
    (h1 : ∀ i < n, g (g' i) = i) (h2 : maps_below' n g') :
    onto_below n g := by
  define at h2; define
  fix k : Nat
  assume h3 : k < n
  apply Exists.intro (g' k)
  exact And.intro (h2 k h3) (h1 k h3)

lemma left_inv_one_one_below' {n : Nat} {g g' : Nat → Nat}
    (h1 : ∀ i < n, g' (g i) = i) : one_one_below' n g := by
  define
  fix k : Nat
  assume h2 : k < n
  fix k' : Nat
  assume h3 : k' < n
  assume h4 : g k = g k'
  have h5 : g' (g k) = k := h1 k h2
  have h6 : g' (g k') = k' := h1 k' h3
  show k = k' from
    calc k
      _ = g' (g k) := by rw [h5]
      _ = g' (g k') := by rw [h4]
      _ = k' := by rw [h6]
  done

/-
To apply these to `g = G m a`, we need a function verifying the role
of g', which is essentially the inverse of g. A natural choice would
be `G m a'`, where a' is chosen so that `[a']_m` is inverse of `[a]_m`.
We know that if m and a are relatively prime then an inverse of `[a]_m`
is `[gcd_c2 m a]_m`. But this is an integer, and we cannot directly
coerce it to a natural because it could potentially be negative.
However, we know that `[gcd_c2 m a]_m = [(gcd_c2 m a) % m]_m` and
that `0 ≤ (gcd_c2 m a) % m`. We can then use the function `Int.toNat`
to convert it to a natural number.
-/

def inv_mod' (m a : Nat) : Nat := Int.toNat ((gcd_c2 m a) % m)

def Ginv' (m a i : Nat) : Nat := G m (inv_mod' m a) i

lemma Ginv_def' {m a i : Nat} : Ginv' m a i = G m (inv_mod m a) i := by rfl

/-
And now we can get the results
-/

lemma Ginv_right_inv' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    ∀ i < m, G m a (Ginv' m a i) = i := by
  fix i : Nat
  assume h2 : i < m
  show G m a (Ginv' m a i) = i from
    calc G m a (Ginv' m a i)
      _ = a * ((inv_mod' m a * i) % m) % m := by rfl
      _ = a * (inv_mod' m a * i) % m := by rw [mul_mod_mod_eq_mul_mod]
      _ = a * inv_mod' m a * i % m := by rw [←mul_assoc]
      _ = i := mul_inv_mod_cancel h1 h2
  done

lemma Ginv_left_inv' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    ∀ i < m, Ginv' m a (G m a i) = i := by
  fix i : Nat
  assume h2 : i < m
  show Ginv' m a (G m a i) = i from
    calc Ginv' m a (G m a i)
      _ = inv_mod m a * ((a * i) % m) % m := by rfl
      _ = inv_mod m a * (a * i) % m := by rw [mul_mod_mod_eq_mul_mod]
      _ = a * inv_mod m a * i % m := by rw [←mul_assoc, mul_comm (inv_mod m a)]
      _ = i := mul_inv_mod_cancel h1 h2
  done

lemma Ginv_maps_below' (m a : Nat) [NeZero m] :
    maps_below' m (Ginv' m a) := G_maps_below' m (inv_mod' m a)

lemma G_one_one_below' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    one_one_below' m (G m a) :=
  left_inv_one_one_below' (Ginv_left_inv' h1)

lemma G_onto_below' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    onto_below' m (G m a) :=
  right_inv_onto_below' (Ginv_right_inv' h1) (Ginv_maps_below' m a)

lemma G_perm_below' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    perm_below' m (G m a) := And.intro (G_maps_below' m a)
  (And.intro (G_one_one_below' h1) (G_onto_below' h1))

/-
And we now turn to proving that if we have a permutation the
product does not change. We will do so by induction. The key idea is that
if we have a permutation of `n + 1` elements, we can find the element
that maps to `n` and swap it for `n`, so we obtain a permutation of the
`n` elements below.
-/

def swap' (u v i : Nat) : Nat :=
  if i = u then v else if i = v then u else i

lemma swap_fst' (u v : Nat) : swap' u v u = v := by
  define : swap' u v u
  have h : u = u := by rfl
  rw [if_pos h]
  done

lemma swap_snd' (u v : Nat) : swap' u v v = u := by
  define : swap' u v v
  have h : v = v := by rfl
  rw [if_pos h]
  by_cases h1 : v = u
  · rw [if_pos h1]
    exact h1
  · rw [if_neg h1]
  done

lemma swap_other' {u v i : Nat} (h1 : i ≠ u) (h2 : i ≠ v) :
    swap' u v i = i := by
  define : swap' u v i
  rewrite [if_neg h1, if_neg h2]
  rfl
  done

lemma swap_values' (u v i : Nat) : swap' u v i = v
    ∨ swap' u v i = u ∨ swap' u v i = i := by
  by_cases h1 : i = u
  · -- Case 1. h1 : i = u
    apply Or.inl
    rewrite [h1]
    exact swap_fst' u v
  · -- Case 2. h1 : i ≠ u
    apply Or.inr
    by_cases h2 : i = v
    · -- Case 2.1. h2 : i = v
      apply Or.inl
      rewrite [h2]
      exact swap_snd' u v
    · -- Case 2.2. h2 : i ≠ v
      apply Or.inr
      exact swap_other' h1 h2
  done

lemma swap_maps_below' {u v n : Nat} (h1 : u < n) (h2 : v < n) :
    maps_below' n (swap' u v) := by
  define
  fix i : Nat
  assume h3 : i < n
  have h4 : swap' u v i = v ∨ swap' u v i = u ∨ swap' u v i = i
    := swap_values' u v i
  by_cases on h4
  · -- Case 1. h4 : swap u v i = v
    rewrite [h4]
    exact h2
  · -- Case 2.
    by_cases on h4
    · -- Case 2.1. h4 : swap u v i = u
      rewrite [h4]
      exact h1
    · -- Case 2.2. h4 : swap u v i = i
      rewrite [h4]
      exact h3
  done

lemma swap_swap' (u v n : Nat) : ∀ i < n, swap' u v (swap' u v i) = i := by
  fix i : Nat
  assume h : i < n
  by_cases h1 : i = u
  · -- Case 1. h1 : i = u
    rw [h1, swap_fst', swap_snd']
  · -- Case 2. h1 : i ≠ u
    by_cases h2 : i = v
    · -- Case 2.1. h2 : i = v
      rw [h2, swap_snd', swap_fst']
    · -- Case 2.2. h2 : i ≠ v
      rw [swap_other' h1 h2, swap_other' h1 h2]
  done

lemma swap_one_one_below' (u v n) : one_one_below' n (swap u v) :=
  left_inv_one_one_below' (swap_swap' u v n)

lemma swap_onto_below' {u v n} (h1 : u < n) (h2 : v < n) :
    onto_below' n (swap' u v) :=
  right_inv_onto_below' (swap_swap' u v n) (swap_maps_below' h1 h2)

lemma swap_perm_below' {u v n} (h1 : u < n) (h2 : v < n) :
    perm_below' n (swap u v) :=
  And.intro (swap_maps_below' h1 h2)
    (And.intro (swap_one_one_below' u v n) (swap_onto_below' h1 h2))

/-
Now, to prove that `g ∘ swap u n` permutes the numbers below `n`,
we need the following.
-/

lemma comp_perm_below' {n : Nat} {f g : Nat → Nat}
    (h1 : perm_below n f) (h2 : perm_below n g) :
    perm_below n (f ∘ g) := by
  define
  apply And.intro
  · define
    fix i : Nat
    assume h3 : i < n
    have h4 : maps_below n g := h2.left
    define at h4
    have h5 : g i < n := h4 i h3
    have h6 : maps_below n f := h1.left
    define at h6
    exact h6 (g i) h5
  · apply And.intro
    · define
      fix i1 : Nat
      assume h3 : i1 < n
      fix i2 : Nat
      assume h4 : i2 < n
      assume h5 : (f ∘ g) i1 = (f ∘ g) i2
      have h6 : one_one_below n f := h1.right.left
      define at h6
      have h7 : maps_below n g := h2.left
      define at h7
      have h8 : g i1 < n := h7 i1 h3
      have h9 : g i2 < n := h7 i2 h4
      have h10 : g i1 = g i2 := h6 (g i1) h8 (g i2) h9 h5
      have h11 : one_one_below n g := h2.right.left
      exact h11 i1 h3 i2 h4 h10
    · define
      fix k : Nat
      assume h3 : k < n
      have h4 : onto_below n f := h1.right.right
      have h5 : onto_below n g := h2.right.right
      define at h4; define at h5
      obtain (k' : Nat) (h6 : k' < n ∧ f k' = k) from h4 k h3
      obtain (k'' : Nat) (h7 : k'' < n ∧ g k'' = k') from h5 k' h6.left
      apply Exists.intro k''
      apply And.intro h7.left
      have h8 : f (g k'') = k := by
        rw [h7.right, h6.right]
      exact h8
  done

lemma perm_below_fixed' {n : Nat} {g : Nat → Nat}
    (h1 : perm_below' (n + 1) g) (h2 : g n = n) : perm_below' n g := by
  define
  apply And.intro
  · define
    fix i : Nat
    assume h3 : i < n
    have h4 : i < n + 1 := by linarith
    have h5 : g i < n + 1 := h1.left i h4
    have h6 : i ≠ n := by linarith
    have h7 : one_one_below (n + 1) g := h1.right.left
    define at h7
    have h8 : g i ≠ n := by
      by_contra h8
      have h9 : n < n + 1 := by linarith
      rw [← h2] at h8
      have h10 : i = n := h7 i h4 n h9 h8
      show False from h6 h10
    rw [Nat.lt_add_one_iff] at h5
    exact Nat.lt_of_le_of_ne h5 h8
  · apply And.intro
    · define
      fix i1 : Nat; assume h4 : i1 < n
      fix i2 : Nat; assume h5 : i2 < n
      have h6 : i1 < n + 1 := by linarith
      have h7 : i2 < n + 1 := by linarith
      exact h1.right.left i1 h6 i2 h7
    · define
      fix k : Nat
      assume h4 : k < n
      have h5 : k < n + 1 := by linarith
      obtain (i : Nat) (h6 : i < (n + 1) ∧ g i = k) from h1.right.right k h5
      apply Exists.intro i
      apply And.intro _ h6.right
      have h7 : i ≠ n := by
        by_contra h7
        have h8 : g i = k := h6.right
        rw [h7, h2] at h8
        linarith
      have h8 : i < n + 1 := h6.left
      rw [Nat.lt_add_one_iff] at h8
      exact Nat.lt_of_le_of_ne h8 h7
  done

/- Here's the proof of `perm_prod` -/

lemma perm_prod' {m : Nat} (f : Nat → ZMod m) :
    ∀ (n : Nat), ∀ (g : Nat → Nat), perm_below n g →
      prod_seq n 0 f = prod_seq n 0 (f ∘ g) := by
  by_induc
  · -- Base Case
    fix g : Nat → Nat
    assume h1 : perm_below 0 g
    rewrite [prod_seq_base, prod_seq_base]
    rfl
    done
  · -- Induction Step
    fix n : Nat
    assume ih : ∀ (g : Nat → Nat), perm_below n g →
      prod_seq n 0 f = prod_seq n 0 (f ∘ g)
    fix g : Nat → Nat
    assume g_pb : perm_below (n + 1) g
    define at g_pb
    have g_ob : onto_below (n + 1) g := g_pb.right.right
    define at g_ob
    have h1 : n < n + 1 := by linarith
    obtain (u : Nat) (h2 : u < n + 1 ∧ g u = n) from g_ob n h1
    have s_pb : perm_below (n + 1) (swap u n) :=
      swap_perm_below h2.left h1
    have gs_pb_n1 : perm_below (n + 1) (g ∘ swap u n) :=
      comp_perm_below g_pb s_pb
    have gs_fix_n : (g ∘ swap u n) n = n :=
      calc (g ∘ swap u n) n
        _ = g (swap u n n) := by rfl
        _ = g u := by rw [swap_snd]
        _ = n := h2.right
    have gs_pb_n : perm_below n (g ∘ swap u n) :=
      perm_below_fixed gs_pb_n1 gs_fix_n
    have gs_prod : prod_seq n 0 f = prod_seq n 0 (f ∘ (g ∘ swap u n)) :=
      ih (g ∘ swap u n) gs_pb_n
    have h3 : u ≤ n := by linarith
    show prod_seq (n + 1) 0 f = prod_seq (n + 1) 0 (f ∘ g) from
      calc prod_seq (n + 1) 0 f
        _ = prod_seq n 0 f * f n := prod_seq_zero_step n f
        _ = prod_seq n 0 (f ∘ (g ∘ swap u n)) *
            f ((g ∘ swap u n) n) := by rw [gs_prod, gs_fix_n]
        _ = prod_seq n 0 (f ∘ g ∘ swap u n) *
            (f ∘ g ∘ swap u n) n := by rfl
        _ = prod_seq (n + 1) 0 (f ∘ g ∘ swap u n) :=
              (prod_seq_zero_step n (f ∘ g ∘ swap u n)).symm
        _ = prod_seq (n + 1) 0 ((f ∘ g) ∘ swap u n) := by rfl
        _ = prod_seq (n + 1) 0 (f ∘ g) := swap_prod_eq_prod (f ∘ g) h3
    done
  done

-- Now let's prove that all of the factors in the F-product are invertible

lemma F_invertible' (m i : Nat) : invertible (F m i) := by
  by_cases h : rel_prime m i
  · -- Case 1. h : rel_prime m i
    rewrite [F_rp_def' h]
    show invertible [i]_m from (Theorem_7_3_7 m i).rtl h
    done
  · -- Case 2. h : ¬rel_prime m i
    rewrite [F_not_rp_def' h]
    apply Exists.intro [1]_m
    show [1]_m * [1]_m = [1]_m from Theorem_7_3_6_7 [1]_m
    done
  done

lemma Fprod_invertible' (m : Nat) :
    ∀ (k : Nat), invertible (prod_seq k 0 (F m)) := by
  by_induc
  · -- Base Case
    apply Exists.intro [1]_m
    show prod_seq 0 0 (F m) * [1]_m = [1]_m from
      calc prod_seq 0 0 (F m) * [1]_m
        _ = [1]_m * [1]_m := by rw [prod_seq_base]
        _ = [1]_m := Theorem_7_3_6_7 ([1]_m)
    done
  · -- Induction Step
    fix k : Nat
    assume ih : invertible (prod_seq k 0 (F m))
    rewrite [prod_seq_zero_step]
    show invertible (prod_seq k 0 (F m) * (F m k)) from
      (prod_inv_iff_inv ih (F m k)).rtl (F_invertible m k)
    done
  done

-- And with that...

theorem Theorem_7_4_2' {m a : Nat} [NeZero m] (h1 : rel_prime m a) :
    [a]_m ^ (phi m) = [1]_m := by
  have h2 : invertible (prod_seq m 0 (F m)) := Fprod_invertible m m
  obtain (Y : ZMod m) (h3 : prod_seq m 0 (F m) * Y = [1]_m) from h2
  show [a]_m ^ (phi m) = [1]_m from
    calc [a]_m ^ (phi m)
      _ = [a]_m ^ (phi m) * [1]_m := (Theorem_7_3_6_7 _).symm
      _ = [a]_m ^ (phi m) * (prod_seq m 0 (F m) * Y) := by rw [h3]
      _ = ([a]_m ^ (phi m) * prod_seq m 0 (F m)) * Y := by ring
      _ = prod_seq m 0 (F m ∘ G m a) * Y := by rw [FG_prod' h1 m, phi_def]
      _ = prod_seq m 0 (F m) * Y := by
            rw [perm_prod (F m) m (G m a) (G_perm_below h1)]
      _ = [1]_m := by rw [h3]
  done

-- Finally, rephrasing we get Euler's theorem
lemma Exercise_7_4_5_Int' (m : Nat) (a : Int) :
    ∀ (n : Nat), [a]_m ^ n = [a ^ n]_m := by
  by_induc
  · -- Base case
    rw [← Theorem_7_3_6_7 ([a]_m ^ 0), pow_zero, pow_zero, one_mul]
  · -- Induction step
    fix n : Nat
    assume ih : [a]_m ^ n = [a ^ n]_m
    show [a]_m ^ (n + 1) = [a ^ (n + 1)]_m from
      calc [a]_m ^ (n + 1)
        _ = [a]_m ^ n * [a]_m := by ring
        _ = [a ^ n]_m * [a]_m := by rw [ih]
        _ = [a ^ n * a]_m := by rw [mul_class]
        _ = [a ^ (n + 1)]_m := by ring_nf
  done

lemma Exercise_7_4_5_Nat' (m a n : Nat) :
    [a]_m ^ n = [a ^ n]_m := by
  rw [Exercise_7_4_5_Int, Nat.cast_pow]
  done

theorem Euler's_theorem' {m a : Nat} [NeZero m]
    (h1 : rel_prime m a) : a ^ (phi m) ≡ 1 (MOD m) := by
  have h2 : [a]_m ^ (phi m) = [1]_m := Theorem_7_4_2 h1
  rewrite [Exercise_7_4_5_Nat m a (phi m)] at h2
    --h2 : [↑(a ^ phi m)]_m = [1]_m
  show a ^ (phi m) ≡ 1 (MOD m) from (cc_eq_iff_congr _ _ _).ltr h2
  done

#eval gcd 10 7     --Answer: 1.  So 10 and 7 are relatively prime

#eval 7 ^ phi 10   --Answer: 2401, which is congruent to 1 mod 10.


-- Section 7.5. Public-key cryptography

/-
The idea here is to introduce the main ideas behind RSA cryptography.
-/

lemma num_rp_prime' {p : Nat} (h1 : prime p) :
    ∀ k < p, num_rp_below p (k + 1) = k := by
  by_induc
  · -- Base case
    assume h2 : 0 < p
    have h3 : ¬rel_prime p 0 := by
      define
      rw [gcd_base]
      exact prime_not_one h1
    rw [num_rp_below_step_not_rp h3, num_rp_below_base]
  · -- Induction step
    fix k : Nat
    assume ih : k < p → num_rp_below p (k + 1) = k
    assume h2 : k + 1 < p
    have h3 : rel_prime (k + 1) p := by
      have h3 : ¬p ∣ (k + 1) := by
        by_contra h4
        define at h4
        obtain (c : Nat) (h5 : k + 1 = p * c) from h4
        have h6 : c ≥ 1 := by
          by_contra h6
          have h6 : c = 0 := by linarith
          rw [h6, mul_zero] at h5
          linarith
        have h7 : k + 1 ≥ p := by
          calc k + 1
            _ = p * c := by rw [h5]
            _ ≥ p * 1 := by rel [h6]
            _ = p := by rw [mul_one]
        linarith
      exact rel_prime_of_prime_not_dvd h1 h3
    have h4 : rel_prime p (k + 1) := rel_prime_symm h3
    have h5 : k < p := by linarith
    rw [num_rp_below_step_rp h4, ih h5]
  done


lemma phi_prime' {p : Nat} (h1 : prime p) : phi p = p - 1 := by
  have h2 : 1 ≤ p := prime_pos h1
  have h3 : p - 1 + 1 = p := Nat.sub_add_cancel h2
  have h4 : p - 1 < p := by linarith
  have h5 : num_rp_below p (p - 1 + 1) = p - 1 :=
    num_rp_prime h1 (p - 1) h4
  rewrite [h3] at h5
  exact h5
  done

/-
We now will need to use Theorem_7_2_2, which states that if a, b and c are
naturals, if `c ∣ a * b` and `c` and `a` are relatively prime, then `c ∣ b`.
But we need this result for integers. We will use the following functions.
-/

#check Int.coe_nat_dvd_left
-- ∀ {n : ℕ} {z : ℤ}, ↑n ∣ z ↔ n ∣ Int.natAbs z

#check Int.natAbs_mul
-- Int.natAbs (a * b) = Int.natAbs a * Int.natAbs b

#check Int.natAbs_ofNat
-- ∀ (n : ℕ), Int.natAbs ↑n = n

/-
Of course, naturals are non-negative so they are their own absolute values.
-/

theorem Theorem_7_2_2_Int' {a c : Nat} {b : Int}
    (h1 : ↑c ∣ ↑a * b) (h2 : rel_prime a c) : ↑c ∣ b := by
  rw [Int.coe_nat_dvd_left, Int.natAbs_mul,
    Int.natAbs_ofNat] at h1
  rw [Int.coe_nat_dvd_left]
  exact Theorem_7_2_2 h1 h2
  done

/-
And now we can prove the following key lemma.
-/

lemma Lemma_7_4_5' {m n : Nat} (a b : Int) (h1 : rel_prime m n) :
    a ≡ b (MOD m * n) ↔ a ≡ b (MOD m) ∧ a ≡ b (MOD n) := by
  apply Iff.intro
  · -- (→)
    assume h2 : a ≡ b (MOD m * n)
    obtain (j : Int) (h3 : a - b = (m * n) * j) from h2
    apply And.intro
    · -- Proof of a ≡ b (MOD m)
      apply Exists.intro (n * j)
      show a - b = m * (n * j) from
        calc a - b
          _ = m * n * j := h3
          _ = m * (n * j) := by ring
    · -- Proof of a ≡ b (MOD n)
      apply Exists.intro (m * j)
      show a - b = n * (m * j) from
        calc a - b
          _ = m * n * j := h3
          _ = n * (m * j) := by ring
  · -- (←)
    assume h2 : a ≡ b (MOD m) ∧ a ≡ b (MOD n)
    obtain (j : Int) (h3 : a - b = m * j) from h2.left
    have h4 : (↑n : Int) ∣ a - b := h2.right
    rw [h3] at h4
    have h5 : ↑n ∣ j := Theorem_7_2_2_Int h4 h1
    obtain (k : Int) (h6 : j = n * k) from h5
    apply Exists.intro k
    show a - b = (m * n) * k from
      calc a - b
        _ = m * j := h3
        _ = m * (n * k) := by rw [h6]
        _ = (m * n) * k := by ring
  done

lemma prime_NeZero' {p : Nat} (h : prime p) : NeZero p := by
  rw [neZero_iff]
  define at h
  linarith
  done

/-
The following technical lemma contains much of the reasoning we need.
-/

lemma Lemma_7_5_1' {p e d m c s : Nat} {t : Int}
    (h1 : prime p) (h2 : e * d = (p - 1) * s + 1)
    (h3 : ↑(m ^ e) - ↑c = ↑p * t) :
    c ^ d ≡ m (MOD p) := by
  have h4 : m ^ e ≡ c (MOD p) := Exists.intro t h3
  have h5 : [m ^ e]_p = [c]_p := (cc_eq_iff_congr _ _ _).rtl h4
  rw [←Exercise_7_4_5_Nat] at h5
  by_cases h6 : p ∣ m
  · -- Case 1. h6 : p ∣ m
    have h7 : m ≡ 0 (MOD p) := by
      obtain (j : Nat) (h8 : m = p * j) from h6
      apply Exists.intro (↑j : Int)
      rw [h8, Nat.cast_mul]
      ring
    have h8 : [m]_p = [0]_p := (cc_eq_iff_congr _ _ _).rtl h7
    have h9 : 0 < (e * d) := by
      rw [h2]
      have h10 : 0 ≤ (p - 1) * s := Nat.zero_le _
      linarith
    have h10 : (0 : Int) ^ (e * d) = 0 := zero_pow h9
    have h11 : [c ^ d]_p = [m]_p :=
      calc [c ^ d]_p
        _ = [c]_p ^ d := by rw [Exercise_7_4_5_Nat]
        _ = ([m]_p ^ e) ^ d := by rw [h5]
        _ = [m]_p ^ (e * d) := by ring
        _ = [0]_p ^ (e * d) := by rw [h8]
        _ = [0 ^ (e * d)]_p := Exercise_7_4_5_Int _ _ _
        _ = [0]_p := by rw [h10]
        _ = [m]_p := by rw [h8]
    exact (cc_eq_iff_congr _ _ _).ltr h11
  · -- Case 2. h6 : ¬p ∣ m
    have h7 : rel_prime m p := rel_prime_of_prime_not_dvd h1 h6
    have h8 : rel_prime p m := rel_prime_symm h7
    have h9 : NeZero p := prime_NeZero h1
    have h10 : (1 : Int) ^ s = 1 := by ring
    have h11 : [c ^ d]_p = [m]_p :=
      calc [c ^ d]_p
        _ = [c]_p ^ d := by rw [Exercise_7_4_5_Nat]
        _ = ([m]_p ^ e) ^ d := by rw [h5]
        _ = [m]_p ^ (e * d) := by ring
        _ = [m]_p ^ ((p - 1) * s + 1) := by rw [h2]
        _ = ([m]_p ^ (p - 1)) ^ s * [m]_p := by ring
        _ = ([m]_p ^ (phi p)) ^ s * [m]_p := by rw [phi_prime h1]
        _ = [1]_p ^ s * [m]_p := by rw [Theorem_7_4_2 h8]
        _ = [1 ^ s]_p * [m]_p := by rw [Exercise_7_4_5_Int]
        _ = [1]_p * [m]_p := by rw [h10]
        _ = [m]_p * [1]_p := by ring
        _ = [m]_p := Theorem_7_3_6_7 _
    exact (cc_eq_iff_congr _ _ _).ltr h11
  done

/-
And finally we can prove the theorem!
-/

theorem Theorem_7_5_1' (p q n e d k m c : Nat)
    (p_prime : prime p) (q_prime : prime q) (p_ne_q : p ≠ q)
    (n_pq : n = p * q) (ed_congr_1 : e * d = k * (p - 1) * (q - 1) + 1)
    (h1 : [m]_n ^ e = [c]_n) : [c]_n ^ d = [m]_n := by
  rw [Exercise_7_4_5_Nat, cc_eq_iff_congr] at h1
  rw [Exercise_7_4_5_Nat, cc_eq_iff_congr]
  obtain (j : Int) (h2 : ↑(m ^ e) - ↑c = ↑n * j) from h1
  rw [n_pq, Nat.cast_mul] at h2
  have h3 : e * d = (p - 1) * (k * (q - 1)) + 1 := by
    rw [ed_congr_1]
    ring
  have h4 : ↑(m ^ e) - ↑c = ↑p * (↑q * j) := by
    rw [h2]
    ring
  have congr_p : c ^ d ≡ m (MOD p) := Lemma_7_5_1 p_prime h3 h4
  have h5 : e * d = (q - 1) * (k * (p - 1)) + 1 := by
    rw [ed_congr_1]
    ring
  have h6 : ↑(m ^ e) - ↑c = ↑q * (↑p * j) := by
    rw [h2]
    ring
  have congr_q : c ^ d ≡ m (MOD q) := Lemma_7_5_1 q_prime h5 h6
  have h7 : ¬q ∣ p := by
    by_contra h8
    have h9 : q = 1 ∨ q = p := dvd_prime p_prime h8
    disj_syll h9 (prime_not_one q_prime)
    show False from p_ne_q h9.symm
  have h8 : rel_prime p q := rel_prime_of_prime_not_dvd q_prime h7
  rw [n_pq, Lemma_7_4_5 _ _ h8]
  exact And.intro congr_p congr_q
  done
